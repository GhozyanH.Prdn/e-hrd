<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalityTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personality_tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_postulant')->nullable();
            $table->Integer('characterN');
            $table->Integer('characterG');
            $table->Integer('characterA');
            $table->Integer('characterL');
            $table->Integer('characterP');
            $table->Integer('characterI');
            $table->Integer('characterT');
            $table->Integer('characterV');
            $table->Integer('characterX');
            $table->Integer('characterS');
            $table->Integer('characterB');
            $table->Integer('characterO');
            $table->Integer('characterR');
            $table->Integer('characterD');
            $table->Integer('characterC');
            $table->Integer('characterZ');
            $table->Integer('characterE');
            $table->Integer('characterK');
            $table->Integer('characterF');
            $table->Integer('characterW');
            $table->timestamps();

            $table->foreign('id_postulant')
                  ->references('id')
                  ->on('postulants')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personality_tests');
    }
}
