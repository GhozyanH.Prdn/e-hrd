<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiCategorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti_categorys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('kode_kategori_cuti', 50)->unique();
            $table->char('jenis_cuti', 50);
            $table->char('keterangan', 100);
            $table->char('status', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_cutis');
    }
}
