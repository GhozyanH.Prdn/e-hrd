<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->unsignedBigInteger('id_postulant')-> unique()->nullable();
            $table->string('nama')-> nullable();
            $table->integer('parent')-> nullable();
            $table->char('parent_path',25)-> nullable();
            $table->string('kode_karyawan')-> unique();
            $table->string('password');
            $table->string('role');
            $table->unsignedBigInteger('id_jabatan')->nullable();
            $table->unsignedBigInteger('id_grade')->nullable();
            $table->unsignedBigInteger('id_corporate')->nullable();
            $table->unsignedBigInteger('id_devisi')->nullable();
            $table->date('join_date')->nullable();;
            $table->char('status', 15);
            $table->char('email_corporate', 50);
            $table->char('bank_account', 25);
            $table->char('rekening_number', 25);
            $table->char('account_name', 25);
            $table->char('nama_atasan', 100);
            $table->Integer('jatah_cuti');
            $table->date('first_contract')->nullable();
            $table->date('start_addendum1')->nullable();
            $table->date('end_addendum1')->nullable();
            $table->date('start_addendum2')->nullable();
            $table->date('end_addendum2')->nullable();
            $table->date('end_contract')->nullable();
            $table->char('alasan_berhenti', 25)->nullable();
            $table->char('gaji_pokok')->nullable();
            $table->char('tunjangan_harian')->nullable();
            $table->char('tunjangan_transport')->nullable();
            $table->integer('bpjs_kes')->nullable();
            $table->integer('bpjs_ket')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('id_jabatan')
                  ->references('id')
                  ->on('positions')
                  ->onDelete('set null');

            $table->foreign('id_grade')
                  ->references('id')
                  ->on('grades')
                  ->onDelete('set null');

            $table->foreign('id_corporate')
                  ->references('id')
                  ->on('corporate_groups')
                  ->onDelete('set null');
            $table->foreign('id_devisi')
                  ->references('id')
                  ->on('divisions')
                  ->onDelete('set null');
             $table->foreign('id_postulant')
                  ->references('id')
                  ->on('postulants')
                  ->onDelete('set null');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
