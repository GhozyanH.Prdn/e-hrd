<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobHistorysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_historys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_postulant')->nullable();
            $table->char('nama_perusahaan', 100);
            $table->char('posisi', 100)->nullable();
            $table->date('tanggal_mulai')->nullable();
            $table->date('tanggal_selesai')->nullable();
            $table->char('alasan_berhenti', 100)->nullable();
            $table->timestamps();

            $table->foreign('id_postulant')
                  ->references('id')
                  ->on('postulants')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_historys');
    }
}
