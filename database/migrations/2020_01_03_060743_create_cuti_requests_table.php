<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCutiRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuti_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Date('tanggal_request');
            $table->unsignedBigInteger('id_karyawan')->nullable();
            $table->Char('nama_atasan',90);
            $table->unsignedBigInteger('id_cuti')->nullable();
            $table->Date('tanggal_cuti');
            $table->float('quantity');
            $table->text('alasan');
            $table->Char('keputusan_hrd',50)->nullable();
            $table->Text('catatan_hrd')->nullable();
            $table->Char('keputusan_atasan',50)->nullable();
            $table->Text('catatan_atasan')->nullable();
            $table->timestamps();

            $table->foreign('id_karyawan')
            ->references('id')
            ->on('employees')
            ->onDelete('set null');

            $table->foreign('id_cuti')
            ->references('id')
            ->on('cuti_categorys')
            ->onDelete('set null');

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuti_requests');
    }
}
