<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('kode_peminjaman', 100);
            $table->unsignedBigInteger('id_aset')->nullable();
            $table->unsignedBigInteger('id_pegawai')->nullable();
            $table->char('tanggal_pinjam', 20)->nullable();
            $table->char('tanggal_kembali', 20)->nullable();
            $table->timestamps();

            $table->foreign('id_aset')
                  ->references('id')
                  ->on('assets')
                  ->onDelete('set null');

            $table->foreign('id_pegawai')
                  ->references('id')
                  ->on('employees')
                  ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_categorys');
    }
}
