<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AppsCountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('apps_countries')->insert([
        [
            'country_code' => 'ID',
            'country_name' => 'Indonesia',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
              'country_code' => 'AF',
              'country_name' => 'Afghanistan',
              'created_at' => '2019-12-26 00:00:00',
              'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
