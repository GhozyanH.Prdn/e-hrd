<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SalariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salaries')->insert([
            [
                'id' => '1',
                'Tahun' => '2020',
                'Bulan' => 'Januari',
                'ID_Pegawai' => '1',
                'Nama_Pegawai' => 'Randa',
                'Kehadiran' => '20',
                'Jumlah_Cuti_Tak_Dibayar' => '10000',
                'Gaji_Pensiun' => '20000',
                'Tunjangan_Harian' => '30000',
                'Tunjangan_Lainnya_Uang_Lembur_dsb' => '40000',
                'Cuti_Tidak_Dibayar' => '60000',
                'Bonus_Tak_Tetap' => '70000',
                'Jumlah_Penghasilan_Bruto' => '80000',
                'Jumlah_Pengurangan' => '90000',
                'PPh_21_sebulan' => '100000',
                'Uang_Pembayaran_Gaji' => '110000',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
            ],
            [
                'id' => '2',
                'Tahun' => '2020',
                'Bulan' => 'Januari',
                'ID_Pegawai' => '3',
                'Nama_Pegawai' => 'Dika',
                'Kehadiran' => '20',
                'Jumlah_Cuti_Tak_Dibayar' => '10000',
                'Gaji_Pensiun' => '20000',
                'Tunjangan_Harian' => '30000',
                'Tunjangan_Lainnya_Uang_Lembur_dsb' => '40000',
                'Cuti_Tidak_Dibayar' => '60000',
                'Bonus_Tak_Tetap' => '70000',
                'Jumlah_Penghasilan_Bruto' => '80000',
                'Jumlah_Pengurangan' => '90000',
                'PPh_21_sebulan' => '100000',
                'Uang_Pembayaran_Gaji' => '110000',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
            ],
            [
                'id' => '3',
                'Tahun' => '2020',
                'Bulan' => 'Januari',
                'ID_Pegawai' => '2',
                'Nama_Pegawai' => 'Miko',
                'Kehadiran' => '20',
                'Jumlah_Cuti_Tak_Dibayar' => '10000',
                'Gaji_Pensiun' => '20000',
                'Tunjangan_Harian' => '30000',
                'Tunjangan_Lainnya_Uang_Lembur_dsb' => '40000',
                'Cuti_Tidak_Dibayar' => '60000',
                'Bonus_Tak_Tetap' => '70000',
                'Jumlah_Penghasilan_Bruto' => '80000',
                'Jumlah_Pengurangan' => '90000',
                'PPh_21_sebulan' => '100000',
                'Uang_Pembayaran_Gaji' => '110000',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
            ],
        ]);
    }
}
