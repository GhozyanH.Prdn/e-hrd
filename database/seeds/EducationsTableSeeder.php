<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('educations')->insert([
            [
            'id_postulant' => '1',
            'jenis_pendidikan' => 'SMA',
            'nama_sekolah' => 'SMA 1',
            'jurusan' => 'IPA',
            'kota' => 'Malang',
            'tahun_mulai' => '2016',
            'tahun_selesai' => '2019',
            'kota' => 'Surabaya',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_postulant' => '1',
            'jenis_pendidikan' => 'SMA',
            'nama_sekolah' => 'SMA 1',
            'jurusan' => 'IPA',
            'tahun_mulai' => '2016',
            'tahun_selesai' => '2019',
            'kota' => 'Surabaya',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
