<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CutirequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuti_requests')->insert([
            [
                'tanggal_request' => '2019-12-26',
                'id_karyawan' => '2',
                'nama_atasan' => 'Dika',
                'id_cuti' => '1',
                'tanggal_cuti' => '2019-12-26',
                'quantity' => '0.5',
                'alasan' => 'llefefef',
                'keputusan_hrd' => 'menunggu',
                'catatan_hrd' => 'llefefef',
                'keputusan_atasan' => 'menunggu',
                'catatan_atasan' => 'llefefef',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],
                [

                'tanggal_request' => '2019-12-25 00:00:00',
                'id_karyawan' => '1',
                'nama_atasan' => 'Kelvin',
                'id_cuti' => '2',
                'tanggal_cuti' => '2019-12-26',
                'quantity' => '1',
                'alasan' => 'llefefef',
                'keputusan_hrd' => 'menunggu',
                'catatan_hrd' => 'llefefef',
                'keputusan_atasan' => 'menunggu',
                'catatan_atasan' => 'llefefef',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],
                [
                    'tanggal_request' => '2019-12-25 00:00:00',
                    'id_karyawan' => '3',
                    'nama_atasan' => 'Randa',
                    'id_cuti' => '2',
                    'tanggal_cuti' => '2019-12-26 00:00:00',
                    'quantity' => '3',
                    'alasan' => 'llefefef',
                    'keputusan_hrd' => 'menunggu',
                    'catatan_hrd' => 'llefefef',
                    'keputusan_atasan' => 'menunggu',
                    'catatan_atasan' => 'llefefef',
                    'created_at' => '2019-12-26 00:00:00',
                    'updated_at' => '2019-12-26 00:00:00',
                    ],

            ]);
    }
}
