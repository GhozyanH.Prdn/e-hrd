-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 24 Feb 2020 pada 05.50
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrd`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `apps_countries`
--

CREATE TABLE `apps_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_code` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name` char(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `apps_countries`
--

INSERT INTO `apps_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'ID', 'Indonesia'),
(2, 'AF', 'Afghanistan'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'DS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British Indian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CA', 'Canada'),
(41, 'CV', 'Cape Verde'),
(42, 'KY', 'Cayman Islands'),
(43, 'CF', 'Central African Republic'),
(44, 'TD', 'Chad'),
(45, 'CL', 'Chile'),
(46, 'CN', 'China'),
(47, 'CX', 'Christmas Island'),
(48, 'CC', 'Cocos (Keeling) Islands'),
(49, 'CO', 'Colombia'),
(50, 'KM', 'Comoros'),
(51, 'CD', 'Democratic Republic of the Congo'),
(52, 'CG', 'Republic of Congo'),
(53, 'CK', 'Cook Islands'),
(54, 'CR', 'Costa Rica'),
(55, 'HR', 'Croatia (Hrvatska)'),
(56, 'CU', 'Cuba'),
(57, 'CY', 'Cyprus'),
(58, 'CZ', 'Czech Republic'),
(59, 'DK', 'Denmark'),
(60, 'DJ', 'Djibouti'),
(61, 'DM', 'Dominica'),
(62, 'DO', 'Dominican Republic'),
(63, 'TP', 'East Timor'),
(64, 'EC', 'Ecuador'),
(65, 'EG', 'Egypt'),
(66, 'SV', 'El Salvador'),
(67, 'GQ', 'Equatorial Guinea'),
(68, 'ER', 'Eritrea'),
(69, 'EE', 'Estonia'),
(70, 'ET', 'Ethiopia'),
(71, 'FK', 'Falkland Islands (Malvinas)'),
(72, 'FO', 'Faroe Islands'),
(73, 'FJ', 'Fiji'),
(74, 'FI', 'Finland'),
(75, 'FR', 'France'),
(76, 'FX', 'France, Metropolitan'),
(77, 'GF', 'French Guiana'),
(78, 'PF', 'French Polynesia'),
(79, 'TF', 'French Southern Territories'),
(80, 'GA', 'Gabon'),
(81, 'GM', 'Gambia'),
(82, 'GE', 'Georgia'),
(83, 'DE', 'Germany'),
(84, 'GH', 'Ghana'),
(85, 'GI', 'Gibraltar'),
(86, 'GK', 'Guernsey'),
(87, 'GR', 'Greece'),
(88, 'GL', 'Greenland'),
(89, 'GD', 'Grenada'),
(90, 'GP', 'Guadeloupe'),
(91, 'GU', 'Guam'),
(92, 'GT', 'Guatemala'),
(93, 'GN', 'Guinea'),
(94, 'GW', 'Guinea-Bissau'),
(95, 'GY', 'Guyana'),
(96, 'HT', 'Haiti'),
(97, 'HM', 'Heard and Mc Donald Islands'),
(98, 'HN', 'Honduras'),
(99, 'HK', 'Hong Kong'),
(100, 'HU', 'Hungary'),
(101, 'IS', 'Iceland'),
(102, 'IN', 'India'),
(103, 'IM', 'Isle of Man'),
(104, 'ID', 'Indonesia'),
(105, 'IR', 'Iran (Islamic Republic of)'),
(106, 'IQ', 'Iraq'),
(107, 'IE', 'Ireland'),
(108, 'IL', 'Israel'),
(109, 'IT', 'Italy'),
(110, 'CI', 'Ivory Coast'),
(111, 'JE', 'Jersey'),
(112, 'JM', 'Jamaica'),
(113, 'JP', 'Japan'),
(114, 'JO', 'Jordan'),
(115, 'KZ', 'Kazakhstan'),
(116, 'KE', 'Kenya'),
(117, 'KI', 'Kiribati'),
(118, 'KP', 'Korea, Democratic People\'s Republic of'),
(119, 'KR', 'Korea, Republic of'),
(120, 'XK', 'Kosovo'),
(121, 'KW', 'Kuwait'),
(122, 'KG', 'Kyrgyzstan'),
(123, 'LA', 'Lao People\'s Democratic Republic'),
(124, 'LV', 'Latvia'),
(125, 'LB', 'Lebanon'),
(126, 'LS', 'Lesotho'),
(127, 'LR', 'Liberia'),
(128, 'LY', 'Libyan Arab Jamahiriya'),
(129, 'LI', 'Liechtenstein'),
(130, 'LT', 'Lithuania'),
(131, 'LU', 'Luxembourg'),
(132, 'MO', 'Macau'),
(133, 'MK', 'North Macedonia'),
(134, 'MG', 'Madagascar'),
(135, 'MW', 'Malawi'),
(136, 'MY', 'Malaysia'),
(137, 'MV', 'Maldives'),
(138, 'ML', 'Mali'),
(139, 'MT', 'Malta'),
(140, 'MH', 'Marshall Islands'),
(141, 'MQ', 'Martinique'),
(142, 'MR', 'Mauritania'),
(143, 'MU', 'Mauritius'),
(144, 'TY', 'Mayotte'),
(145, 'MX', 'Mexico'),
(146, 'FM', 'Micronesia, Federated States of'),
(147, 'MD', 'Moldova, Republic of'),
(148, 'MC', 'Monaco'),
(149, 'MN', 'Mongolia'),
(150, 'ME', 'Montenegro'),
(151, 'MS', 'Montserrat'),
(152, 'MA', 'Morocco'),
(153, 'MZ', 'Mozambique'),
(154, 'MM', 'Myanmar'),
(155, 'NA', 'Namibia'),
(156, 'NR', 'Nauru'),
(157, 'NP', 'Nepal'),
(158, 'NL', 'Netherlands'),
(159, 'AN', 'Netherlands Antilles'),
(160, 'NC', 'New Caledonia'),
(161, 'NZ', 'New Zealand'),
(162, 'NI', 'Nicaragua'),
(163, 'NE', 'Niger'),
(164, 'NG', 'Nigeria'),
(165, 'NU', 'Niue'),
(166, 'NF', 'Norfolk Island'),
(167, 'MP', 'Northern Mariana Islands'),
(168, 'NO', 'Norway'),
(169, 'OM', 'Oman'),
(170, 'PK', 'Pakistan'),
(171, 'PW', 'Palau'),
(172, 'PS', 'Palestine'),
(173, 'PA', 'Panama'),
(174, 'PG', 'Papua New Guinea'),
(175, 'PY', 'Paraguay'),
(176, 'PE', 'Peru'),
(177, 'PH', 'Philippines'),
(178, 'PN', 'Pitcairn'),
(179, 'PL', 'Poland'),
(180, 'PT', 'Portugal'),
(181, 'PR', 'Puerto Rico'),
(182, 'QA', 'Qatar'),
(183, 'RE', 'Reunion'),
(184, 'RO', 'Romania'),
(185, 'RU', 'Russian Federation'),
(186, 'RW', 'Rwanda'),
(187, 'KN', 'Saint Kitts and Nevis'),
(188, 'LC', 'Saint Lucia'),
(189, 'VC', 'Saint Vincent and the Grenadines'),
(190, 'WS', 'Samoa'),
(191, 'SM', 'San Marino'),
(192, 'ST', 'Sao Tome and Principe'),
(193, 'SA', 'Saudi Arabia'),
(194, 'SN', 'Senegal'),
(195, 'RS', 'Serbia'),
(196, 'SC', 'Seychelles'),
(197, 'SL', 'Sierra Leone'),
(198, 'SG', 'Singapore'),
(199, 'SK', 'Slovakia'),
(200, 'SI', 'Slovenia'),
(201, 'SB', 'Solomon Islands'),
(202, 'SO', 'Somalia'),
(203, 'ZA', 'South Africa'),
(204, 'GS', 'South Georgia South Sandwich Islands'),
(205, 'SS', 'South Sudan'),
(206, 'ES', 'Spain'),
(207, 'LK', 'Sri Lanka'),
(208, 'SH', 'St. Helena'),
(209, 'PM', 'St. Pierre and Miquelon'),
(210, 'SD', 'Sudan'),
(211, 'SR', 'Suriname'),
(212, 'SJ', 'Svalbard and Jan Mayen Islands'),
(213, 'SZ', 'Swaziland'),
(214, 'SE', 'Sweden'),
(215, 'CH', 'Switzerland'),
(216, 'SY', 'Syrian Arab Republic'),
(217, 'TW', 'Taiwan'),
(218, 'TJ', 'Tajikistan'),
(219, 'TZ', 'Tanzania, United Republic of'),
(220, 'TH', 'Thailand'),
(221, 'TG', 'Togo'),
(222, 'TK', 'Tokelau'),
(223, 'TO', 'Tonga'),
(224, 'TT', 'Trinidad and Tobago'),
(225, 'TN', 'Tunisia'),
(226, 'TR', 'Turkey'),
(227, 'TM', 'Turkmenistan'),
(228, 'TC', 'Turks and Caicos Islands'),
(229, 'TV', 'Tuvalu'),
(230, 'UG', 'Uganda'),
(231, 'UA', 'Ukraine'),
(232, 'AE', 'United Arab Emirates'),
(233, 'GB', 'United Kingdom'),
(234, 'US', 'United States'),
(235, 'UM', 'United States minor outlying islands'),
(236, 'UY', 'Uruguay'),
(237, 'UZ', 'Uzbekistan'),
(238, 'VU', 'Vanuatu'),
(239, 'VA', 'Vatican City State'),
(240, 'VE', 'Venezuela'),
(241, 'VN', 'Vietnam'),
(242, 'VG', 'Virgin Islands (British)'),
(243, 'VI', 'Virgin Islands (U.S.)'),
(244, 'WF', 'Wallis and Futuna Islands'),
(245, 'EH', 'Western Sahara'),
(246, 'YE', 'Yemen'),
(247, 'ZM', 'Zambia'),
(248, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `assets`
--

CREATE TABLE `assets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_aset` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_aset` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_beli` date NOT NULL,
  `gambar` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `assets`
--

INSERT INTO `assets` (`id`, `kode_aset`, `nama_aset`, `kategori`, `tanggal_beli`, `gambar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AS01', 'spanishl', 'Elektronik', '2019-12-26', 'ewdwdwdw', '2019-12-25 17:00:00', '2020-02-22 00:35:34', '2020-02-22 00:35:34'),
(2, 'AS02', 'spanish', 'Peralatan Kantor', '2019-12-26', 'wdwdw', '2019-12-25 17:00:00', '2020-02-22 00:35:39', '2020-02-22 00:35:39'),
(3, 'AS03', 'spanish', 'Lain-lain', '2019-12-26', 'wdwdw', '2019-12-25 17:00:00', '2020-02-22 00:35:43', '2020-02-22 00:35:43'),
(4, 'QWERTY/4/02-2020', 'Macbook Pro 2017', 'Elektronik', '2020-02-22', '1582357097.png', '2020-02-22 00:36:34', '2020-02-22 00:38:24', '2020-02-22 00:38:24'),
(5, 'QWERTY/5/02-2020', 'Macbook Pro 2017', 'Elektronik', '2020-02-22', '1582515553.jpeg', '2020-02-22 02:19:04', '2020-02-23 20:39:13', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `asset_transactions`
--

CREATE TABLE `asset_transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_peminjaman` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_aset` bigint(20) UNSIGNED NOT NULL,
  `id_pegawai` bigint(20) UNSIGNED NOT NULL,
  `tanggal_pinjam` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `asset_transactions`
--

INSERT INTO `asset_transactions` (`id`, `kode_peminjaman`, `id_aset`, `id_pegawai`, `tanggal_pinjam`, `tanggal_kembali`, `created_at`, `updated_at`) VALUES
(1, 'PA01', 1, 1, '2019-12-26', '2019-12-26', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 'PA02', 1, 2, '2019-12-26', '0000-00-00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(3, 'QWERTY/3/02-2020', 5, 1, '0000-00-00', '0000-00-00', '2020-02-23 21:11:32', '2020-02-23 21:11:32'),
(4, 'QWERTY/4/02-2020', 5, 1, '0000-00-00', '0000-00-00', '2020-02-23 21:14:47', '2020-02-23 21:14:47'),
(5, 'QWERTY/5/02-2020', 5, 1, '0000-00-00', '0000-00-00', '2020-02-23 21:15:18', '2020-02-23 21:15:18'),
(6, 'QWERTY/6/02-2020', 5, 2, '0000-00-00', '0000-00-00', '2020-02-23 21:22:03', '2020-02-23 21:22:03'),
(7, 'QWERTY/7/02-2020', 5, 2, '24-02-2020', '0000-00-00', '2020-02-23 21:22:46', '2020-02-23 21:22:46'),
(8, 'QWERTY/8/02-2020', 5, 2, '24-02-2020', '0000-00-00', '2020-02-23 21:22:58', '2020-02-23 21:22:58'),
(11, 'QWERTY/9/02-2020', 5, 1, '24-02-2020', '0000-00-00', '2020-02-23 21:24:38', '2020-02-23 21:24:38'),
(12, 'QWERTY/12/02-2020', 5, 2, '24-02-2020', '0000-00-00', '2020-02-23 21:36:12', '2020-02-23 21:36:12'),
(14, 'QWERTY/13/02-2020', 5, 1, '24-02-2020', '0000-00-00', '2020-02-23 21:38:27', '2020-02-23 21:38:27'),
(15, 'QWERTY/15/02-2020', 5, 1, '24-02-2020', NULL, '2020-02-23 21:41:25', '2020-02-23 21:41:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `awards`
--

CREATE TABLE `awards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `nama_prestasi` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `periode` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `awards`
--

INSERT INTO `awards` (`id`, `id_postulant`, `nama_prestasi`, `periode`, `created_at`, `updated_at`) VALUES
(1, 1, 'ruqyah', '2019', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'dwdwdw', 'Kepala Gugus', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `corporate_groups`
--

CREATE TABLE `corporate_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_corporate_group` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_corporate_group` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `corporate_groups`
--

INSERT INTO `corporate_groups` (`id`, `kode_corporate_group`, `nama_corporate_group`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'C01', 'picodio', '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti_categorys`
--

CREATE TABLE `cuti_categorys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_kategori_cuti` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_cuti` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cuti_categorys`
--

INSERT INTO `cuti_categorys` (`id`, `kode_kategori_cuti`, `jenis_cuti`, `keterangan`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CC01', 'sakit', 'gapapa', 1.00, '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL),
(2, 'CC02', 'izin', 'lalala', 2.00, '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti_requests`
--

CREATE TABLE `cuti_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal_request` datetime NOT NULL,
  `id_karyawan` bigint(20) UNSIGNED NOT NULL,
  `nama_atasan` char(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cuti` bigint(20) UNSIGNED NOT NULL,
  `tanggal_cuti` datetime NOT NULL,
  `lama_cuti` int(11) NOT NULL,
  `alasan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuti_terpakai` int(11) DEFAULT NULL,
  `keputusan_direktur` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan_direktur` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keputusan_hrd` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan_hrd` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keputusan_atasan` char(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan_atasan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `cuti_requests`
--

INSERT INTO `cuti_requests` (`id`, `tanggal_request`, `id_karyawan`, `nama_atasan`, `id_cuti`, `tanggal_cuti`, `lama_cuti`, `alasan`, `cuti_terpakai`, `keputusan_direktur`, `catatan_direktur`, `keputusan_hrd`, `catatan_hrd`, `keputusan_atasan`, `catatan_atasan`, `created_at`, `updated_at`) VALUES
(1, '2019-12-26 00:00:00', 2, 'Dika', 1, '2019-12-26 00:00:00', 2, 'llefefef', 2, 'menunggu', 'llefefef', 'menunggu', 'llefefef', 'menunggu', 'llefefef', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, '2019-12-25 00:00:00', 3, 'Kelvin', 2, '2019-12-26 00:00:00', 2, 'llefefef', 2, 'menunggu', 'llefefef', 'menunggu', 'llefefef', 'menunggu', 'llefefef', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_devisi` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_devisi` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `divisions`
--

INSERT INTO `divisions` (`id`, `kode_devisi`, `nama_devisi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'D01', 'pengembangan', '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL),
(2, 'D02', 'testing', '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `educations`
--

CREATE TABLE `educations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `jenis_pendidikan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_sekolah` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jurusan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_mulai` year(4) NOT NULL,
  `tahun_selesai` year(4) NOT NULL,
  `kota` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `educations`
--

INSERT INTO `educations` (`id`, `id_postulant`, `jenis_pendidikan`, `nama_sekolah`, `jurusan`, `tahun_mulai`, `tahun_selesai`, `kota`, `created_at`, `updated_at`) VALUES
(1, 1, 'SMA', 'SMA 1', 'IPA', 2016, 2019, 'Surabaya', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'SMA', 'SMA 1', 'IPA', 2016, 2019, 'Surabaya', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `emergencycontacts`
--

CREATE TABLE `emergencycontacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `nama` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hubungan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `emergencycontacts`
--

INSERT INTO `emergencycontacts` (`id`, `id_postulant`, `nama`, `hubungan`, `alamat`, `telepon`, `created_at`, `updated_at`) VALUES
(1, 1, 'dwi', 'Kepala', 'jaan tandes lor sby', '0823232424', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'Miko', 'saudara', 'jaan tandes lor sby', '0823232424', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `parent_path` char(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_karyawan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` bigint(20) UNSIGNED DEFAULT NULL,
  `id_grade` bigint(20) UNSIGNED DEFAULT NULL,
  `id_corporate` bigint(20) UNSIGNED DEFAULT NULL,
  `id_devisi` bigint(20) UNSIGNED DEFAULT NULL,
  `join_date` date NOT NULL,
  `status` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_corporate` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_account` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rekening_number` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_name` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_atasan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jatah_cuti` int(11) NOT NULL,
  `first_contract` date NOT NULL,
  `start_addendum1` date NOT NULL,
  `end_addendum1` date NOT NULL,
  `start_addendum2` date NOT NULL,
  `end_addendum2` date NOT NULL,
  `end_contract` date NOT NULL,
  `alasan_berhenti` char(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`id`, `id_postulant`, `nama`, `parent`, `parent_path`, `kode_karyawan`, `password`, `role`, `id_jabatan`, `id_grade`, `id_corporate`, `id_devisi`, `join_date`, `status`, `email_corporate`, `bank_account`, `rekening_number`, `account_name`, `nama_atasan`, `jatah_cuti`, `first_contract`, `start_addendum1`, `end_addendum1`, `start_addendum2`, `end_addendum2`, `end_contract`, `alasan_berhenti`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Randa', 1, 'Randa,Miko', 'randa@gmail.com', '$2y$10$qctr6rgsl27Ve/bNo7y8zenfSxiWeyOGfTPdTjRR/KHopfVZZNn7.', 'Admin', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'yM2YOP9CsQ@gmail.com', 'QwCHEDvuO7APOj5', '4Zkp3a8YS9iKCXX', '95Im3QPTovf5j79', 'Kelvin', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', '0Ol33yYirp1JseLFgracNw4G4Wz6YBWVx5nAkh8EbZYNRb0fmbOY1ctKrxJB', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 2, 'Miko', 2, 'Miko,Roby', 'miko@gmail.com', '$2y$10$QAxdy/AU53kvafaeTGbZUuQeYrioCjbONycK7OhhFlc7c.8lsk5H2', 'Pegawai', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'l7WzW3kh5x@gmail.com', 'X6oSNpB2wO8Ryli', 'bxYdBwG78tbzaBG', 'kLTTtfk8gaZ0icJ', 'Dika', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'W0BUqswDRqAQlbC', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(3, 3, 'Dika', 3, 'Miko,Dika', 'dika@gmail.com', '$2y$10$syuPSYPHr3v6l1CMFhibHeFa4PSS6/IKYFwfIvZ6WGyOzcW9vkPIe', 'Atasan', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'aiR92YuX2P@gmail.com', 'rbMoo7ZyyRKWKth', 'rNcY3wcubWzcTPw', 'w69g32b1MNRlP2V', 'Kelvin', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'DqqODH7kBWDzMb7', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(4, 4, 'Kelvin', 4, 'Miko,Kelvin', 'direktur@gmail.com', '$2y$10$6c9aecvkU.LVl5duLLoA1eCM1K7XywmpXZcKNTTJgCzqJctP5XrWi', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', '91qius4k2f@gmail.com', '6aHo9YsFDhNDwpK', 'pQLAVBQfmvWq41v', '1B37oUmUU7fv47R', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'VhlNLu3PoCmadOtqHcGJtkBdwghMO5LykwXPmPFcO3VhpdnZub4wjfLtlUNW', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(5, 5, 'Roby', 5, 'Miko,Roby', 'roby@gmail.com', '$2y$10$5b9QmysNsrYYonezALOcLeQ98g5lf00oRwZdG635K4GBjjHyCKKci', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'Mgik1p5WTR@gmail.com', 'sEQ9VLTvcilHSOc', 'zD6M3sCBW0YEpaX', '2X9Pp2s5f94QWyD', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'Ql0UltsvPSl7vG9', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(6, 6, 'Ghozyan', 6, 'Dika,Ghozyan', 'ghozyan@gmail.com', '$2y$10$MFCfAkBhJUxaC4gnGv8huueR2dNrc8pHJSb6YB9YdUeFZXTxRyw1S', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'onRZPQAz0P@gmail.com', 'GP5wypmWtPdrFCC', 'BBOUsYMDhqFZj6A', 'tViNRw8l2ftVyYQ', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'wFnhok0RYj0txrL', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(7, NULL, 'Randas', 7, 'Dika,Randas', 'randas@gmail.com', '$2y$10$rieaZfcuUfz5HPhAB4q7xuPSFNIbMsEdcCWpuk32Sg3qcvvUXjRQ6', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'oD2Rh5dDwP@gmail.com', 'IQiA7GghH8XaFhP', 'sIhiugMkrXLu91b', 'DgwgCKsd7onkI7L', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'eFNYc9f6vvQz9Md', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(8, 8, 'Karina', 8, 'Kelvin,Karina', 'karina@gmail.com', '$2y$10$5irFVGuoRV1HQiyn3TAUTe/1/wto2UYM4qO84Se1d0op.L.uVk606', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', '5xrJcJu7Zj@gmail.com', 'OtcrUA0e3LMhbzi', '2MY5JP1LjNTn84J', 'U2A2U0YBFGvf5qU', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'DTzIo0vhTzK3aXj', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(9, 9, 'Elita', 9, 'Kelvin,Elita', 'elita@gmail.com', '$2y$10$Y9arN.Gii3fwkC0WxCrvxuzHqO/G2st6BivAnn8N56HJm5KnqrqGS', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'pFSWWGJg1b@gmail.com', 'cpG8FcakW7lP3Xn', '8WYB43Hi77TBwE8', 'HI8fB8ltrlnmVMW', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', '1gKV0vooU2oQdOz', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(10, 10, 'Rizki', 10, 'Ghozyan,Rizki', 'rizki@gmail.com', '$2y$10$EfPbwLIwwydnSP1RIWW0Eeu05lgJrBEeXMO3uzXsk/bWCrC1zCBCW', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'vxJur1iT0v@gmail.com', '84zpr9REjomSUBd', 'SacI2Ulo2YYSgrN', 'm8tbOBX0SkR0nzy', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'CthIwmLbAGkWhBj', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(11, 11, 'Tevi', 11, 'Ghozyan,Tevi', 'tevi@gmail.com', '$2y$10$.L97mTLj8OHXAggGcorJ8.7y0plWiVVoOO0XSvF2PC2EcNIEhwE0K', 'Direktur', 1, 1, 1, 1, '2019-12-26', 'jomblo', 'viVfMvISgd@gmail.com', 'VClpUTAeltlEnBm', '4RTDyWJzZPCXQWb', 'GPcVfKPr5MpImPi', '-', 10, '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', '2019-12-26', 'aafafa', 'H8TDpYiKaBOYj26', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `familys`
--

CREATE TABLE `familys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `hubungan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usia` int(11) NOT NULL,
  `pendidikan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `familys`
--

INSERT INTO `familys` (`id`, `id_postulant`, `hubungan`, `nama`, `jenis_kelamin`, `usia`, `pendidikan`, `pekerjaan`, `created_at`, `updated_at`) VALUES
(1, 1, 'adik', 'Indah Kusuma', 'perempuan', 21, 's1', 'swasta', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'kakak', 'Indah dwi', 'perempuan', 21, 's1', 'swasta', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_grade` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_grade` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `grades`
--

INSERT INTO `grades` (`id`, `kode_grade`, `nama_grade`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'G01', 'sersan', '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `job_historys`
--

CREATE TABLE `job_historys` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `nama_perusahaan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posisi` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `alasan_berhenti` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `job_historys`
--

INSERT INTO `job_historys` (`id`, `id_postulant`, `nama_perusahaan`, `posisi`, `tanggal_mulai`, `tanggal_selesai`, `alasan_berhenti`, `created_at`, `updated_at`) VALUES
(1, 1, 'spanish', 'Kepala Gugus', '2019-12-26', '2019-12-26', '2000', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'spanish', 'Kepala Gugus', '2019-12-26', '2019-12-26', '2000', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `language` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `oral` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `written` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reading` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `languages`
--

INSERT INTO `languages` (`id`, `id_postulant`, `language`, `oral`, `written`, `reading`, `created_at`, `updated_at`) VALUES
(1, 1, 'spanish', 'Kepala Gugus', '2000', '2000', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'spanish', 'Kepala Gugus', '2000', '2000', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `organizations`
--

CREATE TABLE `organizations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `nama_organisasi` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_mulai` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun_selesai` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `organizations`
--

INSERT INTO `organizations` (`id`, `id_postulant`, `nama_organisasi`, `jabatan`, `tahun_mulai`, `tahun_selesai`, `created_at`, `updated_at`) VALUES
(1, 1, 'Pramuka Unipma', 'Kepala Gugus', '2000', '2005', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'Osis Sma Bati', 'Ketua', '2000', '2005', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `personality_tests`
--

CREATE TABLE `personality_tests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `characterN` int(11) NOT NULL,
  `characterG` int(11) NOT NULL,
  `characterA` int(11) NOT NULL,
  `characterL` int(11) NOT NULL,
  `characterP` int(11) NOT NULL,
  `characterI` int(11) NOT NULL,
  `characterT` int(11) NOT NULL,
  `characterV` int(11) NOT NULL,
  `characterX` int(11) NOT NULL,
  `characterS` int(11) NOT NULL,
  `characterB` int(11) NOT NULL,
  `characterO` int(11) NOT NULL,
  `characterR` int(11) NOT NULL,
  `characterD` int(11) NOT NULL,
  `characterC` int(11) NOT NULL,
  `characterZ` int(11) NOT NULL,
  `characterE` int(11) NOT NULL,
  `characterK` int(11) NOT NULL,
  `characterF` int(11) NOT NULL,
  `characterW` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `personality_tests`
--

INSERT INTO `personality_tests` (`id`, `id_postulant`, `characterN`, `characterG`, `characterA`, `characterL`, `characterP`, `characterI`, `characterT`, `characterV`, `characterX`, `characterS`, `characterB`, `characterO`, `characterR`, `characterD`, `characterC`, `characterZ`, `characterE`, `characterK`, `characterF`, `characterW`, `created_at`, `updated_at`) VALUES
(1, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, '2020-02-19 23:28:54', '2020-02-19 23:28:54'),
(2, 1, 7, 5, 7, 5, 6, 6, 6, 5, 7, 6, 5, 4, 6, 6, 6, 4, 6, 5, 5, 6, '2020-02-22 00:55:47', '2020-02-22 00:55:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_jabatan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_jabatan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `positions`
--

INSERT INTO `positions` (`id`, `kode_jabatan`, `nama_jabatan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'J01', 'lead developer', '2019-12-25 17:00:00', '2019-12-25 17:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `postulants`
--

CREATE TABLE `postulants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_lengkap` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posisi_yang_dilamar` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tempat_lahir` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `no_ktp` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_perkawinan` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agama` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kebangsaan` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tanggal_join` datetime DEFAULT NULL,
  `npwp` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_domisili` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_domisili` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat_ktp` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kota_ktp` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kendaraan` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `laptop` char(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gaji_diharap` int(11) DEFAULT NULL,
  `anak_ke` int(11) DEFAULT NULL,
  `saudara` int(11) DEFAULT NULL,
  `pencapaian_sudah_dilakukan` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pencapaian_yang_diinginkan` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_lamaran` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alasan` char(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `postulants`
--

INSERT INTO `postulants` (`id`, `nama_lengkap`, `posisi_yang_dilamar`, `tempat_lahir`, `tanggal_lahir`, `no_ktp`, `jenis_kelamin`, `status_perkawinan`, `agama`, `kebangsaan`, `tanggal_join`, `npwp`, `alamat_domisili`, `kota_domisili`, `alamat_ktp`, `kota_ktp`, `telepon`, `email`, `password`, `kendaraan`, `laptop`, `gaji_diharap`, `anak_ke`, `saudara`, `pencapaian_sudah_dilakukan`, `pencapaian_yang_diinginkan`, `foto`, `catatan`, `status_lamaran`, `alasan`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Randa', 'copy writer', 'Surabaya', '1998-03-21', 'A52MiUdlmcfoOhF', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'randa@gmail.com', '$2y$10$kfEStC46K5jO8pOUHXseeOyahs6KX4nHYQ0JSOerseO4jkdbsdk6G', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 'Miko', 'copy writer', 'Surabaya', '1998-03-21', 'k2wLeHe0vjXDQLs', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'miko@gmail.com', '$2y$10$Wzwbjln4lp45vKiVeo5lSu2iIb.h/F47frbx.swkl2BR.QimSwFwK', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(3, 'Dika', 'copy writer', 'Surabaya', '1998-03-21', '7CknYZSkVXFa6l5', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'dika@gmail.com', '$2y$10$62GzWhPFUXI4U9QmWpssD.DqAgiqzuiSJ56An2N0/BaYALfRqF1VW', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(4, 'Kelvin', 'copy writer', 'Surabaya', '1998-03-21', 'd2GGZu1N264Mu8l', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'direktur@gmail.com', '$2y$10$2CEXjDYk70QfIzvzV4EPsO8rjXAW2mIzK6eU4Z.A0sjO8QDFrBvTK', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(5, 'Roby', 'copy writer', 'Surabaya', '1998-03-21', '9VxBN0nyW4na6wQ', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'roby@gmail.com', '$2y$10$JIFZXR64DZd3zCe1nZ7JM.LwDsXvbdcKqNer8sSYHUwi20X4mo2a2', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'Diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(6, 'Ghozyan', 'copy writer', 'Surabaya', '1998-03-21', 'zBYt66CYdDaQ1Ps', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'ghozyan@gmail.com', '$2y$10$9Y7QZ1ogqtAExJGmE4NXn.6h0VrBmvgCt98GsXTgnTJQkjcNfQ..i', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'Ditolak : Good', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(8, 'Karina', 'copy writer', 'Surabaya', '1998-03-21', 'ozLzbpv5vwzY80w', 'P', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'karina@gmail.com', '$2y$10$Knpu5g5PLEzk/TqPHAArAOiYOsyGf5FN7L6T7UokTjmw2FNoopS6O', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'Diterima', 'kepo', '2019-12-25 17:00:00', '2020-02-19 23:10:21', '2019-12-25 17:00:00'),
(9, 'Elita', 'copy writer', 'Surabaya', '1998-03-21', 'M3gSKT6HL1ryTWm', 'P', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'elita@gmail.com', '$2y$10$Mya5o5j.AApyqk1sE/9jB.IfmJZU7ad3fV2VkR6Bvo/5yFvjY7tY.', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', '', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(10, 'Rizki', 'copy writer', 'Surabaya', '1998-03-21', '0nf1rUolE6tyjs6', 'L', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'rizki@gmail.com', '$2y$10$sbFJv4RlESbSq.erRt40auabC5Q25jruCx439Hq7uGF.fkUm5uHBS', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(11, 'Tevi', 'copy writer', 'Surabaya', '1998-03-21', 'XoTRMJzEvs1btMo', 'P', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'tevi@gmail.com', '$2y$10$NY/ifcmEAKgoAHTXJxHw.eL9zYy/JMYxu5WfGMsscMnDrmNCtsf/G', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(12, 'Niken Andriani', 'copy writer', 'Surabaya', '1998-03-21', 'rDHS5C9KnUukObe', 'P', 'menikah', 'Islam', 'Islam', '1998-03-21 00:00:00', '081273338544', 'Ketintang Timur PTT', 'Surabaya', 'Jogorogo Ngawi', 'Ngawi', '081273338544', 'niken@gmail.com', '$2y$10$n3C5a/7ftAjw9zCJlDFfj.LS2YrYZIYmw7mVQmitfuJpHENPI91pC', 'Surabaya', 'ada', 500000, 1, 2, 'Sarjana', 'Sarjana', NULL, 'diterima', 'diterima', 'kepo', '2019-12-25 17:00:00', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `references`
--

CREATE TABLE `references` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `nama` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perusahaan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `references`
--

INSERT INTO `references` (`id`, `id_postulant`, `nama`, `perusahaan`, `jabatan`, `telepon`, `created_at`, `updated_at`) VALUES
(1, 1, 'Heru', 'PT Indah Kusuma', 'Web Programmer', '081273338544', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'Hina', 'PT Indah Kusuma', 'HRD', '081273338544', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `superiors`
--

CREATE TABLE `superiors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_atasan` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_atasan` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jabatan` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `trainings`
--

CREATE TABLE `trainings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_postulant` bigint(20) UNSIGNED NOT NULL,
  `jenis_pelatihan` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_penyelenggara` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `tempat` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `trainings`
--

INSERT INTO `trainings` (`id`, `id_postulant`, `jenis_pelatihan`, `nama_penyelenggara`, `tanggal_mulai`, `tanggal_selesai`, `tempat`, `created_at`, `updated_at`) VALUES
(1, 1, 'Heru', 'PT Indah Kusuma', '2019-12-26', '2019-12-26', 'Web Programmer', '2019-12-25 17:00:00', '2019-12-25 17:00:00'),
(2, 1, 'Heru', 'PT Indah Kusuma', '2019-12-26', '2019-12-26', 'Web Programmer', '2019-12-25 17:00:00', '2019-12-25 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `apps_countries`
--
ALTER TABLE `apps_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assets_kode_aset_unique` (`kode_aset`);

--
-- Indeks untuk tabel `asset_transactions`
--
ALTER TABLE `asset_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_transactions_id_aset_foreign` (`id_aset`),
  ADD KEY `asset_transactions_id_pegawai_foreign` (`id_pegawai`);

--
-- Indeks untuk tabel `awards`
--
ALTER TABLE `awards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `awards_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `corporate_groups`
--
ALTER TABLE `corporate_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cuti_categorys`
--
ALTER TABLE `cuti_categorys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cuti_categorys_kode_kategori_cuti_unique` (`kode_kategori_cuti`);

--
-- Indeks untuk tabel `cuti_requests`
--
ALTER TABLE `cuti_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cuti_requests_id_karyawan_foreign` (`id_karyawan`),
  ADD KEY `cuti_requests_id_cuti_foreign` (`id_cuti`);

--
-- Indeks untuk tabel `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `divisions_kode_devisi_unique` (`kode_devisi`);

--
-- Indeks untuk tabel `educations`
--
ALTER TABLE `educations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `educations_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `emergencycontacts`
--
ALTER TABLE `emergencycontacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emergencycontacts_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_id_unique` (`id`),
  ADD UNIQUE KEY `employees_kode_karyawan_unique` (`kode_karyawan`),
  ADD UNIQUE KEY `employees_id_postulant_unique` (`id_postulant`),
  ADD KEY `employees_id_jabatan_foreign` (`id_jabatan`),
  ADD KEY `employees_id_grade_foreign` (`id_grade`),
  ADD KEY `employees_id_corporate_foreign` (`id_corporate`),
  ADD KEY `employees_id_devisi_foreign` (`id_devisi`);

--
-- Indeks untuk tabel `familys`
--
ALTER TABLE `familys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `familys_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `grades_kode_grade_unique` (`kode_grade`);

--
-- Indeks untuk tabel `job_historys`
--
ALTER TABLE `job_historys`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_historys_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `languages_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organizations_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `personality_tests`
--
ALTER TABLE `personality_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personality_tests_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `positions_kode_jabatan_unique` (`kode_jabatan`);

--
-- Indeks untuk tabel `postulants`
--
ALTER TABLE `postulants`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `references`
--
ALTER TABLE `references`
  ADD PRIMARY KEY (`id`),
  ADD KEY `references_id_postulant_foreign` (`id_postulant`);

--
-- Indeks untuk tabel `superiors`
--
ALTER TABLE `superiors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `superiors_kode_atasan_unique` (`kode_atasan`),
  ADD KEY `superiors_id_jabatan_foreign` (`id_jabatan`);

--
-- Indeks untuk tabel `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainings_id_postulant_foreign` (`id_postulant`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `apps_countries`
--
ALTER TABLE `apps_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT untuk tabel `assets`
--
ALTER TABLE `assets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `asset_transactions`
--
ALTER TABLE `asset_transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `awards`
--
ALTER TABLE `awards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `corporate_groups`
--
ALTER TABLE `corporate_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `cuti_categorys`
--
ALTER TABLE `cuti_categorys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `cuti_requests`
--
ALTER TABLE `cuti_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `educations`
--
ALTER TABLE `educations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `emergencycontacts`
--
ALTER TABLE `emergencycontacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `familys`
--
ALTER TABLE `familys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `job_historys`
--
ALTER TABLE `job_historys`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `personality_tests`
--
ALTER TABLE `personality_tests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `postulants`
--
ALTER TABLE `postulants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `references`
--
ALTER TABLE `references`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `superiors`
--
ALTER TABLE `superiors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `asset_transactions`
--
ALTER TABLE `asset_transactions`
  ADD CONSTRAINT `asset_transactions_id_aset_foreign` FOREIGN KEY (`id_aset`) REFERENCES `assets` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `asset_transactions_id_pegawai_foreign` FOREIGN KEY (`id_pegawai`) REFERENCES `educations` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `awards`
--
ALTER TABLE `awards`
  ADD CONSTRAINT `awards_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `cuti_requests`
--
ALTER TABLE `cuti_requests`
  ADD CONSTRAINT `cuti_requests_id_cuti_foreign` FOREIGN KEY (`id_cuti`) REFERENCES `cuti_categorys` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cuti_requests_id_karyawan_foreign` FOREIGN KEY (`id_karyawan`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `educations`
--
ALTER TABLE `educations`
  ADD CONSTRAINT `educations_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `emergencycontacts`
--
ALTER TABLE `emergencycontacts`
  ADD CONSTRAINT `emergencycontacts_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_id_corporate_foreign` FOREIGN KEY (`id_corporate`) REFERENCES `corporate_groups` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employees_id_devisi_foreign` FOREIGN KEY (`id_devisi`) REFERENCES `divisions` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employees_id_grade_foreign` FOREIGN KEY (`id_grade`) REFERENCES `grades` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employees_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `positions` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employees_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `familys`
--
ALTER TABLE `familys`
  ADD CONSTRAINT `familys_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `job_historys`
--
ALTER TABLE `job_historys`
  ADD CONSTRAINT `job_historys_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `languages`
--
ALTER TABLE `languages`
  ADD CONSTRAINT `languages_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `personality_tests`
--
ALTER TABLE `personality_tests`
  ADD CONSTRAINT `personality_tests_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `references`
--
ALTER TABLE `references`
  ADD CONSTRAINT `references_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `superiors`
--
ALTER TABLE `superiors`
  ADD CONSTRAINT `superiors_id_jabatan_foreign` FOREIGN KEY (`id_jabatan`) REFERENCES `positions` (`id`) ON DELETE CASCADE;

--
-- Ketidakleluasaan untuk tabel `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_id_postulant_foreign` FOREIGN KEY (`id_postulant`) REFERENCES `postulants` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
