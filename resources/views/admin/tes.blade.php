@extends('layouts.master')

@section('content')
@if(Auth::user()->role=='Admin' || Auth::user()->role=='Direktur')
<!-- page content -->
  <!-- top tiles -->

  <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/cuti') }}">
          @else
          <a href="{{ url('/direktur/persetujuan_direktur') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-user-times"></i> Cuti Request</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top">Belum Disetujui HRD</span>
              <div class="count blue">{{$hitcutadm}} <small>Request</small></div>
            </div>
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top">Belum Disetujui Atasan</span>
              <div class="count">{{$hitcutats}} <small>Request</small></div>
            </div>
          </div>
          </a>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/interview') }}">
          @else
          <a href="{{ url('/direktur/interviewdir') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-envelope"></i> Interview</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-8 col-sm-8  tile_stats_count">
              <span class="count_top">Interview Baru</span>
              <div class="count blue">{{$hitpel}} <small>Pelamar Baru</small></div>
            </div>
          </div>
          </a>
        </div>
      </div>

      <div class="col-md-6 col-sm-6">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/employees') }}">
          @else
          <a href="{{ url('/direktur/employeesdir') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-birthday-cake"></i> Akan Ulang Tahun</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
              @foreach ($karultah as $ultah)
              <h6>- {{$ultah -> tanggal_lahir}}, {{$ultah -> nama_lengkap}}</h6>
              @endforeach
            </div>
          </div>
          </a>
        </div>
      </div>

      <div class="col-md-6 col-sm-6">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/employees') }}">
          @else
          <a href="{{ url('/direktur/employeesdir') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-bell"></i> Masa Kontrak Akan Berakhir</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
              @foreach ($konkar as $kk)
              <h6>- {{$kk -> end_contract}}, {{$kk -> nama}}</h6>
              @endforeach
              @foreach ($konkar2 as $kkk)
              <h6>- {{$kkk -> end_addendum1}}, {{$kkk -> nama}}</h6>
              @endforeach
              @foreach ($konkar3 as $kkkk)
              <h6>- {{$kkkk -> end_addendum2}}, {{$kkkk -> nama}}</h6>
              @endforeach
            </div>
          </div>
          </a>
        </div>
      </div>

    </div>
  </div>

  <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-6 col-sm-6  ">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/employees') }}">
          @else
          <a href="{{ url('/direktur/employeesdir') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-users"></i> Karyawan</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top"><i class="fa fa-male"></i> Pria</span>
              <div class="count blue">{{$jenkell}} <small>Orang</small></div>
            </div>
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top"><i class="fa fa-female"></i> Wanita</span>
              <div class="count">{{$jenkelp}} <small>Orang</small></div>
            </div>
          </div>
          <div class="x_content">
            <div class="panel">
              <div id="karyawan"></div>
            </div>
          </div>
          </a>
        </div>
      </div>

      <div class="col-md-6 col-sm-6  ">
        <div class="x_panel">
          @if(Auth::user()->role=='Admin')
          <a href="{{ url('/admin/assets') }}">
          @else
          <a href="{{ url('/direktur/assetsdir') }}">
          @endif
          <div class="x_title">
            <h2><i class="fa fa-gamepad"></i> Asset</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="panel">
              <div id="karyawan"></div>
            </div>
          </div>
          <div class="tile_count">
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top">Total Aset Perusahaan</span>
              <div class="count blue">{{$hitast}} <small>Unit</small></div>
            </div>
            <div class="col-md-6 col-sm-6  tile_stats_count">
              <span class="count_top">Total Aset Dipinjam</span>
              <div class="count">{{$hitastpin}} <small>Unit</small></div>
            </div>
          </div>
          <div class="x_content">
            <div class="panel">
              <div id="asset"></div>
            </div>
          </div>
          </a>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-sitemap"></i> Struktur Perusahaan PT Davinti Indonesia</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
              <div class="panel">
                <div id="container"></div>
              </div>
            </div>
          </div>
          @php
          $s = $tole;
          //["Randa,Miko","Miko,Robi","Miko,Dika","Miko,Kelvin","Miko,Roby","Dika,Ghozyan","Dika,Randas","Kelvin,Karina","Kelvin,Elita","Ghozyan,Rizki","Ghozyan,Tevi"]

          $ss = str_replace('",','"],',$s);
          $sss = str_replace(',"',',[",',$ss);
          $ssss = str_replace('",','',$sss);
          $sssss = str_replace('["','[',$ssss);
          $ssssss = str_replace('[','["',$sssss);
          $sssssss = str_replace(',','","',$ssssss);
          $ssssssss = str_replace(']"',']',$sssssss);
          $sssssssss = str_replace('"[','[',$ssssssss);

          $k = count($tes);
          for($i=0;$i<$k;$i++){
            $rs[] = $tes[$i];
          }

          $rs1 = implode(",",$rs);
          $r = str_replace('"','',$rs1);
          $rr = str_replace(':',":'",$r);
          $rrr = str_replace(',',"',",$rr);
          $rrrr = str_replace('}',"'}",$rrr);
          $rrrrr = str_replace("}'","}",$rrrr);
          @endphp

        </div>
      </div>

    </div>
  </div>

@elseif(Auth::user()->role=='Atasan')
  <!-- <div class="animated flipInY col-lg-12 col-md-12 col-sm-12  ">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i>
    </div>
    <div class="count">Anda Atasan</div>
    </div>
  </div> -->

  <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-user-times"></i> Cuti Request</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
            <a href="{{ url('/atasan/persetujuan_atasan') }}">
              <span class="count_top">Belum Anda Putuskan</span>
              <div class="count">{{$hitcutatsmy}} <small>Request</small></div>
            </a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-sitemap"></i> Struktur Perusahaan PT Davinti Indonesia</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
              <div class="panel">
                <div id="container"></div>
              </div>
            </div>
          </div>
          @php
          $s = $tole;
          //["Randa,Miko","Miko,Robi","Miko,Dika","Miko,Kelvin","Miko,Roby","Dika,Ghozyan","Dika,Randas","Kelvin,Karina","Kelvin,Elita","Ghozyan,Rizki","Ghozyan,Tevi"]

          $ss = str_replace('",','"],',$s);
          $sss = str_replace(',"',',[",',$ss);
          $ssss = str_replace('",','',$sss);
          $sssss = str_replace('["','[',$ssss);
          $ssssss = str_replace('[','["',$sssss);
          $sssssss = str_replace(',','","',$ssssss);
          $ssssssss = str_replace(']"',']',$sssssss);
          $sssssssss = str_replace('"[','[',$ssssssss);

          $k = count($tes);
          for($i=0;$i<$k;$i++){
            $rs[] = $tes[$i];
          }

          $rs1 = implode(",",$rs);
          $r = str_replace('"','',$rs1);
          $rr = str_replace(':',":'",$r);
          $rrr = str_replace(',',"',",$rr);
          $rrrr = str_replace('}',"'}",$rrr);
          $rrrrr = str_replace("}'","}",$rrrr);
          @endphp

        </div>
      </div>

    </div>
  </div>

@else
  <!-- <div class="animated flipInY col-lg-12 col-md-12 col-sm-12  ">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i>
    </div>
    <div class="count">Anda Pegawai</div>
    </div>
  </div> -->

  <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="x_panel">
          <div class="x_title">
            <h2><i class="fa fa-sitemap"></i> Struktur Perusahaan PT Davinti Indonesia</h2>
            <div class="clearfix"></div>
          </div>
          <div class="tile_count">
            <div class="col-md-12 col-sm-12  tile_stats_count">
              <div class="panel">
                <div id="container"></div>
              </div>
            </div>
          </div>
          @php
          $s = $tole;
          //["Randa,Miko","Miko,Robi","Miko,Dika","Miko,Kelvin","Miko,Roby","Dika,Ghozyan","Dika,Randas","Kelvin,Karina","Kelvin,Elita","Ghozyan,Rizki","Ghozyan,Tevi"]

          $ss = str_replace('",','"],',$s);
          $sss = str_replace(',"',',[",',$ss);
          $ssss = str_replace('",','',$sss);
          $sssss = str_replace('["','[',$ssss);
          $ssssss = str_replace('[','["',$sssss);
          $sssssss = str_replace(',','","',$ssssss);
          $ssssssss = str_replace(']"',']',$sssssss);
          $sssssssss = str_replace('"[','[',$ssssssss);

          $k = count($tes);
          for($i=0;$i<$k;$i++){
            $rs[] = $tes[$i];
          }

          $rs1 = implode(",",$rs);
          $r = str_replace('"','',$rs1);
          $rr = str_replace(':',":'",$r);
          $rrr = str_replace(',',"',",$rr);
          $rrrr = str_replace('}',"'}",$rrr);
          $rrrrr = str_replace("}'","}",$rrrr);
          @endphp

        </div>
      </div>

    </div>
  </div>
@endif
@endsection

@section('chartKar')
<script src="https://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">
Highcharts.chart('karyawan', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: 'Total<br>Karyawan<br>Aktif<br>{!!json_encode($hitkary)!!} Orang',
        align: 'center',
        verticalAlign: 'middle',
        y: 60
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Karyawan',
        innerSize: '50%',
        data: [
                ['Tetap', {!!json_encode($hitkarytet)!!}],
                ['Kontrak', {!!json_encode($hitkarykon)!!}]
              ]
    }],
    subtitle: {
        text: 'Total Karyawan Dengan Status Resign Sebanyak {!!json_encode($hitkaryres)!!} Orang'
    }
});
</script>
@endsection

@section('org')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/sankey.js"></script>
<script src="https://code.highcharts.com/modules/organization.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script type="text/javascript">
        Highcharts.chart('container', {

            chart: {
                height: 600,
                inverted: true
            },

            title: {
                text: 'Struktur Organisasi <br> PT Davinti Indonesia'
            },

            series: [{
                type: 'organization',
                name: 'PT Davinti Indonesia',
                keys: ['from', 'to'],
                data: [
                  @php
                    print_r($sssssssss);
                  @endphp
                ],
                levels: [{
                    level: 0,
                    color: '##28B463',
                    dataLabels: {
                        color: '#ECF0F1'
                    },
                    height: 25
                }, {
                    level: 1,
                    color: 'silver',
                    dataLabels: {
                        color: 'black'
                    },
                    height: 25
                }, {
                    level: 2,
                    color: '#980104'
                }, {
                    level: 4,
                    color: '#359154'
                },
                {
                    level: 5,
                    color: '#BA4A00'
                },
                {
                    level: 6,
                    color: '#D4AC0D'
                },
                {
                    level: 7,
                    color: '#73C6B6'
                },
                {
                    level: 8,
                    color: '#8E44AD'
                },
                {
                    level: 9,
                    color: '#E74C3C'
                }
              ],
                nodes: [
                  @php
                    echo $str = str_replace(array("\r","\n"),"",$rrrrr);
                  @endphp
                ],
                colorByPoint: false,
                color: '#007ad0',
                dataLabels: {
                    color: 'white'
                },
                borderColor: 'white',
                nodeWidth: 65
            }],
            tooltip: {
                outside: true
            },
            exporting: {
                allowHTML: true,
                sourceWidth: 800,
                sourceHeight: 600
            }

        });
</script>
@endsection

@section('chartasset')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script type="text/javascript">
  Highcharts.chart('asset', {
    chart: {
        type: 'pie'
    },

    title: {
        text: ''
    },

    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "Asset",
            colorByPoint: true,
            data: [
              ['Elektronik', {!!json_encode($hitastel)!!}],
              ['Peralatan Kantor', {!!json_encode($hitastper)!!}],
              ['Lain-Lain', {!!json_encode($hitastla)!!}]
            ]
        }
    ]
  });
</script>
@endsection
