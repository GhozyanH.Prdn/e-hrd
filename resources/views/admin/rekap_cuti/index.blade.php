@extends('layouts.master')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>PT Davinti Indonesia</h3>
  </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Rekapan Cuti Bulan {{date('m')}}</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
        <!-- <form action="/admin/rekapcuti" method="post" enctype="multipart/form-data" class="form-label-left input_mask">
                      @csrf
                      <div class="col-md-6 col-sm-6  form-group has-feedback">
                        <select name="bulan" class="form-control">
                          @foreach($bulan as $role)
                            <option value="{{$role}}">{{$role}}</option>
                           @endForeach
                        </select>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <div class="col-md-9 col-sm-9  offset-md-3">
                          <button type="submit" class="btn btn-success pull-right">Cek</button>
                        </div>
                      </div>

                    </form> -->
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Karyawan</th>
                      <th>Quantity Cuti (Terpotong) Diterima Bulan Ini</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($result as $v => $ast )
                    <tr>
                      <td>{{$loop -> iteration}}</td>
                      <td>{{$ast -> nama}}</td>
                      <td>{{$ast -> jumlah}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12 col-sm-12 ">
      <div class="x_panel">
        <div class="x_title">
          <h2>Rekapan Cuti Tahun {{date('Y')}}</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
          <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
                <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Karyawan</th>
                      <th>Jatah Cuti</th>
                      <th>Quantity Cuti (Terpotong) Diterima Tahun Ini</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($rekap as $v => $rek )
                    <tr>
                      <td>{{$loop -> iteration}}</td>
                      <td>{{$rek -> nama}}</td>
                      <td>{{$rek -> jatah_cuti}}</td>
                      <td>{{$rek -> jumlah}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!-- --------- MODAL --------- -->



@endsection
