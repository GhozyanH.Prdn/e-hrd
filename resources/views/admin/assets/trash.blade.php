@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Aset <small>Data Trash</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Aplikasi Aset  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
        <a href="admin/restore_all" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-default"><i class="fa fa-undo"></i>Restore All</a>
        <a href="admin/deleted_all" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-danger"><i class="fa fa-trash"></i>Deleted All</a>
        @if (session('status'))
        <div class="alert alert-success alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
        </div>
        @endif

        @if (session('edit'))
        <div class="alert alert-warning alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Success!</strong> Data Aset Berhasil Diubah.
        </div>
        @endif

        @if (session('delete'))
        <div class="alert alert-danger alert-dismissible " role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <strong>Success!</strong> Data Aset Berhasil Dihapus.
        </div>
        @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Kode Aset</th>
            <th>Nama Aset</th>
            <th>Tanggal Entry</th>
            <th>Tanggal Delete</th>
            <th>Action</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($asset  as $ast )
          <tr>
            <td>{{$ast -> kode_aset}}</td>
            <td>{{$ast -> nama_aset}}</td>
            <td>{{$ast -> created_at}}</td>
            <td>{{$ast -> deleted_at}}</td>
            <td>
              <a href="/admin/assets/restore/{{$ast->id}}" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-success">Restore</a>
              <a href="/admin/assets/deleted_permanent/{{$ast->id}}" onclick="return confirm('Are you sure?')" class="btn btn-custon-four btn-danger">Delete Permanent</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
