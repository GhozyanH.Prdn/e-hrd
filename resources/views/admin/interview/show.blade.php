@extends('layouts.master')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>Karyawan Profile</h3>
  </div>
  <!-- <div class="title_right">
    <div class="col-md-5 col-sm-5  form-group pull-right top_search">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-secondary" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div> -->
</div>

            <div class="clearfix"></div>
            <div class="row">
                          <div class="col-md-12 col-sm-12 ">
                            <div class="x_panel">
                              <div class="x_title">
                                <h2>User Report <small>Activity report</small></h2>
                                <ul class="nav navbar-right panel_toolbox">
                                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                  </li>
                                  <!-- <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                      </div>
                                  </li>
                                  <li><a class="close-link"><i class="fa fa-close"></i></a>
                                  </li> -->
                                </ul>
                                <div class="clearfix"></div>
                              </div>

                              <div class="x_content">
                                <div class="col-md-2 col-sm-2 profile_left">
                                  <div class="profile_img">
                                    <div id="crop-avatar">
                                      <!-- Current avatar -->
                                      <img src="{{url('images/personal/' . $interview->foto)}}" alt="..." class="img-responsive avatar-view img-circle profile_img">
                                    </div>
                                  </div>
                                  <h3>{{$interview -> nama_lengkap}}</h3>

                                  <ul class="list-unstyled user_data">
                                    <li>
                                      <i class="glyphicon glyphicon-folder-open user-profile-icon"><h5>{{$interview -> posisi_yang_dilamar}}</h5></i>
                                    </li>
                                  </ul>

                                  <!-- <a class="btn btn-warning"><i class="fa fa-edit col-md-3 col-sm-3"></i> Edit Profile</a> -->
                                  <a href="/admin/interview" class="btn btn-outline-secondary"><< Kembali</a>
                                  <!-- <br /> -->

                                </div>
                                <div class="col-md-10 col-sm-10 ">

                                  <div class="profile_title">
                                    <div class="col-md-6">
                                      <h2>User Activity Report</h2>
                                    </div>
                                    <!-- <div class="col-md-6">
                                      <div id="reportrange" class="pull-right" style="margin-top: 5px; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #E6E9ED">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                                      </div>
                                    </div> -->
                                  </div>

                                  <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                      <li role="presentation" class="active"><a href="#tab_dataDiri" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Data Diri</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_dataKeluarga" role="tab" id="family-tab" data-toggle="tab" aria-expanded="false">Keluarga</a>
                                      </li>
                                      <!-- <li role="presentation" class="active"><a href="#tab_pendidikan" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Pendidikan</a>
                                      </li> -->
                                      <li role="presentation" class="active"><a href="#tab_riwayatKerja" role="tab" id="jobhistory-tab" data-toggle="tab" aria-expanded="false">Riwayat Kerja</a>
                                      </li>
                                      <!-- <li role="presentation" class="active"><a href="#tab_pelatihan" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Pelatihan</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_organisasi" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Organisasi</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_prestasi" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Prestasi</a>
                                      </li> -->
                                      <li role="presentation" class="active"><a href="#tab_bahasa" role="tab" id="bahasa-tab" data-toggle="tab" aria-expanded="false">Bahasa</a>
                                      </li>
                                      <!-- <li role="presentation" class="active"><a href="#tab_referensi" role="tab" id="referensi-tab" data-toggle="tab" aria-expanded="false">Referensi</a>
                                      </li> -->
                                      <li role="presentation" class="active"><a href="#tab_riwayat" role="tab" id="riwayat-tab" data-toggle="tab" aria-expanded="false">Riwayat</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_ec" role="tab" id="ec-tab" data-toggle="tab" aria-expanded="false">Kontak Darurat</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab_test" role="tab" id="test-tab" data-toggle="tab" aria-expanded="false">Test Kepribadian</a>
                                      </li>
                                    </ul>

                                    <div id="myTabContent" class="tab-content">

                                      <div role="tabpanel" class="tab-pane active " id="tab_dataDiri" aria-labelledby="home-tab">
                                        <!-- start recent activity -->
                                        <br>
                                        <ul class="messages">
                                          <div class=" item form-group">
                                            <label for="namaLengkap" class="col-form-label col-md-2 col-sm-2 label-align">Nama Lengkap</label>
                                                <input type="text" id="namaLengkap" name="namaLengkap" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> nama_lengkap}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="posisiDilamar" class="col-form-label col-md-2 col-sm-2 label-align">Posisi Dilamar</label>
                                                <input type="text" id="posisiDilamar" name="posisiDilamar" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> posisi_yang_dilamar}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="tempatLahir" class="col-form-label col-md-2 col-sm-2 label-align">Tempat Lahir</label>
                                                <input type="text" id="tempatLahir" name="tempatLahir" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> tempat_lahir}}">
                                            <label for="posisiDilamar" class="col-form-label col-md-3 label-align">Tanggal Lahir</label>
                                                <input type="text" id="posisiDilamar" name="posisiDilamar" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> tanggal_lahir}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="noKtp" class="col-form-label col-md-2 col-sm-2 label-align">No. KTP</label>
                                                <input type="text" id="noKtp" name="noKtp" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> no_ktp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="jenisKelamin" class="col-form-label col-md-2 col-sm-2 label-align">Jenis Kelamin</label>
                                                <input type="text" id="jenisKelamin" name="jenisKelamin" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> jenis_kelamin}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="anakke" class="col-form-label col-md-2 col-sm-2 label-align">Anak Ke</label>
                                                <input type="text" id="anakke" name="anakKe" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> anak_ke}}">
                                            <label for="jmlhs" class="col-form-label col-md-3 label-align">Jumlah Saudara</label>
                                                <input type="text" id="jmlhs" name="jmlhs" disabled class="form-control col-md-3 col-sm-3" value="{{$interview -> saudara}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="statusKawin" class="col-form-label col-md-2 col-sm-2 label-align">Status Perkawinan</label>
                                                <input type="text" id="statusKawin" name="statusKawin" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> status_perkawinan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="agama" class="col-form-label col-md-2 col-sm-2 label-align">Agama</label>
                                                <input type="text" id="agama" name="agama" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> agama}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="kebangsaan" class="col-form-label col-md-2 col-sm-2 label-align">Kebangsaan</label>
                                                <input type="text" id="kebangsaan" name="kebangsaan" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> kebangsaan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="npwp" class="col-form-label col-md-2 col-sm-2 label-align">NPWP</label>
                                                <input type="text" id="npwp" name="npwp" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> npwp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="alamat" class="col-form-label col-md-2 col-sm-2 label-align">Alamat Domisili</label>
                                                <input type="text" id="alamat" name="alamat" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> alamat_domisili}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Alamat KTP</label>
                                                <input type="text" id="alamatKtp" name="alalamatKtpamat" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> alamat_ktp}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="telepon" class="col-form-label col-md-2 col-sm-2 label-align">Telepon</label>
                                                <input type="text" id="telepon" name="telepon" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> telepon}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="email" class="col-form-label col-md-2 col-sm-2 label-align">Email Pribadi</label>
                                                <input type="text" id="email" name="email" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> email}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">Jenis Kendaraan</label>
                                                <input type="text" id="kendaraan" name="kendaraan" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> kendaraan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="laptop" class="col-form-label col-md-2 col-sm-2 label-align">Memiliki Laptop?</label>
                                                <input type="text" id="laptop" name="laptop" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> laptop}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="harapGaji" class="col-form-label col-md-2 col-sm-2 label-align">Gaji Yang Diharapkan</label>
                                                <input type="text" id="harapGaji" name="harapGaji" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> gaji_diharap}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="capaianSudah" class="col-form-label col-md-2 col-sm-2 label-align">Capaian Yang Sudah</label>
                                                <input type="text" id="capaianSudah" name="capaianSudah" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> pencapaian_sudah_dilakukan}}">
                                          </div>

                                          <div class=" item form-group">
                                            <label for="capaianBelum" class="col-form-label col-md-2 col-sm-2 label-align">Capaian Yang Belum</label>
                                                <input type="text" id="capaianBelum" name="capaianBelum" disabled class="form-control col-md-9 col-sm-9" value="{{$interview -> pencapaian_yang_diinginkan}}">
                                          </div>
                                        </ul>

                                        <div class="ln_solid"></div>
                                        <div class="item form-group">
                                          <div class="col-md-3 col-sm-3 offset-md-10 ">
                                          <a href="#" class="btn btn-custon-four btn-primary"
                                          data-myidpel="{{$interview -> id}}"
                                          data-mynamapel="{{$interview -> nama_lengkap}}"
                                          data-myposisipel="{{$interview -> posisi_yang_dilamar}}"
                                          data-mytempatlahirpel="{{$interview -> tempat_lahir}}"
                                          data-mytanggallahirpel="{{$interview -> tanggal_lahir}}"
                                          data-myktppel="{{$interview -> no_ktp}}"
                                          data-myjenkelpel="{{$interview -> jenis_kelamin}}"
                                          data-myanakke="{{$interview -> anak_ke}}"
                                          data-myjmlhs="{{$interview -> saudara}}"
                                          data-mystatpel="{{$interview -> status_perkawinan}}"
                                          data-myagmpel="{{$interview -> agama}}"
                                          data-mybangsapel="{{$interview -> kebangsaan}}"
                                          data-mynpwppel="{{$interview -> npwp}}"
                                          data-myaldopel="{{$interview -> alamat_domisili}}"
                                          data-myalktppel="{{$interview -> alamat_ktp}}"
                                          data-mytelpel="{{$interview -> telepon}}"
                                          data-myempripel="{{$interview -> email}}"
                                          data-mykendpel="{{$interview -> kendaraan}}"
                                          data-mylaptpel="{{$interview -> laptop}}"
                                          data-mygajihrppel="{{$interview -> gaji_diharap}}"
                                          data-mycapsdhpel="{{$interview -> pencapaian_sudah_dilakukan}}"
                                          data-mycapblmpel="{{$interview -> pencapaian_yang_diinginkan}}"
                                          data-toggle="modal" data-target="#modalEditDataDiri">Edit Data</a>
                                          </div>
                                        </div>
                                        <!-- end recent activity -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_dataKeluarga" aria-labelledby="family-tab">
                                        <!-- start user projects -->
                                        <br>
                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Data Keluarga</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Hubungan</th>
                                                      <th>Nama</th>
                                                      <th>Jenis Kelamin</th>
                                                      <th>Usia</th>
                                                      <th>Pendidikan</th>
                                                      <th>Pekerjaan</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>


                                                  <tbody>
                                                    @foreach ($family as $fm )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$fm -> hubungan}}</td>
                                                      <td>{{$fm -> nama}}</td>
                                                      <td>{{$fm -> jenis_kelamin}}</td>
                                                      <td>{{$fm -> usia}}</td>
                                                      <td>{{$fm -> pendidikan}}</td>
                                                      <td>{{$fm -> pekerjaan}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidfam="{{$fm -> id}}"
                                                        data-myhubfam="{{$fm -> hubungan}}"
                                                        data-mynamfam="{{$fm -> nama}}"
                                                        data-myjenkelfam="{{$fm -> jenis_kelamin}}"
                                                        data-myusifam="{{$fm -> usia}}"
                                                        data-mypenfam="{{$fm -> pendidikan}}"
                                                        data-mypekfam="{{$fm -> pekerjaan}}"
                                                        data-toggle="modal" data-target="#modalEditKeluarga">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$fm->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>
                                        <!-- end user projects -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_riwayatKerja" aria-labelledby="jobhistory-tab">
                                        <!-- start user projects -->
                                        <br>
                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Riwayat Bekerja</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Nama Perusahaan</th>
                                                      <th>Posisi</th>
                                                      <th>Tanggal Mulai</th>
                                                      <th>Tanggal Berhenti</th>
                                                      <th>Alasan Berhenti</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>


                                                  <tbody>
                                                    @foreach ($jobhistory as $jh )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$jh -> nama_perusahaan}}</td>
                                                      <td>{{$jh -> posisi}}</td>
                                                      <td>{{$jh -> tanggal_mulai}}</td>
                                                      <td>{{$jh -> tanggal_selesai}}</td>
                                                      <td>{{$jh -> alasan_berhenti}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidjh="{{$jh -> id}}"
                                                        data-mynamjh="{{$jh -> nama_perusahaan}}"
                                                        data-myposjh="{{$jh -> posisi}}"
                                                        data-mytglmjh="{{$jh -> tanggal_mulai}}"
                                                        data-mytglsjh="{{$jh -> tanggal_selesai}}"
                                                        data-myalsjh="{{$jh -> alasan_berhenti}}"
                                                        data-toggle="modal" data-target="#modalEditJh">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$jh->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>

                                        <br>
                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Referensi</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Perusahaan</th>
                                                      <th>Nama</th>
                                                      <th>Jabatan</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>


                                                  <tbody>
                                                    @foreach ($reference as $ref )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$ref -> perusahaan}}</td>
                                                      <td>{{$ref -> nama}}</td>
                                                      <td>{{$ref -> jabatan}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidref="{{$ref -> id}}"
                                                        data-myperus="{{$ref -> perusahaan}}"
                                                        data-mynama="{{$ref -> nama}}"
                                                        data-myjbtn="{{$ref -> jabatan}}"
                                                        data-toggle="modal" data-target="#modalEditRef">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$ref->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>
                                        <!-- end user projects -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_bahasa" aria-labelledby="bahasa-tab">
                                        <!-- start user projects -->
                                        <br>
                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Kemampuan Bahasa</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Bahasa</th>
                                                      <th>Berbicara</th>
                                                      <th>Tulis</th>
                                                      <th>Membaca</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>


                                                  <tbody>
                                                    @foreach ($language as $lang )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$lang -> language}}</td>
                                                      <td>{{$lang -> oral}}</td>
                                                      <td>{{$lang -> written}}</td>
                                                      <td>{{$lang -> reading}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidBhs="{{$lang -> id}}"
                                                        data-mybhsa="{{$lang -> language}}"
                                                        data-myoral="{{$lang -> oral}}"
                                                        data-mywrit="{{$lang -> written}}"
                                                        data-myread="{{$lang -> reading}}"
                                                        data-toggle="modal" data-target="#modalEditBhs">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$lang->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>
                                        <!-- end user projects -->
                                      </div>

                                      

                                      <div role="tabpanel" class="tab-pane fade" id="tab_riwayat" aria-labelledby="riwayat-tab">
                                        <!-- start user projects -->
                                        <br>
                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Riwayat Pendidikan</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Jenjang Pendidikan</th>
                                                      <th>Tempat Pendidikan</th>
                                                      <th>Jurusan</th>
                                                      <th>Kota</th>
                                                      <th>Tahun Mulai</th>
                                                      <th>Tahun Selesai</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>


                                                  <tbody>
                                                    @foreach ($education as $edu )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$edu -> jenis_pendidikan}}</td>
                                                      <td>{{$edu -> nama_sekolah}}</td>
                                                      <td>{{$edu -> jurusan}}</td>
                                                      <td>{{$edu -> kota}}</td>
                                                      <td>{{$edu -> tahun_mulai}}</td>
                                                      <td>{{$edu -> tahun_selesai}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidedu="{{$edu -> id}}"
                                                        data-myjenpen="{{$edu -> jenis_pendidikan}}"
                                                        data-mynamsek="{{$edu -> nama_sekolah}}"
                                                        data-myjrsn="{{$edu -> jurusan}}"
                                                        data-mykota="{{$edu -> kota}}"
                                                        data-mythnm="{{$edu -> tahun_mulai}}"
                                                        data-mythns="{{$edu -> tahun_selesai}}"
                                                        data-toggle="modal" data-target="#modalEditEdu">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$edu->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>

                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Riwayat Organisasi</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Nama Organisasi</th>
                                                      <th>Jabatan</th>
                                                      <th>Tahun Mulai</th>
                                                      <th>Tahun Selesai</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>

                                                  <tbody>
                                                    @foreach ($organization as $org )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$org -> nama_organisasi}}</td>
                                                      <td>{{$org -> jabatan}}</td>
                                                      <td>{{$org -> tahun_mulai}}</td>
                                                      <td>{{$org -> tahun_selesai}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidorg="{{$org -> id}}"
                                                        data-mynamor="{{$org -> nama_organisasi}}"
                                                        data-myjbtn="{{$org -> jabatan}}"
                                                        data-mythnm="{{$org -> tahun_mulai}}"
                                                        data-mythns="{{$org -> tahun_selesai}}"
                                                        data-toggle="modal" data-target="#modalEditOrg">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$org->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>

                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Riwayat Capaian Prestasi</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Nama Prestasi</th>
                                                      <th>Periode</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>

                                                  <tbody>
                                                    @foreach ($award as $aw )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$aw -> nama_prestasi}}</td>
                                                      <td>{{$aw -> periode}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidaw="{{$aw -> id}}"
                                                        data-mynampres="{{$aw -> nama_prestasi}}"
                                                        data-myperiod="{{$aw -> periode}}"
                                                        data-toggle="modal" data-target="#modalEditAw">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$aw->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>

                                        <label for="pendidikan" class="col-form-label col-md-3 col-sm-3"><h4>Riwayat Pelatihan</h4></label>

                                        <div class="x_content">
                                          <div class="table-responsive">
                                        <table class="table table-striped jambo_table bulk_action">
                                                  <thead>
                                                    <tr>
                                                      <th>No</th>
                                                      <th>Jenis Pelatihan</th>
                                                      <th>Nama Penyelenggara</th>
                                                      <th>Tempat</th>
                                                      <th>Tanggal Mulai</th>
                                                      <th>Tanggal Selesai</th>
                                                      <th>Aksi</th>
                                                    </tr>
                                                  </thead>

                                                  <tbody>
                                                    @foreach ($training as $trn )
                                                    <tr>
                                                      <td>{{$loop -> iteration}}</td>
                                                      <td>{{$trn -> jenis_pelatihan}}</td>
                                                      <td>{{$trn -> nama_penyelenggara}}</td>
                                                      <td>{{$trn -> tempat}}</td>
                                                      <td>{{$trn -> tanggal_mulai}}</td>
                                                      <td>{{$trn -> tanggal_selesai}}</td>
                                                      <td>
                                                        <a href="/admin/interview" class="btn btn-custon-four btn-warning"
                                                        data-myidtrn="{{$trn -> id}}"
                                                        data-myjenpel="{{$trn -> jenis_pelatihan}}"
                                                        data-mynampel="{{$trn -> nama_penyelenggara}}"
                                                        data-mytempel="{{$trn -> tempat}}"
                                                        data-mytglm="{{$trn -> tanggal_mulai}}"
                                                        data-mytgls="{{$trn -> tanggal_selesai}}"
                                                        data-toggle="modal" data-target="#modalEditTrn">Ubah</a>
                                                        <!-- <form action="/admin/interview/{{$trn->id}}" method="post" class="d-inline">
                                                        @method('DELETE')
                                                        @csrf
                                                        <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                                                        </form> -->
                                                      </td>
                                                    </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                          </div>
                                        </div>
                                        <!-- end user projects -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_ec" aria-labelledby="ec-tab">
                                        <!-- start user projects -->
                                          @foreach ($emercont as $ec )
                                          <br>
                                          <div class=" item form-group">
                                            <label for="namaEmercont" class="col-form-label col-md-1 col-sm-1 label-align">Nama</label>
                                                <input type="text" id="namaEmercont" name="namaEmercont" disabled class="form-control col-md-10 col-sm-10" value="{{$ec -> nama}}">
                                          </div>
                                          <div class=" item form-group">
                                            <label for="hubunganEmercont" class="col-form-label col-md-2 col-sm-2 label-align">Hubungan</label>
                                                <input type="text" id="hubunganEmercont" name="hubunganEmercont" disabled class="form-control col-md-9 col-sm-9" value="{{$ec -> hubungan}}">
                                          </div>
                                          <div class=" item form-group">
                                            <label for="alamatEmercont" class="col-form-label col-md-2 col-sm-2 label-align">Alamat</label>
                                                <input type="text" id="alamatEmercont" name="alamatEmercont" disabled class="form-control col-md-9 col-sm-9" value="{{$ec -> alamat}}">
                                          </div>
                                          <div class=" item form-group">
                                            <label for="telpEmercont" class="col-form-label col-md-2 col-sm-2 label-align">Telepon</label>
                                                <input type="text" id="telpEmercont" name="telpEmercont" disabled class="form-control col-md-9 col-sm-9" value="{{$ec -> telepon}}">
                                          </div>
                                          <div class="item form-group">
                                            <div class="col-md-3 col-sm-3 offset-md-10 ">
                                            <a href="#" class="btn btn-custon-four btn-primary"
                                            data-myidec="{{$ec -> id}}"
                                            data-mynama="{{$ec -> nama}}"
                                            data-myhbgn="{{$ec -> hubungan}}"
                                            data-myalmt="{{$ec -> alamat}}"
                                            data-mytlp="{{$ec -> telepon}}"
                                            data-toggle="modal" data-target="#modalEditEc">Edit Data</a>
                                            </div>
                                          </div>
                                          <div class="ln_solid"></div>
                                          @endforeach
                                        <!-- end user projects -->
                                      </div>

                                      <div role="tabpanel" class="tab-pane fade" id="tab_test" aria-labelledby="test-tab">
                                        <!-- start user projects -->
                                        <br>
                                        <div class="x_panel">
                                        <div class="x_title">
                                          <h2> <small> Hasil Test Kepribadian  </small></h2>
                                          <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                          </ul>
                                          <div class="clearfix"></div>
                                        </div>

                                        <div class="x_content">
                                        <div class="table-responsive">
                                          <table class="table jambo_table bulk_action">
                                            <thead>
                                              <tr class="headings">
                                                <th class="column-title">No </th>
                                                <th class="column-title">Tipe </th>
                                                <th class="column-title">Deskripsi </th>
                                                <th class="column-title">Code </th>
                                                <th class="column-title">Score </th>
                                                <th class="column-title">Analysis </th>
                                                </th>
                                              </tr>
                                            </thead>

                                            <tbody>
                                              <tr class="even pointer">
                                                  <tr>
                                                    <td rowspan="3">1</td>
                                                    <td rowspan="3">WORK DIRECTION</td>
                                                    <td>Menyelesaikan Tugas Dengan Segera</td>
                                                    <td>N</td>
                                                    <td>{{$N}}</td>
                                                    <td>
                                                      @if($N > '5' ) HIGH ANALISYS
                                                      @elseif($N > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Pekerja Keras</td>
                                                    <td>G</td>
                                                    <td>{{$G}}</td>
                                                    <td>
                                                      @if($G > '5' ) HIGH ANALISYS
                                                      @elseif($G > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Kebutuhan Berprestasi</td>
                                                    <td>A</td>
                                                    <td>{{$A}}</td>
                                                    <td>
                                                      @if($A > '5' ) HIGH ANALISYS
                                                      @elseif($A > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="odd pointer">
                                                  <tr>
                                                    <td rowspan="3">2</td>
                                                    <td rowspan="3">LEADERSHIP</td>
                                                    <td>Kepemimpinan</td>
                                                    <td>L</td>
                                                    <td>{{$L}}</td>
                                                    <td>
                                                      @if($L > '5' ) HIGH ANALISYS
                                                      @elseif($L > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Kebutuhan Mengendalikan Orang lain</td>
                                                    <td>P</td>
                                                    <td>{{$P}}</td>
                                                    <td>
                                                      @if($P > '5' ) HIGH ANALISYS
                                                      @elseif($P > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Cepat Mengambil Keputusan</td>
                                                    <td>I</td>
                                                    <td>{{$I}}</td>
                                                    <td>
                                                      @if($I > '5' ) HIGH ANALISYS
                                                      @elseif($I > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="even pointer">
                                                  <tr>
                                                    <td rowspan="2">3</td>
                                                    <td rowspan="2">ACTIVITY</td>
                                                    <td>Tipe Tergesa-gesa</td>
                                                    <td>T</td>
                                                    <td>{{$T}}</td>
                                                    <td>
                                                      @if($T > '5' ) HIGH ANALISYS
                                                      @elseif($T > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Rasa Ingin Tahu </td>
                                                    <td>V</td>
                                                    <td>{{$V}}</td>
                                                    <td>
                                                      @if($V > '5' ) HIGH ANALISYS
                                                      @elseif($V > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="odd pointer">
                                                  <tr>
                                                    <td rowspan="4">4</td>
                                                    <td rowspan="4">SOCIAL NATURE</td>
                                                    <td>Butuh Perhatian</td>
                                                    <td>X</td>
                                                    <td>{{$X}}</td>
                                                    <td>
                                                      @if($X > '5' ) HIGH ANALISYS
                                                      @elseif($X > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Pergaulan luas </td>
                                                    <td>S</td>
                                                    <td>{{$S}}</td>
                                                    <td>
                                                      @if($S > '5' ) HIGH ANALISYS
                                                      @elseif($S > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Kebutuhan untuk berkelompok </td>
                                                    <td>B</td>
                                                    <td>{{$B}}</td>
                                                    <td>
                                                      @if($B > '5' ) HIGH ANALISYS
                                                      @elseif($B > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Butuh untuk dekat dan menyayangi </td>
                                                    <td>O</td>
                                                    <td>{{$O}}</td>
                                                    <td>
                                                      @if($O > '5' ) HIGH ANALISYS
                                                      @elseif($O > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="odd pointer">
                                                  <tr>
                                                    <td rowspan="3">5</td>
                                                    <td rowspan="3">WORK STYLE</td>
                                                    <td>Tipe teoretikal</td>
                                                    <td>R</td>
                                                    <td>{{$R}}</td>
                                                    <td>
                                                      @if($R > '5' ) HIGH ANALISYS
                                                      @elseif($R > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Suka pekerjaan yang detail </td>
                                                    <td>D</td>
                                                    <td>{{$D}}</td>
                                                    <td>
                                                      @if($D > '5' ) HIGH ANALISYS
                                                      @elseif($D > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Tipe teratur</td>
                                                    <td>C</td>
                                                    <td>{{$C}}</td>
                                                    <td>
                                                      @if($C > '5' ) HIGH ANALISYS
                                                      @elseif($C > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="odd pointer">
                                                  <tr>
                                                    <td rowspan="3">6</td>
                                                    <td rowspan="3">TEMPERAMENT</td>
                                                    <td>Hasrat untuk berubah</td>
                                                    <td>Z</td>
                                                    <td>{{$Z}}</td>
                                                    <td>
                                                      @if($Z > '5' ) HIGH ANALISYS
                                                      @elseif($Z > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Pengendalian emosi </td>
                                                    <td>E</td>
                                                    <td>{{$E}}</td>
                                                    <td>
                                                      @if($E > '5' ) HIGH ANALISYS
                                                      @elseif($E > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Agresifitas</td>
                                                    <td>K</td>
                                                    <td>{{$K}}</td>
                                                    <td>
                                                      @if($K > '5' ) HIGH ANALISYS
                                                      @elseif($K > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                              <tr class="even pointer">
                                                  <tr>
                                                    <td rowspan="2">7</td>
                                                    <td rowspan="2">FOLLOWERSHIP</td>
                                                    <td>Hasrat mendukung atasan</td>
                                                    <td>F</td>
                                                    <td>{{$F}}</td>
                                                    <td>
                                                      @if($F > '5' ) HIGH ANALISYS
                                                      @elseif($F > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td>Taat pada aturan dan supervisi </td>
                                                    <td>W</td>
                                                    <td>{{$W}}</td>
                                                    <td>
                                                      @if($W > '5' ) HIGH ANALISYS
                                                      @elseif($W > '3') MIDDLE RANGE
                                                      @else LOW ANALISYS
                                                      @endif
                                                    </td>
                                                  </tr>
                                              </tr>

                                            </tbody>
                                          </table>
                                        <div class="ln_solid"></div>
                                        <div class="item form-group">
                                          <div class="col-md-3 col-sm-3 offset-md-10 ">
                                          <a href="#" class="btn btn-custon-four btn-primary"
                                          data-myidtest="{{$id}}"
                                          data-mychn="{{$N}}"
                                          data-mychg="{{$G}}"
                                          data-mycha="{{$A}}"
                                          data-mychl="{{$L}}"
                                          data-mychp="{{$P}}"
                                          data-mychi="{{$I}}"
                                          data-mycht="{{$T}}"
                                          data-mychv="{{$V}}"
                                          data-mychx="{{$X}}"
                                          data-mychs="{{$S}}"
                                          data-mychb="{{$B}}"
                                          data-mycho="{{$O}}"
                                          data-mychr="{{$R}}"
                                          data-mychd="{{$D}}"
                                          data-mychc="{{$C}}"
                                          data-mychz="{{$Z}}"
                                          data-myche="{{$E}}"
                                          data-mychk="{{$K}}"
                                          data-mychf="{{$F}}"
                                          data-mychw="{{$W}}"
                                          data-toggle="modal" data-target="#modalEditTest">Edit Data</a>
                                          </div>
                                        </div>
                                        </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-md-12 col-sm-12  ">
                                          <div class="x_panel">
                                            <div class="x_title">
                                              <h2>PERSONALITY SCORE</h2>
                                              <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                              <div class="panel">
                                                <div id="container"></div>
                                              </div>
                                            </div>
                                            </div>
                                          </div>
                                        </div>

                                            </div>
                                          </div>

                                      </div>
                                        <!-- end user projects -->
                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
<!-- --------- MODAL --------- -->

    <!-- --------- MODAL EDIT DATA DIRI --------- -->
    <div class="modal fade" id="modalEditDataDiri" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Diri Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataDiri">
                              <input type="hidden" id="token" name="token" value="qazmkodd">
                              <input type="hidden" id="idpel" name="idPel" value="">
                              <div class="form-group">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control @error('namaPel') is-invalid @enderror" id="namapel" name="namaPel" value ="{{ old('namaPel')}}">
                                  @error('namaPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="posisi">Posisi Dilamar</label>
                                <input type="text" class="form-control @error('posisiPel') is-invalid @enderror" id="posisipel" name="posisiPel" value ="{{ old('posisiPel')}}">
                                  @error('posisiPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="tempatLahir">Tempat Lahir</label>
                                <input type="text" class="form-control @error('tempatLahirPel') is-invalid @enderror" id="tempatLahirpel" name="tempatLahirPel" value ="{{ old('tempatLahirPel')}}">
                                  @error('tempatLahirPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="tanggalLahir">Tanggal Lahir</label>
                                <input type="text" class="form-control @error('tanggalLahirPel') is-invalid @enderror" id="tanggalLahirpel" name="tanggalLahirPel" value ="{{ old('tanggalLahirPel')}}">
                                  @error('tanggalLahirPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <!-- <input type="hidden" id="catatanpel" name="catatanPel" value=""> -->
                              <div class="form-group">
                                <label for="ktp">No. KTP</label>
                                <input type="text" class="form-control @error('ktpPel') is-invalid @enderror" id="ktppel" name="ktpPel" value ="{{ old('ktpPel')}}">
                                  @error('ktpPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jenKel">Jenis Kelamin</label>
                                <input type="text" class="form-control @error('jenKelPel') is-invalid @enderror" id="jenKelpel" name="jenKelPel" value ="{{ old('jenKelPel')}}" placeholder="L / P">
                                  @error('jenKelPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="anakke">Anak Ke</label>
                                <input type="text" class="form-control @error('anakkePel') is-invalid @enderror" id="anakkepel" name="anakkePel" value ="{{ old('anakkePel')}}">
                                  @error('anakkePel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jmlhs">Jumlah Saudara</label>
                                <input type="text" class="form-control @error('jmlhsPel') is-invalid @enderror" id="jmlhspel" name="jmlhsPel" value ="{{ old('jmlhsPel')}}">
                                  @error('jmlhsPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <!-- <input type="hidden" id="statuspel" name="statusPel" value=""> -->
                              <div class="form-group">
                                <label for="stat">Status Perkawinan</label>
                                <input type="text" class="form-control @error('statPel') is-invalid @enderror" id="statpel" name="statPel" value ="{{ old('statPel')}}">
                                  @error('statPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="agm">Agama</label>
                                <input type="text" class="form-control @error('agmPel') is-invalid @enderror" id="agmpel" name="agmPel" value ="{{ old('agmPel')}}">
                                  @error('agmPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="bangsa">Kebangsaan</label>
                                <input type="text" class="form-control @error('bangsaPel') is-invalid @enderror" id="bangsapel" name="bangsaPel" value ="{{ old('bangsaPel')}}">
                                  @error('bangsaPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <!-- <input type="hidden" id="alasanpel" name="alasanPel" value=""> -->
                              <div class="form-group">
                                <label for="npwp">NPWP</label>
                                <input type="text" class="form-control @error('npwpPel') is-invalid @enderror" id="npwppel" name="npwpPel" value ="{{ old('npwpPel')}}">
                                  @error('npwpPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="alDo">Alamat Domisili</label>
                                <input type="text" class="form-control @error('alDoPel') is-invalid @enderror" id="alDopel" name="alDoPel" value ="{{ old('alDoPel')}}">
                                  @error('alDoPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="alKtp">Alamat KTP</label>
                                <input type="text" class="form-control @error('alKtpPel') is-invalid @enderror" id="alKtppel" name="alKtpPel" value ="{{ old('alKtpPel')}}">
                                  @error('alKtpPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="tel">Telepon</label>
                                <input type="text" class="form-control @error('telPel') is-invalid @enderror" id="telpel" name="telPel" value ="{{ old('telPel')}}">
                                  @error('telPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="emPri">Email Pribadi</label>
                                <input type="text" class="form-control @error('emPriPel') is-invalid @enderror" id="emPripel" name="emPriPel" value ="{{ old('emPriPel')}}">
                                  @error('emPriPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="kend">Jenis Kendaraan</label>
                                <input type="text" class="form-control @error('kendPel') is-invalid @enderror" id="kendpel" name="kendPel" value ="{{ old('kendPel')}}">
                                  @error('kendPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="lapt">Memiliki Laptop?</label>
                                <input type="text" class="form-control @error('laptPel') is-invalid @enderror" id="laptpel" name="laptPel" value ="{{ old('laptPel')}}">
                                  @error('laptPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="gajiHrp">Gaji Yang Diharapkan</label>
                                <input type="text" class="form-control @error('gajiHrpPel') is-invalid @enderror" id="gajiHrppel" name="gajiHrpPel" value ="{{ old('gajiHrpPel')}}">
                                  @error('gajiHrpPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="capSdh">Capaian Yang Sudah</label>
                                <input type="text" class="form-control @error('capSdhPel') is-invalid @enderror" id="capSdhpel" name="capSdhPel" value ="{{ old('capSdhPel')}}">
                                  @error('capSdhPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="capBlm">Capaian Yang Belum</label>
                                <input type="text" class="form-control @error('capBlmPel') is-invalid @enderror" id="capBlmpel" name="capBlmPel" value ="{{ old('capBlmPel')}}">
                                  @error('capBlmPel')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

    <!-- --------- MODAL EDIT DATA KELUARGA --------- -->
    <div class="modal fade" id="modalEditKeluarga" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Keluarga Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataKeluarga">
                              <input type="hidden" id="token" name="token" value="qzwxeck">
                              <input type="hidden" id="idfam" name="idFam" value="">
                              <div class="form-group">
                                <label for="hubungan">Hubungan</label>
                                <input type="text" class="form-control @error('hubFam') is-invalid @enderror" id="hubfam" name="hubFam" value ="{{ old('hubFam')}}">
                                  @error('hubFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" class="form-control @error('namFam') is-invalid @enderror" id="namfam" name="namFam" value ="{{ old('namFam')}}">
                                  @error('namFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jenkel">Jenis Kelamin</label>
                                <input type="text" class="form-control @error('jenkelFam') is-invalid @enderror" id="jenkelfam" name="jenkelFam" value ="{{ old('jenkelFam')}}">
                                  @error('jenkelFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="usia">Usia</label>
                                <input type="text" class="form-control @error('usiFam') is-invalid @enderror" id="usifam" name="usiFam" value ="{{ old('usiFam')}}">
                                  @error('usiFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="pendidikan">Pendidikan</label>
                                <input type="text" class="form-control @error('penFam') is-invalid @enderror" id="penfam" name="penFam" value ="{{ old('penFam')}}">
                                  @error('penFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="pekerjaan">Pekerjaan</label>
                                <input type="text" class="form-control @error('pekFam') is-invalid @enderror" id="pekfam" name="pekFam" value ="{{ old('pekFam')}}">
                                  @error('pekFam')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

    <!-- --------- MODAL EDIT DATA JOB HISTORY --------- -->
    <div class="modal fade" id="modalEditJh" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Riwayat Pekerjaan Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataJh">
                              <input type="hidden" id="token" name="token" value="jklasdjh">
                              <input type="hidden" id="idjh" name="idJh" value="">
                              <div class="form-group">
                                <label for="namaPerusahaan">Nama Perusahaan</label>
                                <input type="text" class="form-control @error('namJh') is-invalid @enderror" id="namjh" name="namJh" value ="{{ old('namJh')}}">
                                  @error('namJh')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="posisi">Posisi</label>
                                <input type="text" class="form-control @error('posJh') is-invalid @enderror" id="posjh" name="posJh" value ="{{ old('posJh')}}">
                                  @error('posJh')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="tglm">Tanggal Mulai</label>
                                <input type="text" class="form-control @error('tglmJh') is-invalid @enderror" id="tglmjh" name="tglmJh" value ="{{ old('tglmJh')}}">
                                  @error('tglmJh')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="tgls">Tanggal Selesai</label>
                                <input type="text" class="form-control @error('tglsJh') is-invalid @enderror" id="tglsjh" name="tglsJh" value ="{{ old('tglsJh')}}">
                                  @error('tglsJh')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="alasan">Alasan Berhenti</label>
                                <input type="text" class="form-control @error('alsJh') is-invalid @enderror" id="alsjh" name="alsJh" value ="{{ old('alsJh')}}">
                                  @error('alsJh')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

    <!-- --------- MODAL EDIT DATA BAHASA --------- -->
    <div class="modal fade" id="modalEditBhs" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Kemampuan Bahasa Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataBhs">
                              <input type="hidden" id="token" name="token" value="oipxzcbhs">
                              <input type="hidden" id="idbhs" name="idBhs" value="">
                              <div class="form-group">
                                <label for="bahasa">Bahasa</label>
                                <input type="text" class="form-control @error('bhsa') is-invalid @enderror" id="bhsa" name="bhsa" value ="{{ old('bhsa')}}">
                                  @error('bhsa')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="oral">Oral</label>
                                <input type="text" class="form-control @error('oralBhs') is-invalid @enderror" id="oralbhs" name="oralBhs" value ="{{ old('oralBhs')}}">
                                  @error('oralBhs')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="written">Written</label>
                                <input type="text" class="form-control @error('writBhs') is-invalid @enderror" id="writbhs" name="writBhs" value ="{{ old('writBhs')}}">
                                  @error('writBhs')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="read">Reading</label>
                                <input type="text" class="form-control @error('readBhs') is-invalid @enderror" id="readbhs" name="readBhs" value ="{{ old('readBhs')}}">
                                  @error('readBhs')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

    <!-- --------- MODAL EDIT DATA REFERENCE --------- -->
    <div class="modal fade" id="modalEditRef" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Referensi Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataRef">
                              <input type="hidden" id="token" name="token" value="hgfvbnref">
                              <input type="hidden" id="idref" name="idRef" value="">
                              <div class="form-group">
                                <label for="perus">Perusahaan</label>
                                <input type="text" class="form-control @error('perusRef') is-invalid @enderror" id="perusref" name="perusRef" value ="{{ old('perusRef')}}">
                                  @error('perusRef')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="nama">Nama</label>
                                <input type="text" class="form-control @error('namaRef') is-invalid @enderror" id="namaref" name="namaRef" value ="{{ old('namaRef')}}">
                                  @error('namaRef')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jbtn">Jabatan</label>
                                <input type="text" class="form-control @error('jbtnRef') is-invalid @enderror" id="jbtnref" name="jbtnRef" value ="{{ old('jbtnRef')}}">
                                  @error('jbtnRef')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

    <!-- --------- MODAL EDIT DATA EDUCATION --------- -->
    <div class="modal fade" id="modalEditEdu" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Riwayat Pendidikan Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataEdu">
                              <input type="hidden" id="token" name="token" value="mnbpoiedu">
                              <input type="hidden" id="idedu" name="idEdu" value="">
                              <div class="form-group">
                                <label for="jenis">Jenis Pendidikan</label>
                                <input type="text" class="form-control @error('jenpenEdu') is-invalid @enderror" id="jenpenedu" name="jenpenEdu" value ="{{ old('jenpenEdu')}}">
                                  @error('jenpenEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="nama">Nama Sekolah</label>
                                <input type="text" class="form-control @error('namsekEdu') is-invalid @enderror" id="namsekedu" name="namsekEdu" value ="{{ old('namsekEdu')}}">
                                  @error('namsekEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jurusan">Jurusan</label>
                                <input type="text" class="form-control @error('jrsnEdu') is-invalid @enderror" id="jrsnedu" name="jrsnEdu" value ="{{ old('jrsnEdu')}}">
                                  @error('jrsnEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="kota">Kota</label>
                                <input type="text" class="form-control @error('kotaEdu') is-invalid @enderror" id="kotaedu" name="kotaEdu" value ="{{ old('kotaEdu')}}">
                                  @error('kotaEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="thnm">Tahun Mulai</label>
                                <input type="text" class="form-control @error('thnmEdu') is-invalid @enderror" id="thnmedu" name="thnmEdu" value ="{{ old('thnmEdu')}}">
                                  @error('thnmEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="kota">Tahun Selesai</label>
                                <input type="thns" class="form-control @error('thnsEdu') is-invalid @enderror" id="thnsedu" name="thnsEdu" value ="{{ old('thnsEdu')}}">
                                  @error('thnsEdu')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT ----------->

    <!-- --------- END MODAL EDIT Organisasi----------->
    <div class="modal fade" id="modalEditOrg" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Riwayat Organsisasi Pelamar</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>

                          <div class="modal-body" id="editDataOrg">
                              <input type="hidden" id="token" name="token" value="zwxecrorg">
                              <input type="hidden" id="idorg" name="idOrg" value="">
                              <div class="form-group">
                                <label for="namor">Nama Organisasi</label>
                                <input type="text" class="form-control @error('namorOrg') is-invalid @enderror" id="namororg" name="namorOrg" value ="{{ old('namorOrg')}}">
                                  @error('namorOrg')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="jbtn">Jabatan</label>
                                <input type="text" class="form-control @error('jbtnOrg') is-invalid @enderror" id="jbtnorg" name="jbtnOrg" value ="{{ old('jbtnOrg')}}">
                                  @error('jbtnOrg')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="thnm">Tahun Mulai</label>
                                <input type="text" class="form-control @error('thnmOrg') is-invalid @enderror" id="thnmorg" name="thnmOrg" value ="{{ old('thnmOrg')}}">
                                  @error('thnmOrg')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                              <div class="form-group">
                                <label for="kota">Tahun Selesai</label>
                                <input type="thns" class="form-control @error('thnsOrg') is-invalid @enderror" id="thnsorg" name="thnsOrg" value ="{{ old('thnsOrg')}}">
                                  @error('thnsOrg')
                                    <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT ----------->

    <!-- --------- MODAL EDIT DATA AWARD --------- -->
    <div class="modal fade" id="modalEditAw" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                  <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                  @method('patch')
                  @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalEditTitle">Edit Data Diri Pelamar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                      <div class="modal-body" id="editDataAw">
                          <input type="hidden" id="token" name="token" value="adgjlxaw">
                          <input type="hidden" id="idaw" name="idAw" value="">
                          <div class="form-group">
                            <label for="nampres">Nama Prestasi</label>
                            <input type="text" class="form-control @error('nampresAw') is-invalid @enderror" id="nampresaw" name="nampresAw" value ="{{ old('nampresAw')}}">
                              @error('nampresAw')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="period">Periode</label>
                            <input type="text" class="form-control @error('periodAw') is-invalid @enderror" id="periodaw" name="periodAw" value ="{{ old('periodAw')}}">
                              @error('periodAw')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
    <!-- --------- END MODAL EDIT ----------->

    <!-- --------- MODAL EDIT DATA TRAINING --------- -->
    <div class="modal fade" id="modalEditTrn" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                  <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                  @method('patch')
                  @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalEditTitle">Edit Data Diri Pelamar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                      <div class="modal-body" id="editDataTrn">
                          <input type="hidden" id="token" name="token" value="urhfbvtrn">
                          <input type="hidden" id="idtrn" name="idTrn" value="">
                          <div class="form-group">
                            <label for="jenpel">Jenis Pelatihan</label>
                            <input type="text" class="form-control @error('jenpelTrn') is-invalid @enderror" id="jenpeltrn" name="jenpelTrn" value ="{{ old('jenpelTrn')}}">
                              @error('jenpelTrn')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="nampel">Nama Penyelenggara</label>
                            <input type="text" class="form-control @error('nampelTrn') is-invalid @enderror" id="nampeltrn" name="nampelTrn" value ="{{ old('nampelTrn')}}">
                              @error('nampelTrn')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="tempel">Tempat</label>
                            <input type="text" class="form-control @error('tempelTrn') is-invalid @enderror" id="tempeltrn" name="tempelTrn" value ="{{ old('tempelTrn')}}">
                              @error('tempelTrn')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="tglm">Tanggal Mulai</label>
                            <input type="text" class="form-control @error('tglmTrn') is-invalid @enderror" id="tglmtrn" name="tglmTrn" value ="{{ old('tglmTrn')}}">
                              @error('tglmTrn')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="tgls">Tanggal Selesai</label>
                            <input type="text" class="form-control @error('tglsTrn') is-invalid @enderror" id="tglstrn" name="tglsTrn" value ="{{ old('tglsTrn')}}">
                              @error('tglsTrn')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
    <!-- --------- END MODAL EDIT ----------->

    <!-- --------- MODAL EDIT EMERGENCY CONTACT --------- -->
    <div class="modal fade" id="modalEditEc" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                  <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                  @method('patch')
                  @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalEditTitle">Edit Data Diri Pelamar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                      <div class="modal-body" id="editDataEc">
                          <input type="hidden" id="token" name="token" value="qrupdhec">
                          <input type="hidden" id="idec" name="idEc" value="">
                          <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control @error('namaEc') is-invalid @enderror" id="namaec" name="namaEc" value ="{{ old('namaEc')}}">
                              @error('namaEc')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="hub">Hubungan</label>
                            <input type="text" class="form-control @error('hubunganEc') is-invalid @enderror" id="hubunganec" name="hubunganEc" value ="{{ old('hubunganEc')}}">
                              @error('hubunganEc')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="almt">Alamat</label>
                            <input type="text" class="form-control @error('almtEc') is-invalid @enderror" id="almtec" name="almtEc" value ="{{ old('almtEc')}}">
                              @error('almtEc')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                          <div class="form-group">
                            <label for="tlp">Telepon</label>
                            <input type="text" class="form-control @error('tlpEc') is-invalid @enderror" id="tlpec" name="tlpEc" value ="{{ old('tlpEc')}}">
                              @error('tlpEc')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                          </div>
                      </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
    <!-- --------- END MODAL EDIT ----------->

    <!-- --------- MODAL EDIT TEST --------- -->
    <div class="modal fade" id="modalEditTest" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                  <form action="{{route('interview.update', 'test')}}" id="editForm" method="post">
                  @method('patch')
                  @csrf
                    <div class="modal-header">
                      <h5 class="modal-title" id="modalEditTitle">Edit Data Diri Pelamar</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>

                      <div class="modal-body" id="editDataTest">
                        <input type="hidden" id="token" name="token" value="awsxdrtest">
                        <input type="hidden" id="idtest" name="idTest" value="">
                        <div class="form-group">
                            <label for="jchN">Karakter N</label>
                            <input type="number" class="form-control @error('jchN') is-invalid @enderror" id="jchn" name="jchN" value ="{{ old('jchN')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchN')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchG">Karakter G</label>
                            <input type="number" class="form-control @error('jchG') is-invalid @enderror" id="jchg" name="jchG" value ="{{ old('jchG')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchG')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchA">Karakter A</label>
                            <input type="number" class="form-control @error('jchA') is-invalid @enderror" id="jcha" name="jchA" value ="{{ old('jchA')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchA')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchL">Karakter L</label>
                            <input type="number" class="form-control @error('jchL') is-invalid @enderror" id="jchl" name="jchL" value ="{{ old('jchL')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchL')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchP">Karakter P</label>
                            <input type="number" class="form-control @error('jchP') is-invalid @enderror" id="jchp" name="jchP" value ="{{ old('jchP')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchP')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchI">Karakter I</label>
                            <input type="number" class="form-control @error('jchI') is-invalid @enderror" id="jchi" name="jchI" value ="{{ old('jchI')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchI')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchT">Karakter T</label>
                            <input type="number" class="form-control @error('jchT') is-invalid @enderror" id="jcht" name="jchT" value ="{{ old('jchT')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchT')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchV">Karakter V</label>
                            <input type="number" class="form-control @error('jchV') is-invalid @enderror" id="jchv" name="jchV" value ="{{ old('jchV')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchV')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchX">Karakter X</label>
                            <input type="number" class="form-control @error('jchX') is-invalid @enderror" id="jchx" name="jchX" value ="{{ old('jchX')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchX')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchS">Karakter S</label>
                            <input type="number" class="form-control @error('jchS') is-invalid @enderror" id="jchs" name="jchS" value ="{{ old('jchS')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchS')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchB">Karakter B</label>
                            <input type="number" class="form-control @error('jchB') is-invalid @enderror" id="jchb" name="jchB" value ="{{ old('jchB')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchB')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchO">Karakter O</label>
                            <input type="number" class="form-control @error('jchO') is-invalid @enderror" id="jcho" name="jchO" value ="{{ old('jchO')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchO')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchR">Karakter R</label>
                            <input type="number" class="form-control @error('jchR') is-invalid @enderror" id="jchr" name="jchR" value ="{{ old('jchR')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchR')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchD">Karakter D</label>
                            <input type="number" class="form-control @error('jchD') is-invalid @enderror" id="jchd" name="jchD" value ="{{ old('jchD')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchD')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchC">Karakter C</label>
                            <input type="number" class="form-control @error('jchC') is-invalid @enderror" id="jchc" name="jchC" value ="{{ old('jchC')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchC')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchZ">Karakter Z</label>
                            <input type="number" class="form-control @error('jchZ') is-invalid @enderror" id="jchz" name="jchZ" value ="{{ old('jchZ')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchZ')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchE">Karakter E</label>
                            <input type="number" class="form-control @error('jchE') is-invalid @enderror" id="jche" name="jchE" value ="{{ old('jchE')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchE')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchK">Karakter K</label>
                            <input type="number" class="form-control @error('jchK') is-invalid @enderror" id="jchk" name="jchK" value ="{{ old('jchK')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchK')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchF">Karakter F</label>
                            <input type="number" class="form-control @error('jchF') is-invalid @enderror" id="jchf" name="jchF" value ="{{ old('jchF')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchF')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                        <div class="form-group">
                            <label for="jchW">Karakter W</label>
                            <input type="number" class="form-control @error('jchW') is-invalid @enderror" id="jchw" name="jchW" value ="{{ old('jchW')}}" placeholder="Isi Dengan Angka 0-10">
                              @error('jchW')
                                <div class="invalid-feedback">Bagian Ini Tidak Boleh Kosong</div>
                              @enderror
                        </div>
                      <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
    <!-- --------- END MODAL EDIT ----------->
@endsection

@section('chart')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script type="text/javascript">
        Highcharts.chart('container', {

          chart: {
              polar: true,
              type: 'line'
          },

          title: {
              text: 'GRAPH SCORE',
              x: -80
          },

          pane: {
              size: '90%'
          },

          xAxis: {
              categories: ['N', 'G', 'A', 'L','P',
                  'I', 'T', 'V', 'X','S',
                'B', 'O', 'R', 'D','C',
              'Z', 'E', 'K', 'F','W'],
              tickmarkPlacement: 'on',
              lineWidth: 0
          },

          yAxis: {
              gridLineInterpolation: 'polygon',
              lineWidth: 0,
              min: 0
          },

          tooltip: {
              shared: true,
              pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
          },

          legend: {
              align: 'right',
              verticalAlign: 'middle'
          },

          series: [{
              name: 'Actual Spending',
              data: {!!json_encode($dataset)!!},
              pointPlacement: 'on'
          }],

          responsive: {
              rules: [{
                  condition: {
                      maxWidth: 500
                  },
                  chartOptions: {
                      legend: {
                          align: 'center',
                          verticalAlign: 'bottom'
                      },
                      pane: {
                          size: '90%'
                      }
                  }
              }]
          }

        });
</script>
@endsection
