@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Edit Karyawan <small>By : {{auth()->user()->level}} ({{auth()->user()->nama}})</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Form Edit Karyawan  <small>PT Davinti Indonesia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Settings 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form action="/admin/employees2/{{$employee->id}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
          @method('patch')
          @csrf
         
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Kode Karyawan</label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" readonly name="kode" value="{{$employee->kode_karyawan}}" />
                      </div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Nama Karyawan</label>
                      <div class="col-md-8 col-sm-8">
                        <input class="form-control" readonly name="nama" value="{{$employee->nama}}" />
                      </div>
                    </div>
                   
                    
                        <div class="field item form-group">
                    <label class="col-form-label col-md-2 col-sm-2  label-align">Level</label>
                    <div class="col-md-3 col-sm-3">
                              <select name="level"  class="form-control ">
                              <option value="{{$employee->role}}"  selected="true">{{$employee->role}}</option>
                               <option >Admin</option>
                               <option >Pegawai</option>
                               <option >Atasan</option>
                               <option >Direktur</option>
                              </select >
                           </div>
                           <label class="col-form-label col-md-2 col-sm-2 label-align">Jabatan</label>
                    <div class="col-md-3 col-sm-3">
                              <select name="jabatan"  class="form-control ">
                               <option value="{{$employee->id_jabatan}}"  selected="true">{{$employee->position->nama_jabatan}}</option>
                                @foreach ($position as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_jabatan}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                       
                        <div class="field item form-group">
                    <label class="col-form-label col-md-2 col-sm-2  label-align">Devisi</label>
                    <div class="col-md-3 col-sm-3">
                    <select name="devisi"  class="form-control ">
                               <option value="{{$employee->id_devisi}}"  >{{$employee->division->nama_devisi}}</option>
                                @foreach ($division as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_devisi}}</option>
                                @endforeach
                              </select >
                           </div>
                           <label class="col-form-label col-md-2 col-sm-2 label-align">Grade</label>
                    <div class="col-md-3 col-sm-3">
                    <select name="grade"  class="form-control ">
                               <option value="{{$employee->id_grade}}" >{{$employee->grade->nama_grade}}</option>
                                @foreach ($grade as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_grade}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        
                        <div class="field item form-group">
                    <label class="col-form-label col-md-2 col-sm-2  label-align">Corporate Grup</label>
                    <div class="col-md-3 col-sm-3">
                    <select name="corporate"  class="form-control ">
                               <option value="{{$employee->id_corporate}}"  >{{$employee->corporate->nama_corporate_group}}</option>
                                @foreach ($corporate as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_corporate_group}}</option>
                                @endforeach
                              </select >
                           </div>
                           <label class="col-form-label col-md-2 col-sm-2 label-align">Status</label>
                    <div class="col-md-3 col-sm-3">
                    <select name="status"  class="form-control ">
                              <option value="{{$employee->status}}"  >{{$employee->status}}</option>
                               <option >Kontrak</option>
                               <option >Tetap</option>
                               <option >Resign</option>
                              </select >
                           </div>
                        </div>
                        
                        <div class="field item form-group">
                    <label class="col-form-label col-md-2 col-sm-2  label-align">Nama Atasan</label>
                    <div class="col-md-8 col-sm-8">
                              <select name="atasan"  class="form-control">
                               <option value="{{$employee->nama_atasan}}" >{{$employee->nama_atasan}}</option>
                                @foreach ($employees as $k => $v)
                               <option value="{{$v->nama}}">{{$v->nama}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                       
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Gaji Pokok</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->gaji_pokok}}"type="tel" class='tel' name="gaji" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Jatah Cuti</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->jatah_cuti}}" type="number" name="jatah_cuti" ></div>
                    </div>

                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Tunjangan Harian</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee->tunjangan_harian}}" class='tel' name="tunjangan_harian" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Tunjangan Transport</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel" value="{{$employee->tunjangan_transport}}" class='tel' name="tunjangan_transport" ></div>
                    </div> 

                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">BPJS Kesehatan</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->bpjs_kes}}"type="tel" class='tel' name="kesehatan" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">BPJS Ketenagakerjaan</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee->bpjs_ket}}"class='tel' name="ketenagakerjaan" ></div>
                    </div> 
                      <div class="field item form-group">
                      <label for="agama" class="col-form-label col-md-2 col-sm-2 label-align">Email Corporate</label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" value="{{$employee->email_corporate}}" class='date' type="text" name="email_corporate" ></div>
                      </div>
                      <div class="field item form-group">
                      <label for="kebangsaan" class="col-form-label col-md-2 col-sm-2 label-align">Nama Akun Bank</label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" value="{{$employee->bank_account}}" class='date' type="text" name="bank_acoount" ></div>
                     </div>
                      <div class="field item form-group">
                      <label for="npwp" class="col-form-label col-md-2 col-sm-2 label-align">Nomer Rekening </label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" value="{{$employee->rekening_number}}" class='date' type="text" name="join_date" ></div>
                      </div>
                      <div class="field item form-group">
                      <label for="alamat" class="col-form-label col-md-2 col-sm-2 label-align">Nama Akun</label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" value="{{$employee->account_name}}" class='date' type="text" name="join_date" ></div>
                     </div>
                     <div class="field item form-group">
                     <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Posisi Yang Dilamar</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee-> postulant->posisi_yang_dilamar}}"type="tel" class='tel' name="posisi_yang_dilamar" ></div>
                        <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">No Ktp</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee-> postulant->no_ktp}}"class='tel' name="no_ktp" ></div>
                        </div>       
                        <div class="field item form-group">
                     <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Tempat Lahir</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee-> postulant->tempat_lahir}}"type="tel" class='tel' name="tempat_lahir" ></div>
                        <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">Tanggal Lahir</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="date"  value="{{$employee-> postulant->tanggal_lahir}}"class='tel' name="tanggal_lahir" ></div>
                        </div>   
                        <div class="field item form-group">
                     <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Jenis Kelamin</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee-> postulant->jenis_kelamin}}"type="tel" class='tel' name="jenis_kelamin" ></div>
                        <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">Status Perkawinan</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee-> postulant->status_perkawinan}}"class='tel' name="status_perkawinan" ></div>
                        </div>   
                        <div class="field item form-group">
                     <label for="alamatKtp" class="col-form-label col-md-2 col-sm-2 label-align">Agama</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee-> postulant->agama}}"type="tel" class='tel' name="agama" ></div>
                        <label for="kendaraan" class="col-form-label col-md-2 col-sm-2 label-align">Kebangsaan</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee-> postulant->kebangsaan}}"class='tel' name="kebangsaan" ></div>
                        </div>     
                      <div class="field item form-group">
                      <label for="agama" class="col-form-label col-md-2 col-sm-2 label-align">NPWP</label>
                      <div class="col-md-6 col-sm-6">
                      <input type="text" id="npwp" name="npwp"  class="form-control " value="{{$employee -> postulant-> npwp}}"></div>
                      </div>

                      <div class="field item form-group">
                      <label for="kebangsaan" class="col-form-label col-md-2 col-sm-2 label-align">Alamat Domisili</label>
                      <div class="col-md-6 col-sm-6">
                      <input type="text" id="alamat" name="alamat_domisili"  class="form-control " value="{{$employee -> postulant-> alamat_domisili}}"></div>
                     </div>

                      <div class="field item form-group">
                      <label for="npwp" class="col-form-label col-md-2 col-sm-2 label-align">Kota Domisili </label>
                      <div class="col-md-6 col-sm-6">
                      <input type="text" id="alamatKtp" name="kota_domisili"  class="form-control " value="{{$employee -> postulant-> kota_domisili}}"></div>
                      </div>
                      <div class="field item form-group">
                      <label for="alamat" class="col-form-label col-md-2 col-sm-2 label-align">Alamat KTP</label>
                      <div class="col-md-6 col-sm-6">
                      <input type="text" id="telepon" name="alamat_ktp"  class="form-control" value="{{$employee ->postulant->alamat_ktp }}"></div>
                     </div>
                     <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Kota KTP</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->postulant->kota_ktp}}"type="tel" class='tel' name="kota_ktp" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Telepon</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee->postulant->telepon}}"class='tel' name="telepon" ></div>
                    </div> 
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Email</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->postulant->email}}"type="tel" class='tel' name="email" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Kendaraan</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee->postulant->kendaraan}}"class='tel' name="kendaraan" ></div>
                    </div> 
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Mempunyai Laptop ?</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->postulant->laptop}}"type="tel" class='tel' name="laptop" ></div>
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Gaji Diharap</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="tel"  value="{{$employee->postulant->gaji_diharap}}"class='tel' name="gaji_diharap" ></div>
                    </div> 
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Anak Ke-</label>
                      <div class="col-md-1 col-sm-1">
                        <input class="form-control"  value="{{$employee->postulant->anak_ke}}"type="tel" class='tel' name="anak_ke" ></div>
                      <label class="col-form-label col-md-4 col-sm-4  label-align">Dari</label>
                      <div class="col-md-1 col-sm-1">
                        <input class="form-control" type="tel"  value="{{$employee->postulant->saudara }}"class='tel' name="saudara" ></div>
                        <label for="middle-name" class="col-form-label label-align">Bersaudara</label>
                </label>
                    </div> 
                    <div class="item form-group">
                     <label class="col-form-label col-md-2 col-sm-2 label-align"  for="last-name"  >Pencapaian Yang Sudah Dilakukan  </label>
                     <div class="col-md-8 col-sm-8 ">
                     <textarea class="form-control"  rows="3"  type="text" name="pencapaian_sudah_dilakukan">{{$employee ->postulant->pencapaian_sudah_dilakukan }}</textarea> </div>
                   </div>
                   <div class="item form-group">
                     <label class="col-form-label col-md-2 col-sm-2 label-align"  for="last-name"  >Pencapaian Yang Ingin Diraih  </label>
                     <div class="col-md-8 col-sm-8 ">
                     <textarea class="form-control"  rows="3"   type="text" name="pencapaian_yang_diinginkan"> {{$employee ->postulant->pencapaian_yang_diinginkan }}</textarea> </div>
                   </div>
        
                   <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Mulai Bergabung</label>
                      <div class="col-md-5 col-sm-5">
                        <input class="form-control" value="{{$employee->join_date}}" class='date' type="date" name="join_date" ></div>
                    </div>

                    <div class="field item form-group">
                    <label class="col-form-label col-md-2 col-sm-2  label-align">Mulai Addendum 1</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->start_addendum1}}"class='time' type="date" name="start" ></div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">Akhir Addendum 1</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" value="{{$employee->end_addendum1}}" type="date" name="end" /></div>
                      </div>

                      <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Mulai Addendum 2</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control"  value="{{$employee->start_addendum2}}" class='time' type="date" name="start2" ></div>
                        <label class="col-form-label col-md-2 col-sm-2  label-align">Akhir Addendum 2</label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" value="{{$employee->end_addendum2}}" type="date" name="end2" /></div>
                      </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-2 col-sm-2  label-align">Akhir Kontrak</label>
                      <div class="col-md-5 col-sm-5">
                        <input class="form-control"  value="{{$employee->end_contract}}"type="date" name="end_contract" /></div>
                    </div>

                       
                     
          <div class="ln_solid"></div>
          <div class="item form-group ">
            <div class="col-md-6 col-sm-6 offset-md-3 ">
              <button type="submit" class="btn btn-round btn-success">Save</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
  function previewImage(event){
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  </script>

@endsection
