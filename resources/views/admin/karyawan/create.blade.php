@extends('layouts.master')

@section('content')

      <!-- page content -->
        <div class="">
          <div class="page-title">
            <div class="title_left">
              <h3>Form Input Karyawan</h3>
            </div>

            <div class="title_right">
              <div class="col-md-5 col-sm-5 form-group pull-right top_search">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="row">
            <div class="col-md-12 col-sm-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Form Input Karyawan <small>PT Davinti Indonesia</small></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                          class="fa fa-wrench"></i></a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Settings 1</a>
                        <a class="dropdown-item" href="#">Settings 2</a>
                      </div>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <form action="{{url('admin/employees')}}" method="post" enctype="multipart/form-data"  class="form-horizontal form-label-left">
                       @csrf
                    <p>For alternative validation library <code>parsleyJS</code> check out in the <a
                        href="form.html">form page</a>
                    </p>
                    <span class="section">Personal Info</span>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Kode Karyawan</label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" name="kode" readonly placeholder="Automatic Generate" />
                      </div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Password</label>
                      <div class="col-md-6 col-sm-6">
                        <input class="form-control" class='optional' name="password" readonly  placeholder="Auto Generate Default Password" /></div>
                    </div>
                    <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Nama Karyawan</label>
                    <div class="col-md-8 col-sm-8">
                              <select name="nama_karyawan"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Nama Pelamar---</option>
                                @foreach ($postulant as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_lengkap}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Level</label>
                    <div class="col-md-4 col-sm-4">
                              <select name="level"  class="form-control col-md-7 col-xs-12">
                              <option value="0" disabled="true" selected="true">----Pilih Level ---</option>
                               <option >Admin</option>
                               <option >Pegawai</option>
                               <option >Atasan</option>
                               <option >Direktur</option>
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Jabatan</label>
                    <div class="col-md-5 col-sm-5">
                              <select name="jabatan"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Jenis Jabatan---</option>
                                @foreach ($position as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_jabatan}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Devisi</label>
                    <div class="col-md-5 col-sm-5">
                              <select name="devisi"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Jenis Divisi---</option>
                                @foreach ($division as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_devisi}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Grade</label>
                    <div class="col-md-5 col-sm-5">
                              <select name="grade"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Jenis Grade---</option>
                                @foreach ($grade as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_grade}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Corporate Group</label>
                    <div class="col-md-5 col-sm-5">
                              <select name="corporate"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Corporate ---</option>
                                @foreach ($corporate as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama_corporate_group}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Nama Atasan</label>
                    <div class="col-md-8 col-sm-8">
                              <select name="atasan"  class="form-control col-md-7 col-xs-12">
                               <option value="0" disabled="true" selected="true">----Pilih Atasan---</option>
                                @foreach ($employee as $k => $v)
                               <option value="{{$v->id}}">{{$v->nama}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                    <label class="col-form-label col-md-3 col-sm-3  label-align">Status</label>
                    <div class="col-md-6 col-sm-6">
                              <select name="status"  class="form-control col-md-7 col-xs-12">
                              <option value="0" disabled="true" selected="true">----Pilih Status ---</option>
                               <option >Kontrak</option>
                               <option >Tetap</option>
                               <option >Resign</option>
                              </select >
                           </div>
                        </div>
                        <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Mulai Bergabung<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" class='date' type="date" name="join_date" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Mulai Addendum 1<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" class='time' type="date" name="start" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Akhir Addendum 1<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="date" name="end" /></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Mulai Addendum 2<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" class='time' type="date" name="start2" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Akhir Addendum 2<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="date" name="end2" /></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Akhir Kontrak<span
                          class="required">*</span></label>
                      <div class="col-md-3 col-sm-3">
                        <input class="form-control" type="date" name="end_contract" /></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Jatah Cuti<span
                          class="required">*</span></label>
                      <div class="col-md-2 col-sm-2">
                        <input class="form-control" type="number" name="jatah_cuti" data-validate-linked='password'
                          required='required' /></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Gaji Pokok</label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="tel" class='tel' name="gaji" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Tunjangan Harian<span
                          class="required">*</span></label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="tel" class='tel' name="tunjangan_harian" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">Tunjangan Transport<span
                          class="required">*</span></label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="tel" class='tel' name="tunjangan_transport" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">BPJS Kesehatan<span
                          class="required">*</span></label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="tel" class='tel' name="kesehatan" ></div>
                    </div>
                    <div class="field item form-group">
                      <label class="col-form-label col-md-3 col-sm-3  label-align">BPJS Ketenagakerjaan<span
                          class="required">*</span></label>
                      <div class="col-md-4 col-sm-4">
                        <input class="form-control" type="tel" class='tel' name="ketenagakerjaan" ></div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="item form-group ">
                      <div class="col-md-6 col-sm-6 offset-md-3 ">
                        <button type="reset" class="btn btn-round btn-primary">Reset</button>
                        <button type="submit" class="btn btn-round btn-success">Save</button>
                      </div>
                    </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      <!-- /page content -->
@endsection
