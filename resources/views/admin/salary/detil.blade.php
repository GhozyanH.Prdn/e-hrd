@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Users <small>Some examples to get you started</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Aplikasi Gaji Karyawan  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
              <div class="col-md-12 ">
                <div class="x_panwwel">
                  <div class="x_content">
                    <form action="{{ url('/admin/salary/detil')}}" method="post" enctype="multipart/form-data" class="form-label-left input_mask">
                      @csrf
                      <div class="col-md-6 col-sm-6  form-group has-feedback">
                        <select name="bulan" class="form-control">
                          @foreach($bulan as $role)
                            <option value="{{$role->Bulan}}">{{$role->Bulan}}</option>
                           @endForeach
                        </select>
                      </div>
                      <div class="col-md-6 col-sm-6  form-group has-feedback">
                        <select name="tahun" class="form-control">
                          @foreach($tahun as $role)
                            <option value="{{$role->Tahun}}">{{$role->Tahun}}</option>
                           @endForeach
                        </select>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group row">
                        <div class="col-md-9 col-sm-9  offset-md-3">
                          <button type="submit" class="btn btn-success pull-right">Cek</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
          @if(session()->has('Alert'))
              <script>
                  alert({{ session()->get('Alert') }});
              </script>
          @endif

          <!-- @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif -->
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:120%">
        <thead>
          <tr>
            <th>No </th>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Nama Karyawan</th>
            <th>Gaji Pokok</th>
            <th>Total Gaji Bruto</th>
            <th>Total Pengurangan</th>
            <th>Gaji Diterima</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($detil as $v => $sly )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$sly -> Tahun}}</td>
            <td>{{$sly -> Bulan}}</td>
            <td>{{$sly -> Nama_Pegawai}}</td>
            <td>{{$sly -> Gaji_Pensiun}}</td>
            <td>{{$sly -> Jumlah_Penghasilan_Bruto}}</td>
            <td>{{$sly -> Jumlah_Pengurangan}}</td>
            <td>{{$sly -> Uang_Pembayaran_Gaji}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
