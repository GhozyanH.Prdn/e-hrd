@extends('layouts.master')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>PT Davinti Indonesia></h3>
  </div>
</div>



<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      
      
        <div class="clearfix"></div>
        <button type="button" class="btn btn-primary mr-5" data-toggle="modal" data-target="#importExcel">
			IMPORT EXCEL
		</button>
    </div>
    
    <div class="x_content">
    
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box table-responsive">
            <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tahun</th>
                  <th>Bulan</th>
                  <th>ID</th>
                  <th>Nama Pegawai</th>
                  <th>Kehadiran</th>
                  <th>Jumlah Cuti tak Dibayar</th>
                  <th>Gaji</th>
                  <th>Tunjangan</th>
                  <th>Tunjangan Lain Lain</th>
                  <th>Potongan Cuti</th>
                  <th>Bonus</th>
                  <th>Jumlah Pengurangan Gaji</th>
                  <th>Potongan Pajak</th>
                  <th>Total Gaji Bersih</th>
                
                  <th>Aksi</th>
                </tr>
              </thead>

              <tbody>
                @foreach ($gaji as $v => $gp )
                <tr>
                  <td>{{$loop -> iteration}}</td>
                  <td>{{$gp -> Tahun}}</td>
                  <td>{{$gp -> Bulan}}</td>
                  <td>{{$gp -> Id_Pegawai}}</td>
                  <td>{{$gp -> Nama_Pegawai}}</td>
                  <td>{{$gp -> Kehadiran}}</td>
                  <td>{{$gp -> Jumlah_Cuti_Tak_Dibayar}}</td>
                  <td>{{$gp -> Gaji_Pensiun}}</td>
                  <td>{{$gp -> Tunjangan_Harian}}</td>
                  <td>{{$gp -> Tunjangan_Lainnya_Uang_Lembur_dsb}}</td>
                  <td>{{$gp -> Cuti_Tidak_Dibayar}}</td>
                  <td>{{$gp -> Jumlah_Penghasilan_Bruto}}</td>
                  <td>{{$gp -> Jumlah_Pengurangan}}</td>
                  <td>{{$gp -> PPh_21_sebulan}}</td>
                  <td>{{$gp -> Uang_Pembayaran_Gaji}}</td>
                
                  <td>
                    <button class="btn-danger " ><a class="text-white" href="/admin/salary/deleted_permanent/{{$gp->id}}" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus Secara Permanen Data ini?')">Hapus</a></button>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>




		<!-- Import Excel -->
		<div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<form method="post" action="{{ url('/karyawan/import_excel')}}" enctype="multipart/form-data">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
						</div>
						<div class="modal-body">

							{{ csrf_field() }}
        
              <div class="form-group">
                   <label for="kode">Masukan Tahun</label>
                      <input type="text" name="tahun"  class="form-control" >
              </div>
              <div class="form-group">
                   <label for="kode">Masukan Bulan</label>
                
                          <select name="bulan" class="form-control">
                            <option  disabled="true" selected="true">Choose Option</option>
                            <option>Januari</option>
                            <option>Februari</option>
                            <option>Maret</option>
                            <option>April</option>
                            <option>Mei</option>
                            <option>Juni</option>
                            <option>Juli</option>
                            <option>Agustus</option>
                            <option>September</option>
                            <option>Oktober</option>
                            <option>November</option>
                            <option>Desember</option>
                          </select>
              </div>
							<label>Pilih file excel</label>
							<div class="form-group">
								<input type="file" name="file" required="required">
							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Import</button>
						</div>
					</div>
				</form>
			</div>
		</div>

@endsection
