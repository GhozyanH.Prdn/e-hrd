@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Users <small>Some examples to get you started</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Aplikasi Gaji  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
          <a href="salary/create" class="btn btn-custon-four btn-primary"><i class="fa fa-reply"></i> Hitung Gaji</a>
          <a href="salary/show" class="btn btn-custon-four btn-primary"><i class="fa fa-reply"></i> Detil Gaji</a>
          @if(session()->has('Alert'))
              <script>
                  alert({{ session()->get('Alert') }});
              </script>
          @endif

          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:120%">
        <thead>
          <tr>
            <th>No </th>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Total Take Home Pay</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($salary as $v => $sly )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$sly -> tahun}}</td>
            <td>{{$sly -> bulan}}</td>
            <td>{{$sly -> gaji}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
