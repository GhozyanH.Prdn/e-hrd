@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>PT Davinti Indonesia</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Daftar Jabatan</h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <!-- <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                </div>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li> -->
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
              <div class="row">
                  <div class="col-sm-12">
                    <div class="card-box table-responsive">
            <p class="text-muted font-13 m-b-30">
                <a href="" class="btn btn-custon-four btn-primary" data-toggle="modal" data-target="#modalTambahJabatan"><i class="fa fa-reply"></i> Tambah Jabatan</a>
                <a href="jabatan/trash" class="btn btn-custon-four btn-warning"><i class="fa fa-refresh fa-spin fa-fw"></i> Dapatkan Data Terhapus</a>
                @if (session('status'))
                <div class="alert alert-success alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>SUKSES !!!</strong> Data Jabatan Berhasil Ditambahkan.
                </div>
                @endif
      
                @if (session('edit'))
                <div class="alert alert-warning alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>SUKSES !!!</strong> Data Jabatan Berhasil Diubah.
                </div>
                @endif
      
                @if (session('delete'))
                <div class="alert alert-danger alert-dismissible " role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <strong>SUKSES !!!</strong> Data Jabatan Berhasil Dihapus.
                </div>
                @endif
            </p>
            <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Jabatan</th>
                  <th>Nama Jabatan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
      
      
              <tbody>
                @foreach ($positions as $v => $position )
                <tr>
                  <td>{{$loop -> iteration}}</td>
                  <td>{{$position -> kode_jabatan}}</td>
                  <td>{{$position -> nama_jabatan}}</td>
                  <td>
                      <a href="/admin/jabatan" class="btn btn-custon-four btn-warning" data-myidjab="{{$position -> id}}" data-mykodejab="{{$position -> kode_jabatan}}" data-mynamajab="{{$position -> nama_jabatan}}" data-toggle="modal" data-target="#modalEditJabatan">Ubah</a>
                      <form action="/admin/jabatan/{{$position->id}}" method="post" class="d-inline">
                      @method('DELETE')
                      @csrf
                      <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Apakah Anda Yakin Untuk Mengahpus Data Ini?')" >Hapus</button>
                      </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- --------- MODAL --------- -->

    <!-- --------- MODAL TAMBAH --------- -->
                  <div class="modal fade" id="modalTambahJabatan" tabindex="-1" role="dialog" aria-labelledby="modalTambahTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form id="formSave" method="POST" action="/admin/jabatan/">

                        <div class="modal-header">
                          <h5 class="modal-title" id="modalTambahTitle">Tambah Jabatan Baru</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                          <div class="modal-body">
                            @csrf
                            <div class="form-group">
                                  <label for="kode">Kode Jabatan</label>
                                  <input type="text" name="kodeJab" disabled class="form-control" placeholder="Automatic Generate">
                            </div>

                            <div class="form-group">
                                  <label for="nama">Nama Jabatan</label>
                                  <input type="text" class="form-control @error('namaJab') is-invalid @enderror" name="namaJab" placeholder="Masukan Nama Jabatan Baru" value ="{{ old('namaJab')}}">
                                  @error('namaJab')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                            </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL TAMBAH --------- -->

    <!-- --------- MODAL EDIT --------- -->
    <div class="modal fade" id="modalEditJabatan" tabindex="-1" role="dialog" aria-labelledby="modalEditTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                      <form action="{{route('jabatan.update', 'test')}}" id="editForm" method="post">
                      @method('patch')
                      @csrf
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalEditTitle">Edit Data Jabatan</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>


                          <div class="modal-body" id="editJabatan">
                              <input type="hidden" id="idjab" name="idJab" value="">

                              <div class="form-group">
                                <label for="kode">Kode Jabatan</label>
                                <input type="text" disabled class="form-control" id="kodejab" name="kodeJab" value="">
                              </div>

                              <div class="form-group">
                                <label for="kode">Kode Jabatan</label>
                                <input type="text" class="form-control @error('namaJab') is-invalid @enderror" id="namajab" name="namaJab" placeholder="Masukan Nama Jabatan Baru" value ="{{ old('namaJab')}}">
                                  @error('namaJab')
                                    <div class="invalid-feedback">Kolom Ini Tidak Boleh Kosong</div>
                                  @enderror
                              </div>
                          </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
    <!-- --------- END MODAL EDIT --------- -->

@endsection

