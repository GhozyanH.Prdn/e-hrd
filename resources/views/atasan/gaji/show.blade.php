@extends('layouts.master')

@section('content')
<div class="page-title">
  <div class="title_left">
    <h3>PT Davinti Indonesia</h3>
  </div>
</div>

<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Detail Gaji {{ Auth::user()->nama }}</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <div class="row">
        <div class="col-sm-12">
          <div class="card-box table-responsive">
          @foreach($detsal as $v => $ds)
            <ul class="messages">
              <div class=" item form-group">
                <label for="Tahun" class="col-form-label col-md-3 col-sm-3 label-align">Tahun</label>
                    <input type="text" id="Tahun" name="Tahun" disabled class="form-control col-md-8 col-sm-8" value="{{$ds -> Tahun}}">
              </div>
              <div class=" item form-group">
                <label for="Bulan" class="col-form-label col-md-3 col-sm-3 label-align">Bulan</label>
                    <input type="text" id="Bulan" name="Bulan" disabled class="form-control col-md-8 col-sm-8" value="{{$ds -> Bulan}}">
              </div>
              <br>
              <div class=" item form-group">
                <label for="JumlahKehadiran" class="col-form-label col-md-3 col-sm-3 label-align">Jumlah Kehadiran</label>
                    <input type="text" id="JumlahKehadiran" name="JumlahKehadiran" disabled class="form-control col-md-8 col-sm-8" value="{{$ds -> Kehadiran}} Hari Kerja">
              </div>
              <div class=" item form-group">
                <label for="GajiPokok" class="col-form-label col-md-3 col-sm-3 label-align">Gaji Pokok</label>
                    <input type="text" id="GajiPokok" name="GajiPokok" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Gaji_Pensiun}},-">
              </div>
              <div class=" item form-group">
                <label for="TunjanganHarian" class="col-form-label col-md-3 col-sm-3 label-align">Tunjangan Harian</label>
                    <input type="text" id="TunjanganHarian" name="TunjanganHarian" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Tunjangan_Harian}},-">
              </div>
              <div class=" item form-group">
                <label for="TunjanganLain" class="col-form-label col-md-3 col-sm-3 label-align">Tunjangan Lainnya</label>
                    <input type="text" id="TunjanganLain" name="TunjanganLain" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Tunjangan_Lainnya_Uang_Lembur_dsb}},-">
              </div>
              <div class=" item form-group">
                <label for="BonusTakTetap" class="col-form-label col-md-3 col-sm-3 label-align">Bonus Tak Tetap</label>
                    <input type="text" id="BonusTakTetap" name="BonusTakTetap" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Bonus_Tak_Tetap}},-">
              </div>
              <!-- <div class=" item form-group">
                <label for="PenghasilanBruto" class="col-form-label col-md-3 col-sm-3 label-align">Jumlah Penghasilan Bruto</label>
                    <input type="text" id="PenghasilanBruto" name="PenghasilanBruto" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Jumlah_Penghasilan_Bruto}},-">
              </div> -->
              <br>
              <div class=" item form-group">
                <label for="JumlahCutiTakDibayar" class="col-form-label col-md-3 col-sm-3 label-align">Jumlah Cuti Tidak Dibayar</label>
                    <input type="text" id="JumlahCutiTakDibayar" name="JumlahCutiTakDibayar" disabled class="form-control col-md-8 col-sm-8" value="{{$ds -> Jumlah_Cuti_Tak_Dibayar}} Hari Kerja">
              </div>
              <div class=" item form-group">
                <label for="PotonganCuti" class="col-form-label col-md-3 col-sm-3 label-align">Potongan Cuti Tak Dibayar</label>
                    <input type="text" id="PotonganCuti" name="PotonganCuti" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Cuti_Tidak_Dibayar}},-">
              </div>
              <div class=" item form-group">
                <label for="Pengurangan" class="col-form-label col-md-3 col-sm-3 label-align">Pengurangan</label>
                    <input type="text" id="Pengurangan" name="Pengurangan" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Jumlah_Pengurangan}},-">
              </div>
              <div class=" item form-group">
                <label for="Pph21" class="col-form-label col-md-3 col-sm-3 label-align">PPh21</label>
                    <input type="text" id="Pph21" name="Pph21" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> PPh_21_sebulan}},-">
              </div>
              <br>
            <div class="ln_solid"></div>

              <div class=" item form-group">
                <label for="TotalGajiDiterima" class="col-form-label col-md-3 col-sm-3 label-align">Total Gaji Diterima</label>
                    <input type="text" id="TotalGajiDiterima" name="TotalGajiDiterima" disabled class="form-control col-md-8 col-sm-8" value="Rp. {{$ds -> Uang_Pembayaran_Gaji}},-">
              </div>
            </ul>

            <div class="ln_solid"></div>
            <div class="item form-group">
              <div class="col-md-12 col-sm-12">
              <a href="/atasan/gajiatasan" class="btn btn-primary pull-right"><< Kembali</a>
              </div>
            </div>
          @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
