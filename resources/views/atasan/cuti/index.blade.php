@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Users <small>Some examples to get you started</small></h3>
              </div>
              <div class="title_left">
              <h3> <small>Sisa Jatah Cuti Anda :
              @foreach ($cuti4 as $v => $k )
              @php

              $cuti=$k->join('cuti_categorys', 'cuti_requests.id_cuti', '=', 'cuti_categorys.id')
              ->where('id_karyawan', Auth::user()->id)
              ->whereYear('tanggal_cuti', date('Y'))
              ->where('keputusan_hrd','=' ,'diterima')
              ->where('keputusan_atasan','=' ,'diterima')
              ->where('status','=' ,'True')
              ->sum('quantity');
              $awal=$k->employee->jatah_cuti;

              $value= $awal- $cuti; @endphp @endforeach
              @if(empty($value)) <button class="btn btn-round btn-info">{{auth()->user()->jatah_cuti}}</button>
              @elseif($value < 0) <button class="btn btn-round btn-info"> 0 </button>
              @else <button class="btn btn-round btn-info"> {{$value}} </button> @endif
               </small></h3>
            </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Halaman Pengajuan Cuti  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">

      <button type="button"  class="btn btn-custon-four btn-primary" data-toggle="modal" data-target=".modal-tambah-alternatif"><i class="fa fa-reply"></i> Ajukan Cuti</button>

          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No </th>
            <th>Tanggal Mengajukan</th>
            <th>Nama</th>
            <th>Nama Atasan</th>
            <th>tanggal Cuti </th>
            <th>Quantity</th>
            <th>Jenis Cuti</th>
            <th>Status Cuti</th>

          </tr>
        </thead>


        <tbody>
          @foreach ($cuti4 as $v => $ast )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$ast -> tanggal_request}}</td>
            <td>{{$ast -> employee->nama}}</td>
            <td>{{$ast -> employee->nama_atasan}}</td>
            <td>{{$ast -> tanggal_cuti}}</td>
            <td>{{$ast -> quantity}}</td>
            <td>{{$ast -> category->jenis_cuti}}</td>
           
            <td>
            <form  method="post">
            @if($ast->keputusan_hrd=='menunggu')  <button class="btn-warning " ><a class="text-white" href="/atasan/cuti_atasan/{{$ast -> id}}/detil">Belum Diputuskan</a></button>
            @elseif($ast->keputusan_hrd=='ditolak') <button class="btn-danger " ><a class="text-white" href="/atasan/cuti_atasan/{{$ast -> id}}/detil">Cuti Ditolak</a></button>
            @else <button class="btn-success " ><a class="text-white" href="/atasan/cuti_atasan/{{$ast -> id}}/detil">Cuti Diterima</a></button>@endif
            </form>

            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
      {{ $cuti4->links() }}
    </div>
  </div>
</div>
</div>
  </div>
</div>
</div>


<!-- ----------------------------------modal -------------------------------------------------------->


                <div class="modal fade modal-tambah-alternatif" tabindex="-1" role="dialog" aaria-hidden="true">
                    <div class="modal-dialog modal-lg " role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="myModalLabel">From Pengajuan Cuti</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <form action="{{url('atasan/cuti_atasan')}}" method="post" data-parsley-validate class="form-horizontal form-label-left">
                        @csrf
                      
                        <div class="item form-group">
                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Pilih Jenis Cuti</label>
                           <div class="col-md-8 col-sm-8 ">
                              <select name="jenis_cuti"  class="form-control col-md-7 col-xs-12">
                               <option value="">----Pilih Jenis Cuti---</option>
                                @foreach ($category as $k => $v)
                               <option value="{{$v->id}}">{{$v->jenis_cuti}}</option>
                                @endforeach
                              </select >
                           </div>
                        </div>
                        <div class="item form-group">
                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Taggal Cuti</label>
                           <div class="col-md-4 col-sm-4 ">
                             <input id="middle-name" class="form-control" type="date" name="tanggal_cuti">
                           </div>
                        </div>
                        <div class="item form-group">
                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantity</label>
                           <div class="col-md-3 col-sm-3 ">
                           <select name="quantity" class="form-control">
                            <option value="0" disabled="true" selected="true">Choose Option</option>
                            <option>0</option>
                            <option>0.5</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                            <option>11</option>
                            <option>12</option>
                            <option>13</option>
                            <option>14</option>
                            <option>15</option>
                            <option>16</option>
                            <option>17</option>
                            <option>18</option>
                            <option>19</option>
                            <option>20</option>
                          </select>
                           </div>
                        </div>
                        <div class="item form-group">
                         <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Alasan</label>
                           <div class="col-md-8 col-sm-8 ">
                           <textarea class="form-control"   rows="3"  type="text" name="alasan"></textarea>
                           </div>
                        </div>

                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal" id = "closeModalTambah" >Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>

                  @endsection
