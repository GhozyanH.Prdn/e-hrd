@extends('layouts.master')

@section('content')
<div class="page-title">

            </div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Detail Pengambilan Cuti  <small>PT Davinti Indonesia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>

          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form action="/atasan/cuti/{{$cuti->id}}" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">
          @method('patch')
          @csrf

          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Tanggal Request Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="tanggal_request" value="{{$cuti -> tanggal_request}}" readonly class="form-control" >
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nama Karyawan</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$cuti ->employee-> nama}}"  readonly type="text" name="nama_karyawan">
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Nama Atasan <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="nama_atasan" value="{{$cuti -> employee-> nama_atasan}}" readonly class="form-control" >
            </div>
          </div>

          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Tanggal Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="tanggal_cuti" value="{{$cuti -> tanggal_cuti}}" readonly class="form-control" >
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Quantity</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$cuti ->quantity}}"  readonly type="text" name="quantity">
            </div>
          </div>
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Alasan Mengambil Cuti <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
            <textarea class="form-control"  rows="3"  readonly type="text" name="alasan_cuti"> {{$cuti ->alasan}}</textarea>
            </div>
          </div>
        
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Catatan <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
            <textarea class="form-control"   rows="3" readonly type="text" name="catatan_anda">
            dari HRD : {{$cuti -> catatan_hrd}},
            dari Atasan : {{$cuti -> catatan_atasan}}
            </textarea>
            </div>
          </div>

          <!-- peruangan keputusan atasan -->
         



          <!-- peruangan keputusan atasan -->
          @php $button; @endphp
          @if($cuti -> keputusan_hrd  == 'menunggu' )  @php $button='btn btn-warning btn-lg'; @endphp
          @elseif ($cuti -> keputusan_hrd == 'diterima' ) @php $button='btn btn-success btn-lg'; @endphp
          @else  @php $button='btn btn-danger btn-lg'; @endphp
          @endif
          @php $data=$button @endphp
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align"> Keputusan HRD</label>
            <div class="col-md-2 col-sm-2 ">
            <button type="button" class="{{$data}}">{{$cuti -> keputusan_hrd}}</button>
            </div>



          <!-- peruangan keputusan atasan -->
          @php $button; @endphp
          @if($cuti -> keputusan_atasan  == 'menunggu' )  @php $button='btn btn-warning btn-lg'; @endphp
          @elseif ($cuti -> keputusan_atasan == 'diterima' ) @php $button='btn btn-success btn-lg'; @endphp
          @else  @php $button='btn btn-danger btn-lg'; @endphp
          @endif
          @php $data=$button @endphp
            <label for="middle-name" class="col-form-label col-md-2 col-sm-2 label-align"> Keputusan Atasan</label>
            <div class="col-md-2 col-sm-2 ">
            <button type="button" class="{{$data}}">{{$cuti -> keputusan_atasan}}</button>
            </div>
          </div>

          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-2 col-sm-2 label-align">Keputusan Final</label>
            <div class="col-md-9 col-sm-9 ">
            @if($cuti->keputusan_hrd=='diterima'&& $cuti->keputusan_atasan=='diterima') <input type="button"class="col-md-11 col-sm-11 btn btn-round btn-success btn-lg text-dark" value="Cuti Diterima" name="cuti_terpakai">
            @elseif($cuti->keputusan_hrd=='ditolak' or $cuti->keputusan_atasan=='ditolak')<input type="button"class="col-md-11 col-sm-11 btn btn-round btn-danger btn-lg text-white" value="Cuti Ditolak" name="cuti_terpakai">
            @else  <input type="button"class="col-md-11 col-sm-11 btn btn-round btn-warning btn-lg text-dark" value="Menunggu" name="cuti_terpakai">@endif

            </div>
          </div>




          <div class="ln_solid"></div>
          <div class="item form-group ">
            <div class="col-md-6 col-sm-6 offset-md-8 ">
              <a href="{{ '/atasan/persetujuan_atasan' }}" type="button" class="btn btn-round btn-primary ">Kembali</a>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

@endsection
