@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>PT Davinti Indonesia</h3>
              </div>

              <!-- <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div> -->
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Gaji {{ Auth::user()->nama }}</h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
          </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Tahun</th>
            <th>Bulan</th>
            <th>Total Gaji Diterima</th>
            <th>Detail</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($salary as $v => $sal )
          <tr>
            <td>{{$sal -> Tahun}}</td>
            <td>{{$sal -> Bulan}}</td>
            <td>Rp. {{$sal -> Uang_Pembayaran_Gaji}},-</td>
            <td>
              <a href="/pegawai/gajipegawai/{{$sal->id}}" class="btn btn-info">Detail Gaji</a>
              <a href="/pegawai/slipgajipeg/{{$sal->id}}" class="btn btn-warning">Unduh Slip Gaji</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>

@endsection
