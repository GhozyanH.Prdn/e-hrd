<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>PT Davinti Indonesia</title>

    <!-- Bootstrap -->
    <link href="/material/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/material/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/material/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/material/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

    <!-- Datatables -->

    <link href="/material/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- bootstrap-progressbar -->
    <link href="/material/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/material/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/material/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="/material/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    <!-- Custom Theme Style -->
    <link href="/material/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col menu_fixed">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-cogs"></i> <span>Davinti Indonesia</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              <img src="{{url('images/personal/' . Auth::user() ->postulant->foto)}}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
              <span>{{ date('l, d-m-Y') }}</span>
              <h2>{{ Auth::user()->nama }}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>{{Auth::user()->role}}</h3>
              <ul class="nav side-menu">


                <li><a href="{{ route('home') }}"><i class="fa fa-home"></i> Home</a>
                </li>


                @if(Auth::user()->role=='Admin' || Auth::user()->role=='Direktur')
                <li><a><i class="fa fa-users"></i> Karyawan <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/admin/employees') }}">Data Karyawan</a></li>
                    <li><a href="{{ url('/admin/resign') }}">Daftar Karyawan Resign</a></li>
                  </ul>
                </li>
                @endif

                <!-- @if(Auth::user()->role=='Pegawai')
                <li><a href="{{ url('/data') }}"><i class="fa fa-edit"></i> Data</a>
                </li>
                @endif -->

                @if(Auth::user()->role=='Admin' || Auth::user()->role=='Direktur')
                <li><a href="{{ url('admin/assets') }}"><i class="fa fa-desktop"></i> Aset</a>
                </li>
                @endif


                @if(Auth::user()->role=='Admin' || Auth::user()->role=='Direktur')
                <li><a><i class="fa fa-money"></i> Gaji <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/admin/salary') }}">Hitung Gaji</a></li>
                    <li><a href="{{ url('/admin/gajiadmin') }}">Gajiku</a></li>
                  </ul>
                </li>
                @endif

                @if(Auth::user()->role=='Admin')
                <li><a><i class="fa fa-user-times"></i> Cuti <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/admin/cuti') }}">Persetujuan Cuti HRD</a></li>
                    <li><a href="{{ url('/admin/admin_atasan') }}">Persetujuan Cuti Atasan</a></li>
                    <li><a href="{{ url('/admin/category') }}">Kategori Cuti</a></li>
                    <li><a href="{{ url('/admin/rekapcuti') }}">Rekapan Cuti</a></li>
                  </ul>
                </li>
                @endif


                @if(Auth::user()->role=='Direktur')
                <li><a><i class="fa fa-user-times"></i> Cuti <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/direktur/persetujuan_direktur') }}">Data Pengajuan Cuti</a></li>
                    <li><a href="{{ url('/direktur/categorydir') }}">Kategori Cuti</a></li>
                  </ul>
                </li>
                @endif

                @if(Auth::user()->role=='Atasan')
                <li><a href="{{ url('/data') }}"><i class="fa fa-edit"></i> Data</a>
                </li>
                <li><a><i class="fa fa-user-times"></i> Cuti <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/atasan/cuti_atasan') }}"> Pengajuan Cuti</a></li>
                    <li><a href="{{ url('/atasan/persetujuan_atasan') }}">Form Persetujuan Cuti</a></li>
                  </ul>
                </li>
                <li><a href="{{ url('/atasan/gajiatasan') }}"><i class="fa fa-money"></i> Gaji </a>
                </li>
                @endif

                @if(Auth::user()->role=='Pegawai')
                <li><a href="{{ url('/data') }}"><i class="fa fa-edit"></i> Data</a>
                </li>
                <li><a href="{{ url('/pegawai/cutipegawai') }}"><i class="fa fa-user-times"></i> Cuti </a>
                </li>
                <li><a href="{{ url('/pegawai/gajipegawai') }}"><i class="fa fa-money"></i> Gaji </a>
                </li>
                @endif

                @if(Auth::user()->role=='Admin' || Auth::user()->role=='Direktur')
                <li><a href="{{ url('/admin/interview') }}"><i class="fa fa-envelope"></i> Data Interview </a>
                </li>
                @endif
                

                @if(Auth::user()->role=='Admin'|| Auth::user()->role=='Direktur')
                <li><a><i class="fa fa-edit"></i> Kelola Manajemen <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ url('/admin/jabatan') }}">Jabatan</a></li>
                    <li><a href="{{ url('/admin/divisi') }}">Divisi</a></li>
                    <li><a href="{{ url('/admin/grade') }}">Grade</a></li>
                    <li><a href="{{ url('/admin/corporate') }}">Corporate Group</a></li>
                  </ul>
                </li>
                @endif

              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right">
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <img src="/material/images/img.jpg" alt="">{{ Auth::user()->nama }}
                  </a>
                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="javascript:;"> Profile</a>
                      <a class="dropdown-item"  href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                  <a class="dropdown-item"  href="javascript:;">Help</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i>
                                        {{ __('Log out') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                    </form>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
            @yield('content')
        </div>
      </div>
        <!-- /page content -->
        <!-- page content -->

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="/material/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/material/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="/material/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/material/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/material/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/material/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/material/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/material/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/material/vendors/skycons/skycons.js"></script>
    <!-- Datatables -->
    <script src="/material/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/material/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/material/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/material/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/material/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/material/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/material/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/material/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Flot -->
    <script src="/material/vendors/Flot/jquery.flot.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/material/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/material/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/material/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/material/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/material/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/material/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/material/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/material/vendors/moment/min/moment.min.js"></script>
    <script src="/material/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="/material/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/material/build/js/custom.min.js"></script>
    @yield('chartKar')
    @yield('chartasset')
    @yield('chart')
    @yield('org')


    <script>
      $('#modalEditDivisi').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idDiv = button.data('myiddiv')
        var kodeDiv = button.data('mykodediv')
        var namaDiv = button.data('mynamadiv')
        var modal = $(this)

        modal.find('#editDivisi #iddiv').val(idDiv);
        modal.find('#editDivisi #kodediv').val(kodeDiv);
        modal.find('#editDivisi #namadiv').val(namaDiv);

        modal.find('#editDivisi #id').val(idDiv);
        modal.find('#editDivisi #kode').val(kodeDiv);
        modal.find('#editDivisi #nama').val(namaDiv);

      })
    </script>

<!-- Edit Corporate -->
    <script>
      $('#modalEditCorporate').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idCor = button.data('myidcor')
        var kodeCor = button.data('mykodecor')
        var namaCor = button.data('mynamacor')
        var modal = $(this)

        modal.find('#editCorporate #idcor').val(idCor);
        modal.find('#editCorporate #kodecor').val(kodeCor);
        modal.find('#editCorporate #namacor').val(namaCor);
      })
    </script>

<!-- Edit Jabatan -->
    <script>
      $('#modalEditJabatan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idJab = button.data('myidjab')
        var kodeJab = button.data('mykodejab')
        var namaJab = button.data('mynamajab')
        var modal = $(this)

        modal.find('#editJabatan #idjab').val(idJab);
        modal.find('#editJabatan #kodejab').val(kodeJab);
        modal.find('#editJabatan #namajab').val(namaJab);
      })
    </script>

<!-- Edit Grade -->
    <script>
      $('#modalEditGrade').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idGrd = button.data('myidgrd')
        var kodeGrd = button.data('mykodegrd')
        var namaGrd = button.data('mynamagrd')
        var modal = $(this)

        modal.find('#editGrade #idgrd').val(idGrd);
        modal.find('#editGrade #kodegrd').val(kodeGrd);
        modal.find('#editGrade #namagrd').val(namaGrd);
      })
    </script>


<!-- Edit Category -->
<script>
      $('#modalEditCategory').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idCc = button.data('myidcc')
        var kodeCc = button.data('mykodecc')
        var jenisCc = button.data('myjeniscc')
        var ketCc = button.data('myketcc')
        var valueCc = button.data('myvaluecc')
        var modal = $(this)

        modal.find('#editCategory #idcc').val(idCc);
        modal.find('#editCategory #kodecc').val(kodeCc);
        modal.find('#editCategory #jeniscc').val(jenisCc);
        modal.find('#editCategory #ketcc').val(ketCc);
        modal.find('#editCategory #valuecc').val(valueCc);
      })
    </script>

<!-- Edit Keputusan -->
<script>
      $('#modalKeputusan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')
        var keputusanIntv = button.data('mykeputusanintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editKeputusan #idintv').val(idIntv);
        modal.find('#editKeputusan #namaintv').val(namaIntv);
        modal.find('#editKeputusan #posisiintv').val(posisiIntv);
        modal.find('#editKeputusan #keputusanintv').val(keputusanIntv);
        modal.find('#editKeputusan #catatanintv').val(catatanIntv);
        modal.find('#editKeputusan #alasanintv').val(alasanIntv);
      })
    </script>

<!-- Edit Catatan -->
<script>
      $('#modalCatatan').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editCatatan #idintv').val(idIntv);
        modal.find('#editCatatan #namaintv').val(namaIntv);
        modal.find('#editCatatan #posisiintv').val(posisiIntv);
        modal.find('#editCatatan #catatanintv').val(catatanIntv);
        modal.find('#editCatatan #alasanintv').val(alasanIntv);
      })
    </script>

<!-- Edit Keputusan -->
<script>
      $('#modalKeputusan2').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idIntv = button.data('myidintv')
        var namaIntv = button.data('mynamaintv')
        var posisiIntv = button.data('myposisiintv')
        var keputusanIntv = button.data('mykeputusanintv')
        var catatanIntv = button.data('mycatatanintv')
        var alasanIntv = button.data('myalasanintv')
        var modal = $(this)

        modal.find('#editKeputusan2 #idintv').val(idIntv);
        modal.find('#editKeputusan2 #namaintv').val(namaIntv);
        modal.find('#editKeputusan2 #posisiintv').val(posisiIntv);
        modal.find('#editKeputusan2 #keputusanintv').val(keputusanIntv);
        modal.find('#editKeputusan2 #catatanintv').val(catatanIntv);
        modal.find('#editKeputusan2 #alasanintv').val(alasanIntv);
      })
    </script>

  <!-- Edit Data Diri -->
<script>
      $('#modalEditDataDiri').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idPel = button.data('myidpel')
        var namaPel = button.data('mynamapel')
        var posisiPel = button.data('myposisipel')
        var tempatLahirPel = button.data('mytempatlahirpel')
        var tanggalLahirPel = button.data('mytanggallahirpel')
        var ktpPel = button.data('myktppel')
        var jenKelPel = button.data('myjenkelpel')
        var anakKe = button.data('myanakke')
        var jmlhs = button.data('myjmlhs')
        var statPel = button.data('mystatpel')
        var agmPel = button.data('myagmpel')
        var bangsaPel = button.data('mybangsapel')
        var npwpPel = button.data('mynpwppel')
        var alDoPel = button.data('myaldopel')
        var alKtpPel = button.data('myalktppel')
        var telPel = button.data('mytelpel')
        var emPriPel = button.data('myempripel')
        var kendPel = button.data('mykendpel')
        var laptPel = button.data('mylaptpel')
        var gajiHrpPel = button.data('mygajihrppel')
        var capSdhPel = button.data('mycapsdhpel')
        var capBlmPel = button.data('mycapblmpel')
        var catatanPel = button.data('mycatatanpel')
        var statusPel = button.data('mystatuspel')
        var alasanPel = button.data('myalasanpel')
        var modal = $(this)

        modal.find('#editDataDiri #idpel').val(idPel);
        modal.find('#editDataDiri #namapel').val(namaPel);
        modal.find('#editDataDiri #posisipel').val(posisiPel);
        modal.find('#editDataDiri #tempatLahirpel').val(tempatLahirPel);
        modal.find('#editDataDiri #tanggalLahirpel').val(tanggalLahirPel);
        modal.find('#editDataDiri #ktppel').val(ktpPel);
        modal.find('#editDataDiri #jenKelpel').val(jenKelPel);
        modal.find('#editDataDiri #anakkepel').val(anakKe);
        modal.find('#editDataDiri #jmlhspel').val(jmlhs);
        modal.find('#editDataDiri #statpel').val(statPel);
        modal.find('#editDataDiri #agmpel').val(agmPel);
        modal.find('#editDataDiri #bangsapel').val(bangsaPel);
        modal.find('#editDataDiri #npwppel').val(npwpPel);
        modal.find('#editDataDiri #alDopel').val(alDoPel);
        modal.find('#editDataDiri #alKtppel').val(alKtpPel);
        modal.find('#editDataDiri #telpel').val(telPel);
        modal.find('#editDataDiri #emPripel').val(emPriPel);
        modal.find('#editDataDiri #kendpel').val(kendPel);
        modal.find('#editDataDiri #laptpel').val(laptPel);
        modal.find('#editDataDiri #gajiHrppel').val(gajiHrpPel);
        modal.find('#editDataDiri #capSdhpel').val(capSdhPel);
        modal.find('#editDataDiri #capBlmpel').val(capBlmPel);
        modal.find('#editDataDiri #catatanpel').val(catatanPel);
        modal.find('#editDataDiri #statuspel').val(statusPel);
        modal.find('#editDataDiri #alasanpel').val(alasanPel);
      })
    </script>

<!-- Edit Data KEluarga -->
<script>
      $('#modalEditKeluarga').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idFam = button.data('myidfam')
        var hubFam = button.data('myhubfam')
        var namFam = button.data('mynamfam')
        var jenkelFam = button.data('myjenkelfam')
        var usiFam = button.data('myusifam')
        var penFam = button.data('mypenfam')
        var pekFam = button.data('mypekfam')
        var modal = $(this)

        modal.find('#editDataKeluarga #idfam').val(idFam);
        modal.find('#editDataKeluarga #hubfam').val(hubFam);
        modal.find('#editDataKeluarga #namfam').val(namFam);
        modal.find('#editDataKeluarga #jenkelfam').val(jenkelFam);
        modal.find('#editDataKeluarga #usifam').val(usiFam);
        modal.find('#editDataKeluarga #penfam').val(penFam);
        modal.find('#editDataKeluarga #pekfam').val(pekFam);
      })
    </script>

<!-- Edit Data JOB HISTORY -->
<script>
      $('#modalEditJh').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idjh = button.data('myidjh')
        var namjh = button.data('mynamjh')
        var posjh = button.data('myposjh')
        var tglmjh = button.data('mytglmjh')
        var tglsjh = button.data('mytglsjh')
        var alsjh = button.data('myalsjh')
        var modal = $(this)

        modal.find('#editDataJh #idjh').val(idjh);
        modal.find('#editDataJh #namjh').val(namjh);
        modal.find('#editDataJh #posjh').val(posjh);
        modal.find('#editDataJh #tglmjh').val(tglmjh);
        modal.find('#editDataJh #tglsjh').val(tglsjh);
        modal.find('#editDataJh #alsjh').val(alsjh);
      })
    </script>

<!-- Edit Data BAHASA -->
<script>
      $('#modalEditBhs').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idBhs = button.data('myidbhs')
        var bhsa = button.data('mybhsa')
        var oralBhs = button.data('myoral')
        var writBhs = button.data('mywrit')
        var readBhs = button.data('myread')
        var modal = $(this)

        modal.find('#editDataBhs #idbhs').val(idBhs);
        modal.find('#editDataBhs #bhsa').val(bhsa);
        modal.find('#editDataBhs #oralbhs').val(oralBhs);
        modal.find('#editDataBhs #writbhs').val(writBhs);
        modal.find('#editDataBhs #readbhs').val(readBhs);
      })
    </script>

<!-- Edit Data Reference -->
<script>
      $('#modalEditRef').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idRef = button.data('myidref')
        var perusRef = button.data('myperus')
        var namaRef = button.data('mynama')
        var jbtnRef = button.data('myjbtn')
        var modal = $(this)

        modal.find('#editDataRef #idref').val(idRef);
        modal.find('#editDataRef #perusref').val(perusRef);
        modal.find('#editDataRef #namaref').val(namaRef);
        modal.find('#editDataRef #jbtnref').val(jbtnRef);
      })
    </script>

<!-- Edit Data Education -->
<script>
      $('#modalEditEdu').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idedu = button.data('myidedu')
        var jenpen = button.data('myjenpen')
        var namsek = button.data('mynamsek')
        var jrsn = button.data('myjrsn')
        var kota = button.data('mykota')
        var thnm = button.data('mythnm')
        var thns = button.data('mythns')
        var modal = $(this)

        modal.find('#editDataEdu #idedu').val(idedu);
        modal.find('#editDataEdu #jenpenedu').val(jenpen);
        modal.find('#editDataEdu #namsekedu').val(namsek);
        modal.find('#editDataEdu #jrsnedu').val(jrsn);
        modal.find('#editDataEdu #kotaedu').val(kota);
        modal.find('#editDataEdu #thnmedu').val(thnm);
        modal.find('#editDataEdu #thnsedu').val(thns);
      })
    </script>

<!-- Edit Data Organization -->
<script>
      $('#modalEditOrg').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idorg = button.data('myidorg')
        var namor = button.data('mynamor')
        var jbtn = button.data('myjbtn')
        var thnm = button.data('mythnm')
        var thns = button.data('mythns')
        var modal = $(this)

        modal.find('#editDataOrg #idorg').val(idorg);
        modal.find('#editDataOrg #namororg').val(namor);
        modal.find('#editDataOrg #jbtnorg').val(jbtn);
        modal.find('#editDataOrg #thnmorg').val(thnm);
        modal.find('#editDataOrg #thnsorg').val(thns);
      })
    </script>

<!-- Edit Data Award -->
<script>
      $('#modalEditAw').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idaw = button.data('myidaw')
        var nampres = button.data('mynampres')
        var period = button.data('myperiod')
        var modal = $(this)

        modal.find('#editDataAw #idaw').val(idaw);
        modal.find('#editDataAw #nampresaw').val(nampres);
        modal.find('#editDataAw #periodaw').val(period);
      })
    </script>

<!-- Edit Data Training -->
<script>
      $('#modalEditTrn').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idtrn = button.data('myidtrn')
        var jenpel = button.data('myjenpel')
        var nampel = button.data('mynampel')
        var tempel = button.data('mytempel')
        var tglm = button.data('mytglm')
        var tgls = button.data('mytgls')
        var modal = $(this)

        modal.find('#editDataTrn #idtrn').val(idtrn);
        modal.find('#editDataTrn #jenpeltrn').val(jenpel);
        modal.find('#editDataTrn #nampeltrn').val(nampel);
        modal.find('#editDataTrn #tempeltrn').val(tempel);
        modal.find('#editDataTrn #tglmtrn').val(tglm);
        modal.find('#editDataTrn #tglstrn').val(tgls);
      })
    </script>

<!-- Edit Data EC -->
<script>
      $('#modalEditEc').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idec = button.data('myidec')
        var nama = button.data('mynama')
        var hubungan = button.data('myhbgn')
        var alamat = button.data('myalmt')
        var tlp = button.data('mytlp')
        var modal = $(this)

        modal.find('#editDataEc #idec').val(idec);
        modal.find('#editDataEc #namaec').val(nama);
        modal.find('#editDataEc #hubunganec').val(hubungan);
        modal.find('#editDataEc #almtec').val(alamat);
        modal.find('#editDataEc #tlpec').val(tlp);
      })
    </script>

<!-- Edit Data TEST -->
<script>
      $('#modalEditTest').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idtest = button.data('myidtest')
        var chn = button.data('mychn')
        var chg = button.data('mychg')
        var cha = button.data('mycha')
        var chl = button.data('mychl')
        var chp = button.data('mychp')
        var chi = button.data('mychi')
        var cht = button.data('mycht')
        var chv = button.data('mychv')
        var chx = button.data('mychx')
        var chs = button.data('mychd')
        var chb = button.data('mychb')
        var cho = button.data('mycho')
        var chr = button.data('mychr')
        var chd = button.data('mychd')
        var chc = button.data('mychc')
        var chz = button.data('mychz')
        var che = button.data('myche')
        var chk = button.data('mychk')
        var chf = button.data('mychf')
        var chw = button.data('mychw')
        var modal = $(this)

        modal.find('#editDataTest #idtest').val(idtest);
        modal.find('#editDataTest #jchn').val(chn);
        modal.find('#editDataTest #jchg').val(chg);
        modal.find('#editDataTest #jcha').val(cha);
        modal.find('#editDataTest #jchl').val(chl);
        modal.find('#editDataTest #jchp').val(chp);
        modal.find('#editDataTest #jchi').val(chi);
        modal.find('#editDataTest #jcht').val(cht);
        modal.find('#editDataTest #jchv').val(chv);
        modal.find('#editDataTest #jchx').val(chx);
        modal.find('#editDataTest #jchs').val(chs);
        modal.find('#editDataTest #jchb').val(chb);
        modal.find('#editDataTest #jcho').val(cho);
        modal.find('#editDataTest #jchr').val(chr);
        modal.find('#editDataTest #jchd').val(chd);
        modal.find('#editDataTest #jchc').val(chc);
        modal.find('#editDataTest #jchz').val(chz);
        modal.find('#editDataTest #jche').val(che);
        modal.find('#editDataTest #jchk').val(chk);
        modal.find('#editDataTest #jchf').val(chf);
        modal.find('#editDataTest #jchw').val(chw);
        })
    </script>

    <script>
      // jquerynya
      $('.btn btn-custon-four btn-primary').on('click', function (event) {
          event.preventDefault();
          // tangkap data2 yang dibutuhkan untuk dimunculkan di modal
          // berdasarkan kode agan, yang agan butuhkan di modal hanya $post->body jadi tangkap value ini
          var postBody = $(this).parents('.post').find('.article>p').text();

          // isi data2 modal yang dibutuhkan
          $('#comment-modal .modal-body>text').text(postBody);
          // munculkan modal
          $('#comment-modal').modal();
      });
  </script>
    @include('sweetalert::alert')
  </body>
</html>
