
@extends('layouts.postulant')

@section('content')
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">

    <div class="x_title">
      <h2>Form  <small>Data Pengalaman Organisasi</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <!-- <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li> -->
      </ul>
      <div class="clearfix"></div>
      @if(session()->has('Alert'))
          <script>
              alert({{ session()->get('Alert') }});
          </script>
      @endif
    </div>
    <div class="x_content">

      <div >
        <form action="{{route('personal.organization.add')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
          @csrf
          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Organisasi <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" class="form-control @error('nama_organisasi') is-invalid @enderror" id="nama_organisasi" name="nama_organisasi" value ="{{ old('nama_organisasi')}}">
              @error('nama_organisasi')
                <div class="invalid-feedback">Kolom Ini Harus Diisi</div>
              @enderror
            </div>
          </div>


          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Jabatan <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" class="form-control @error('jabatan') is-invalid @enderror" id="jabatan" name="jabatan" value ="{{ old('jabatan')}}">
              @error('jabatan')
                <div class="invalid-feedback">Kolom Ini Harus Diisi</div>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Tahun Mulai <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" class="form-control @error('tahun_mulai') is-invalid @enderror" id="tahun_mulai" name="tahun_mulai" value ="{{ old('tahun_mulai')}}">
              @error('tahun_mulai')
                <div class="invalid-feedback">Kolom Ini Harus Diisi</div>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name"> Tahun Selesai <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" class="form-control @error('tahun_selesai') is-invalid @enderror" id="tahun_selesai" name="tahun_selesai" value ="{{ old('tahun_selesai')}}">
              @error('tahun_selesai')
                <div class="invalid-feedback">Kolom Ini Harus Diisi</div>
              @enderror
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">
            </label>
            <div class="col-md-6 col-sm-6 ">
              <div class="checkbox">
                <label>
                  <input type="checkbox" class="flat" value="no" id="cekorg" name="cekorg"> Saya Tidak Memiliki Riwayat Organisasi
                </label>
              </div>
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="item form-group">
            <div class="col-md-8 col-sm-8 offset-md-3">
              <a href="/personal/award" class="btn btn-info pull-right">Isi Riwayat Prestasi</a>
              <button type="submit" class="btn btn-success pull-right" >Submit</button>
            </div>
          </div>
        </form>
      </div>

    </div>
    </div>
  </div>
  <div class="x_panel">
  <div class="x_title">
    <h2> <small> Data Pengalaman Organisasi </small></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <!-- <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Settings 1</a>
            <a class="dropdown-item" href="#">Settings 2</a>
          </div>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li> -->
    </ul>
    <div class="clearfix"></div>
    @if (session('delete'))
    <div class="alert alert-success alert-danger " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>Success!</strong> Data Pengalaman Organisasi Berhasil di Hapus !
    </div>
    @endif
  </div>
  <div class="x_content">
  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th class="column-title">No </th>
          <th class="column-title">Nama Organisasi </th>
          <th class="column-title">Jabatan </th>
          <th class="column-title">Tahun Mulai </th>
          <th class="column-title">Tahun Selesai </th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
        </tr>
      </thead>

      <tbody>
        @foreach ($tampil as $v => $ast )
        <tr class="even pointer">

          <td class=" ">{{$loop -> iteration}}</td>
          <td class=" ">{{$ast -> nama_organisasi}}</td>
          <td class=" ">{{$ast -> jabatan}}</td>
          <td class=" ">{{$ast -> tahun_mulai}}</td>
          <td class=" ">{{$ast -> tahun_selesai}}</td>
          <td class=" last">
            <form action="/personal/organization/{{$ast -> id}}" method="post" class="d-inline">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" style="font-size:15px"></i></button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  </div>
</div>

@endsection
