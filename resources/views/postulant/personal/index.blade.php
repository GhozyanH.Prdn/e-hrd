@extends('layouts.postulant')

@section('content')
<div class="page-title">
  <div class="col-md-12 col-sm-12">
    <h3>Hai, {{Auth::user()->nama_lengkap}}</h3>
    <h3>Terimakasih Telah Bersedia Melamar di PT. Davinti Indonesia</h3>
  </div>
</div>

<div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12  ">
      <div class="x_panel">

        <div class="x_title">
          <h2>Silahkan Mengisi Form Lamaran Dibawah Ini</h2>
          <div class="clearfix"></div>
        </div>

        <div class="x_content">
            @if($cekall < 10)
            <div class="alert alert-danger">
              <button type="button" class="close" >
              </button>
              <strong>Form Lamaran Belum Lengkap!</strong>  Silahkan Isi Form Lamaran Dibawah Yang Memiliki Tanda +
            </div>

            @else
            <div class="alert alert-success">
              <button type="button" class="close" >
              </button>
              <strong>Form Lamaran Sudah Lengkap!</strong>  Silahkan Melakukan Test Kepribadian
            </div>
            @endif

          <div class="row">


            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6">
              <div class="tile-stats">
              @if($cekpost >= 1)
              <a href="{{ route('personal.data') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.data') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Data</small></div>
                <div class="count"><small>Diri</small></div>
                </a>
              </div>
            </div>


            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cekfam >= 1)
              <a href="{{ route('personal.family') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.family') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Data</small></div>
                <div class="count"><small>Keluarga</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cekedu >= 1)
              <a href="{{ route('personal.education') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.education') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Data</small></div>
                <div class="count"><small>Pendidikan</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cekjhis >= 1)
              <a href="{{ route('personal.old_job') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.old_job') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Riwayat</small></div>
                <div class="count"><small>Pekerjaan</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cektra >= 1)
              <a href="{{ route('personal.training') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.training') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Riwayat</small></div>
                <div class="count"><small>Pelatihan</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cekorg >= 1)
              <a href="{{ route('personal.organization') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.organization') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Pengalaman</small></div>
                <div class="count"><small>Organisasi</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($cekawd >= 1)
              <a href="{{ route('personal.award') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.award') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Riwayat</small></div>
                <div class="count"><small>Prestasi</small></div>
              </a>
              </div>
            </div>

            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
              @if($ceklang >= 1)
              <a href="{{ route('personal.language') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
              @else
              <a href="{{ route('personal.language') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
              @endif
                <div class="count"><small>Penguasaan</small></div>
                <div class="count"><small>Bahasa</small></div>
              </a>
              </div>
            </div>


            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
              <div class="tile-stats">
                @if($cekemc >= 1)
                <a href="{{ route('personal.emergencycontact') }}">
                <div class="icon"><i class="fa fa-check-square-o"></i></div>
                @else
                <a href="{{ route('personal.emergencycontact') }}">
                <div class="icon"><i class="fa fa-plus-square-o"></i></div>
                @endif
              <div class="count"><small>Kontak</small></div>
                <div class="count"><small>Darurat</small></div>
              </a>
              </div>
            </div>

          <div class="animated flipInY col-lg-4 col-md-4 col-sm-6  ">
            <div class="tile-stats">
            @if($cekref >= 1)
            <a href="{{ route('personal.reference') }}">
              <div class="icon"><i class="fa fa-check-square-o"></i></div>
            @else
            <a href="{{ route('personal.reference') }}">
              <div class="icon"><i class="fa fa-plus-square-o"></i></div>
            @endif
              <div class="count"><small>Referensi</small></div>
            </a>
            </div>
          </div>

          <div class="animated flipInY col-lg-8 col-md-8 col-sm-6  ">
            <div class="tile-stats">
            @if($cektest >= 1)
            <a href="{{ route('personal.test') }}">
              <div class="icon"><i class="fa fa-check-square-o"></i></div>
            @elseif($cekall < 9 )
            <a href="{{ route('personal.test') }}">
              <div class="icon"><i class="fa fa-minus-square-o"></i></div>
            @else
            <a href="{{ route('personal.test') }}">
              <div class="icon"><i class="fa fa-plus-square-o"></i></div>
            @endif
              <div class="count"><small>Test Kepribadian</small></div>
            </a>
            </div>
          </div>

          </div>
        </div>

      </div>
    </div>
  </div>
@endsection
