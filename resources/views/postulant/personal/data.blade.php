@extends('layouts.postulant')

@section('content')

  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">

      <div class="x_title">
        <h2>Silahkan Isi Form<small>Data Diri</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
        @if(session()->has('Alert'))
            <script>
                alert({{ session()->get('Alert') }});
            </script>
        @endif
      </div>
      <div class="x_content">

          <div >
            <form action="/personal/data" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
              @method('patch')
              @csrf
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Nama Lengkap <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="nama_lengkap" name="nama_lengkap" value="{{ $personal ['nama_lengkap']}}" required="required" class="form-control  ">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Email Registrasi <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="email" name="email" required="required" class="form-control" value="{{ $personal ['email']}}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Posisi Yang Dipilih <span class="required">*</span>
                </label>
                            <div class="col-md-6 col-sm-6 ">
                              <select name="posisi_yang_dilamar" class="form-control">
                                <option value="0" disabled="true" selected="true">Choose Option</option>
                                @foreach ($position_array as $data)
                                      <option value="{{ $data->id }}" {{ $dropdown == $data->id ? 'selected="selected"' : '' }} >{{ $data->nama_jabatan }}</option>
                                @endforeach
                              </select>
                            </div>
              </div>
              <div class="form-group row">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tempat, Tanggal Lahir <span class="required">*</span></label>
                  <div class="col-md-3 col-sm-3  form-group has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="tempat_lahir" name="tempat_lahir" value="{{ $personal ['tempat_lahir']}}" placeholder="Tempat Lahir">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>

                  <div class="col-md-3 col-sm-3  form-group has-feedback">
                      <input type="date" class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="{{ $personal ['tanggal_lahir']}}" placeholder="Tanggal Lahir"></input>
                  </div>


              </div>

              <div class="form-group row">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nomor KTP <span class="required">*</span> </label>
                <div class="col-md-6 col-sm-6 ">
                  <input id="no_ktp" class="form-control col" type="text" name="no_ktp" value="{{ $personal ['no_ktp']}}">
                </div>
              </div>

              <div class="form-group row">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Gender <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 "><br>
                  @if($gender == "L")
                  <p>
                    Pria:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" checked="" required />
                    &emsp;&emsp;Wanita:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P" />
                  </p>
                  @elseif($gender == "P")
                  <p>
                    Pria:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" required />
                    &emsp;&emsp;Wanita:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P" checked=""  />
                  </p>
                  @else
                  <p>
                    Pria:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" checked="" required />
                    &emsp;&emsp;Wanita:
                    <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P" />
                  </p>
                  @endif

                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Agama <span class="required">*</span>
                </label>
                            <div class="col-md-6 col-sm-6 ">
                              <select name="agama" value="{{ $personal ['agama']}}" class="form-control">
                                <option value="0" disabled="true" selected="true">Choose Option</option>
                                @if($agama == "Islam")
                                <option selected>Islam</option>
                                <option>Kristen</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option>Budha</option>
                                <option>Konghucu</option>
                                @elseif($agama == "Kristen")
                                <option>Islam</option>
                                <option selected>Kristen</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option>Budha</option>
                                <option>Konghucu</option>
                                @elseif($agama == "Katolik")
                                <option>Islam</option>
                                <option>Kristen</option>
                                <option selected>Katolik</option>
                                <option>Hindu</option>
                                <option>Budha</option>
                                <option>Konghucu</option>
                                @elseif($agama == "Hindu")
                                <option>Islam</option>
                                <option>Kristen</option>
                                <option>Katolik</option>
                                <option selected>Hindu</option>
                                <option>Budha</option>
                                <option>Konghucu</option>
                                @elseif($agama == "Budha")
                                <option>Islam</option>
                                <option>Kristen</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option selected>Budha</option>
                                <option>Konghucu</option>
                                @elseif($agama == "Konghucu")
                                <option>Islam</option>
                                <option>Kristen</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option>Budha</option>
                                <option selected>Konghucu</option>
                                @else
                                <option>Islam</option>
                                <option>Kristen</option>
                                <option>Katolik</option>
                                <option>Hindu</option>
                                <option>Budha</option>
                                <option>Konghucu</option>
                                @endif
                              </select>
                            </div>
                          </div>

              <div class="form-group row">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Status Perkawinan <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 "><br>
                  @if($status == "belum_menikah")
                  <p>
                    Belum Menikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="belum_menikah" value="belum_menikah" checked="" required />
                    &emsp;&emsp;Nikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="nikah" value="nikah" />
                    &emsp;&emsp;Janda/Duda:
                    <input type="radio" class="flat" name="status_perkawinan" id="janda/duda" value="janda" />
                  </p>
                  @elseif($status == "nikah")
                  <p>
                    Belum Menikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="belum_menikah" value="belum_menikah"required />
                    &emsp;&emsp;Nikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="nikah" value="nikah" checked="" />
                    &emsp;&emsp;Janda/Duda:
                    <input type="radio" class="flat" name="status_perkawinan" id="janda/duda" value="janda" />
                  </p>
                  @elseif($status == "janda")
                  <p>
                    Belum Menikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="belum_menikah" value="belum_menikah"  required />
                    &emsp;&emsp;Nikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="nikah" value="nikah" />
                    &emsp;&emsp;Janda/Duda:
                    <input type="radio" class="flat" name="status_perkawinan" id="janda/duda" checked="" value="janda" />
                  </p>
                  @else
                  <p>
                    Belum Menikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="belum_menikah" value="belum_menikah" checked=""  required />
                    &emsp;&emsp;Nikah:
                    <input type="radio" class="flat" name="status_perkawinan" id="nikah" value="nikah" />
                    &emsp;&emsp;Janda/Duda:
                    <input type="radio" class="flat" name="status_perkawinan" id="janda/duda"  value="janda" />
                  </p>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Saya Anak Ke <span class="required">*</span></label>
                  <div class="col-md-2 col-sm-2  form-group has-feedback">
                      <input type="number" class="form-control has-feedback-left" id="anak_ke" name="anak_ke" value="{{ $personal ['anak_ke']}}" placeholder="Nomer">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <label for="middle-name" class="col-form-label label-align">Dari<span class="required"></span></label>
                  <div class="col-md-2 col-sm-2  form-group has-feedback">
                      <input type="number" class="form-control has-feedback-left" id="saudara" name="saudara" placeholder="Saudara" value="{{ $personal ['saudara']}}">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                  </div>
                  <label for="middle-name" class="col-form-label label-align">Bersaudara<span class="required"></span></label>
                </label>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Kebangsaan <span class="required">*</span>
                </label>
                            <div class="col-md-6 col-sm-6 ">
                              <input type="hidden" id="tanggal_join" name="tanggal_join" value="{{ date('d-m-Y') }}" class="form-control" value="{{ $personal ['tanggal_join']}}">
                              <select name="kebangsaan" class="form-control">
                                <option value="0" disabled="true" selected="true">Choose Option</option>
                                @foreach ($countries_array as $data)
                                      <option value="{{ $data->country_name }}" {{ $countries == $data->country_name ? 'selected="selected"' : '' }} >{{ $data->country_name}}</option>
                                @endforeach
                              </select>
                            </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NO. NPWP
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="npwp" name="npwp" class="form-control" value="{{ $personal ['npwp']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat Domisili <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="alamat_domisili" name="alamat_domisili" required="required" class="form-control" value="{{ $personal ['alamat_domisili']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kota Domisili <span class="required">*</span>
                </label>
                <div class="col-md-3 col-sm-3 ">
                  <input type="text" id="kota_domisili" name="kota_domisili" required="required" class="form-control" value="{{ $personal ['kota_domisili']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Alamat KTP <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="alamat_ktp" name="alamat_ktp" required="required" class="form-control" value="{{ $personal ['alamat_ktp']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Kota KTP <span class="required">*</span>
                </label>
                <div class="col-md-3 col-sm-3 ">
                  <input type="text" id="kota_ktp" name="kota_ktp" required="required" class="form-control" value="{{ $personal ['kota_ktp']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">NO Telepon <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 ">
                  <input type="text" id="telepon" name="telepon" required="required" class="form-control" value="{{ $personal ['telepon']}}">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Jenis Kendaraan <span class="required">*</span>
                </label>
                            <div class="col-md-6 col-sm-6 ">
                              <select name="kendaraan" class="form-control" value="{{ $personal ['kendaraan']}}">
                              <option value="0" disabled="true" selected="true">Choose Option</option>
                            @if($kendaraan == "Tidak Punya")
                                <option selected>Tidak Punya</option>
                                <option>Roda 2</option>
                                <option>Roda 4</option>
                            @elseif($kendaraan == "Roda 2")
                                <option>Tidak Punya</option>
                                <option selected>Roda 2</option>
                                <option>Roda 4</option>
                            @elseif($kendaraan == "Roda 2")
                                <option>Tidak Punya</option>
                                <option>Roda 2</option>
                                <option selected>Roda 4</option>
                            @else
                                <option selected>Tidak Punya</option>
                                <option>Roda 2</option>
                                <option>Roda 4</option>
                            @endif
                              </select>
                            </div>
                          </div>
                <div class="form-group row">
                    <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Memiliki Laptop Sendiri ? <span class="required">*</span></label>
                      <div class="col-md-6 col-sm-6 "><br>
                        @if($laptop == "ya")
                          <p>
                                Ya:
                                <input type="radio" class="flat" name="laptop"  id="laptop_ya" value="ya" checked="" required />
                                &emsp;&emsp;Tidak:
                                <input type="radio" class="flat" name="laptop"  id="laptop_tidak" value="tidak" />
                              </p>
                        @elseif($laptop == "tidak")
                        <p>
                              Ya:
                              <input type="radio" class="flat" name="laptop"  id="laptop_ya" value="ya"  required />
                              &emsp;&emsp;Tidak:
                              <input type="radio" class="flat" name="laptop"  id="laptop_tidak" value="tidak" checked="" />
                            </p>
                        @else
                        <p>
                              Ya:
                              <input type="radio" class="flat" name="laptop"  id="laptop_ya" value="ya" checked="" required />
                              &emsp;&emsp;Tidak:
                              <input type="radio" class="flat" name="laptop"  id="laptop_tidak" value="tidak" />
                            </p>
                        @endif
                            </div>
                          </div>


                <div class="form-group row">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Gaji Yang Hiharapkan <span class="required">*</span>
                      </label>
                        <div class="col-md-6 col-sm-6 ">
                        <input type="text" id="gaji_diharap" name="gaji_diharap" required="required" class="form-control" value="{{ $personal ['gaji_diharap']}}">
                      </div>
                </div>

                <div class="form-group row">
                  <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Foto Diri <span class="required">*</span>
                  </label>
                  <div class="col-md-3 col-sm-3 ">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="image" name="image" onchange="return previewImage(event)">

                      <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-3">
                      <img class="img-circle user-profile" id="output"  height="200" width="200" border=0>
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pencapaian Sudah Dilakukan Selama Bekerja <span class="required">*</span>
                      </label>
                        <div class="col-md-6 col-sm-6 ">
                        <textarea class="form-control" rows="5" id="pencapaian_sudah_dilakukan" name="pencapaian_sudah_dilakukan">{{ $personal ['pencapaian_sudah_dilakukan']}}</textarea>
                      </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pencapaian Yang Ingin Diraih 1-5 Tahun Kedepan <span class="required">*</span>
                      </label>
                        <div class="col-md-6 col-sm-6 ">
                        <textarea class="form-control" rows="5" id="pencapaian_yang_diinginkan" name="pencapaian_yang_diinginkan">{{ $personal ['pencapaian_yang_diinginkan']}}</textarea>
                      </div>
                </div>


                <div class="ln_solid"></div>
                <div class="item form-group">
                  <div class="col-md-8 col-sm-8 offset-md-3">
                    <a href="/personal/family" class="btn btn-info pull-right">Isi Data Keluarga</a>
                    <button type="submit" class="btn btn-success pull-right" >Submit</button>
                  </div>
                </div>

            </form>

          </div>
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
function previewImage(event){
  var output = document.getElementById('output');
  output.src = URL.createObjectURL(event.target.files[0]);
};

</script>
@endsection
