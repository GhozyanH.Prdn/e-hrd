@extends('layouts.postulant')

@section('content')
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Form  <small>Data Keluarga</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Settings 1</a>
            </li>
            <li><a href="#">Settings 2</a>
            </li>
          </ul>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
      @if(session()->has('Alert'))
          <script>
              alert({{ session()->get('Alert') }});
          </script>
      @endif
    </div>
    <div class="x_content">

      <div>
        <form action="{{route('personal.family.add')}}" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
          @csrf
          <input type="hidden" name="id_postulant" value="{{ Auth::user()->id }}">
          <div class="form-group row">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nama Lengkap <span class="required">*</span> </label>
            <div class="col-md-6 col-sm-6 ">
              <input id="middle-name" class="form-control col" type="text" name="nama">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Hubungan <span class="required">*</span>
            </label>
                        <div class="col-md-6 col-sm-6 ">
                          <select name="hubungan" class="form-control">
                            <option value="0" disabled="true" selected="true">Choose Option</option>
                            <option>Bapak</option>
                            <option>Ibu</option>
                            <option>Kakak</option>
                            <option>Adek</option>
                          </select>
                        </div>
                      </div>

          <div class="form-group row">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Gender <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 "><br>
              <p>
                Pria:
                <input type="radio" class="flat" name="jenis_kelamin" id="genderM" value="L" checked="" required />
                &emsp;&emsp;Wanita:
                <input type="radio" class="flat" name="jenis_kelamin" id="genderF" value="P" />
              </p>
            </div>
          </div>


          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Usia <span class="required">*</span>
            </label>
            <div class="col-md-3 col-sm-3 ">
              <input type="text" id="usia" name="usia" required="required" class="form-control  ">
            </div>
          </div>


          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pendidikan <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" id="pendidikan" name="pendidikan" required="required" class="form-control  ">
            </div>
          </div>

          <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align" for="first-name">Pekerjaan <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text" id="pekerjaan" name="pekerjaan" required="required" class="form-control  ">
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="item form-group">
            <div class="col-md-8 col-sm-8 offset-md-3">
              <a href="/personal/education" class="btn btn-info pull-right">Isi Data Pendidikan</a>
              <button type="submit" class="btn btn-success pull-right" >Submit</button>
            </div>
          </div>

        </form>
      </div>
    </div>
    </div>
  </div>
  <div class="x_panel">
  <div class="x_title">
    <h2> <small> Data Keluarga </small></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">Settings 1</a>
            <a class="dropdown-item" href="#">Settings 2</a>
          </div>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
    <div class="clearfix"></div>
    @if (session('delete'))
    <div class="alert alert-success alert-danger " role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>Success!</strong> Data Family Berhasil di Hapus !
    </div>
    @endif
  </div>

  <div class="x_content">
  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th class="column-title">No </th>
          <th class="column-title">Hubungan </th>
          <th class="column-title">Nama </th>
          <th class="column-title">Jenis Kelamin </th>
          <th class="column-title">Usia </th>
          <th class="column-title">Pendidikan </th>
          <th class="column-title">Pekerjaan </th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
        </tr>
      </thead>

      <tbody>
        @foreach ($tampil as $v => $ast )
        <tr class="even pointer">

          <td class=" ">{{$loop -> iteration}}</td>
          <td class=" ">{{$ast -> hubungan}}</td>
          <td class=" ">{{$ast -> nama}}</td>
          <td class=" ">{{$ast -> jenis_kelamin}}</td>
          <td class=" ">{{$ast -> usia}}</td>
          <td class=" ">{{$ast -> pendidikan}}</td>
          <td class=" ">{{$ast -> pekerjaan}}</td>
          <td class=" last">
            <form action="/personal/family/{{$ast -> id}}" method="post" class="d-inline">
            @method('delete')
            @csrf
            <button type="submit" class="btn btn-custon-four btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o" style="font-size:15px"></i></button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>

</div>


@endsection
