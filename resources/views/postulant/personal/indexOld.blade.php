@extends('layouts.postulant')

@section('content')

<div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>PERSONALITY SCORE <small>Sessions</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="panel">
                      <div id="container"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            @php
            $s = $tole;
            $s2=$tes3;
            //["Randa,Miko","Miko,Robi","Miko,Dika","Miko,Kelvin","Miko,Roby","Dika,Ghozyan","Dika,Randas","Kelvin,Karina","Kelvin,Elita","Ghozyan,Rizki","Ghozyan,Tevi"]

            $ss = str_replace('",','"],',$s);
            $sss = str_replace(',"',',[",',$ss);
            $ssss = str_replace('",','',$sss);
            $sssss = str_replace('["','[',$ssss);
            $ssssss = str_replace('[','["',$sssss);
            $sssssss = str_replace(',','","',$ssssss);
            $ssssssss = str_replace(']"',']',$sssssss);
            $sssssssss = str_replace('"[','[',$ssssssss);

            $k = count($tes);
            for($i=0;$i<$k;$i++){
              $rs[] = $tes[$i];
            }

            $rs1 = implode(",",$rs);
            $r = str_replace('"','',$rs1);
            $rr = str_replace(':',":'",$r);
            $rrr = str_replace(',',"',",$rr);
            $rrrr = str_replace('}',"'}",$rrr);
            $rrrrr = str_replace("}'","}",$rrrr);
            @endphp






@endsection

@section('org')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/sankey.js"></script>
<script src="https://code.highcharts.com/modules/organization.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<script type="text/javascript">
        Highcharts.chart('container', {

            chart: {
                height: 600,
                inverted: true
            },

            title: {
                text: 'Struktur Organisasi <br> PT Davinti Indonesia'
            },

            series: [{
                type: 'organization',
                name: 'PT Davinti Indonesia',
                keys: ['from', 'to'],
                data: [
                  @php
                    print_r($sssssssss);
                  @endphp
                ],
                levels: [{
                    level: 0,
                    color: '##28B463',
                    dataLabels: {
                        color: '#ECF0F1'
                    },
                    height: 25
                }, {
                    level: 1,
                    color: 'silver',
                    dataLabels: {
                        color: 'black'
                    },
                    height: 25
                }, {
                    level: 2,
                    color: '#980104'
                }, {
                    level: 4,
                    color: '#359154'
                },
                {
                    level: 5,
                    color: '#BA4A00'
                },
                {
                    level: 6,
                    color: '#D4AC0D'
                },
                {
                    level: 7,
                    color: '#73C6B6'
                },
                {
                    level: 8,
                    color: '#8E44AD'
                },
                {
                    level: 9,
                    color: '#E74C3C'
                }
              ],
                nodes: [
                  @php
                    echo $str = str_replace(array("\r","\n"),"",$rrrrr);
                  @endphp
                ],
                colorByPoint: false,
                color: '#007ad0',
                dataLabels: {
                    color: 'white'
                },
                borderColor: 'white',
                nodeWidth: 65
            }],
            tooltip: {
                outside: true
            },
            exporting: {
                allowHTML: true,
                sourceWidth: 800,
                sourceHeight: 600
            }

        });
</script>
@endsection
