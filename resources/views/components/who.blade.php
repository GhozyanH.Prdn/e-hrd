@if(Auth::guard('web')->check())
<p class="text-success">
you are Login as <strong>User</strong>
</p>
@else
<p class="text-danger">
you are logout as <strong>User</strong> </p>
@endif

@if(Auth::guard('admin')->check())
    <p class="text-success">
you are Login as <strong>Admin</strong>
</p>
@else
<p class="text-danger">
you are logout as <strong>Admin</strong> </p>
@endif
