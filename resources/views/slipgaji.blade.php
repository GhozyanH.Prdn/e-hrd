<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slip Gaji</title>
</head>
<body>

<table border="0" style="width:100%">
    <thead>
        <tr>
            <th colspan="4"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2"><img src="C:/xampp/htdocs/e-hrd/public/images/davinti.jpg" width="150"></td>
            <td colspan="2" align=right><h3>Slip Gaji Bulan {{$b}} {{$t}} - {{$n}}</h3></td>
        </tr>
    </tbody>

    <thead>
        <tr>
            <th colspan="4"><br>Data Absensi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2" align=right>Jumlah Kehadiran  : </td>
            <td>{{$k}} Hari</td>
        </tr>
        <tr>
            <td colspan="2" align=right>Jumlah Cuti Tak Dibayar  : </td>
            <td>{{$jctd}} Hari</td>
        </tr>
    </tbody>
    
    <thead>
        <tr>
            <th colspan="2"><br> Pendapatan</th>
            <th colspan="2"><br> Pengurangan</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Gaji Pokok</td>
            <td style="border-right: 1px solid #cdd0d4;">Rp. {{$gp}},-</td>
            <td>Cuti Tak Dibayar</td>
            <td>Rp. {{$ctd}},-</td>
        </tr>
        <tr>
            <td>Tunjangan Harian</td>
            <td style="border-right: 1px solid #cdd0d4;">Rp. {{$th}},-</td>
            <td>Potongan Lainnya</td>
            <td>Rp. {{$jp}},-</td>
        </tr>
        <tr>
            <td>Tunjangan Lainnya</td>
            <td style="border-right: 1px solid #cdd0d4;">Rp. {{$tl}},-</td>
            <td>PPh21</td>
            <td>Rp. {{$pph}},-</td>
        </tr>
        <tr>
            <td>Bonus Tak Tetap</td>
            <td style="border-right: 1px solid #cdd0d4;">Rp. {{$btt}},-</td>
        </tr>
        <tr>
            <td align=right><b>Total Penghasilan Bruto  : </b></td>
            <td style="border-right: 1px solid #cdd0d4;"><b>Rp. {{$tpb}},-</b></td>
            <td align=right><b>Total Pengurangan  : </b></td>
            <td><b>Rp. {{$tp}},-</b></td>
        </tr>
        <tr>
            <td colspan="2" align=right><br><b>Total Gaji Diterima  : </b></td>
            <td colspan="2"><br><b>Rp. {{$upg}},-</b></td>
        </tr>
    </tbody>
</table>
</body>
</html>