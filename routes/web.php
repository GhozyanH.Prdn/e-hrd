<?php
use RealRashid\SweetAlert\Facades\Alert;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('/karyawan/import_excel', 'ExcelController@import_excel');

Route::get('/', 'HomeController@depan')->name('depan');
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});
Route::group(['prefix' => 'dashboard','middleware' => ['auth','role:Admin,Pegawai,Atasan,Direktur']], function(){
  Route::get('/', 'HomeController@index')->name('home');
  });

Auth::routes();
//ROUTE ADMIN
Route::group(['prefix' => 'admin','middleware' => ['auth','role:Admin,Direktur']], function(){
    Route::get('/user/logout', 'Auth\LoginController@logoutUser')->name('user.logout');
    //ASET
    Route::get('/assets/trash','Admin\AssetsController@trash');
    Route::get('/assets/restore/{id}','Admin\AssetsController@restore');
    Route::get('/assets/deleted_permanent/{id}','Admin\AssetsController@deleted_permanent');
    Route::get('/assets/restore_all','Admin\AssetsController@restore_all');
    Route::get('/assets/deleted_all','Admin\AssetsController@deleted_all');
    Route::get('/assets/{asset}/transaksi','Admin\AssetsController@transaksi');
    Route::patch('/transaksi/{asset}','Admin\AssetsController@transaksi');
    Route::post('/transaksi','Admin\AssetsController@input_transaksi');
    Route::patch('/pengembalian/{asset}','Admin\AssetsController@pengembalian');
    Route::Resource('assets', 'Admin\AssetsController');
    Route::Resource('employees', 'EmployeesController');
    Route::delete('/resign/{employee}','Admin\EmployeesController@destroy');

    //Salary
    Route::get('/salary/deleted_permanent/{salary}','Admin\SalaryController@destroy');
    Route::get('/salary/show','Admin\SalaryController@tabel');
    Route::get('/salary/detil','Admin\SalaryController@detil');
    Route::post('/salary/detil','Admin\SalaryController@detil');
    Route::Resource('salary', 'Admin\SalaryController');
    Route::Resource('gajiadmin','Admin\GajiAdminController');
    Route::get('/slipgajiadmin/{id}','Admin\GajiAdminController@slip');


    //INTERVIEW
    Route::get('/interview/trash','Admin\InterviewController@trash');
    Route::get('/interview/restore/{interview}','Admin\InterviewController@restore');
    Route::get('/interview/deleted_permanent/{interview}','Admin\InterviewController@deleted_permanent');
    Route::get('/interview/restore_all','Admin\InterviewController@restore_all');
    Route::get('/interview/deleted_all','Admin\InterviewController@deleted_all');
    Route::delete('/interview/{interview}','Admin\InterviewController@destroy');
    Route::Resource('interview','Admin\InterviewController');

    //KARYAWAN
    Route::Resource('employees', 'Admin\EmployeesController');
    Route::get('/resign','Admin\EmployeesController@resign');
    Route::get('/resign/{employee}/edit','Admin\EmployeesController@resignedit');
    Route::patch('/resign/{employee}','Admin\EmployeesController@resignupdate');
    Route::patch('/employees2/{employee}','Admin\EmployeesController@karyawanupdate');



    //CATEGORI
    Route::get('/category/trash','Admin\CategoryController@trash');
    Route::get('/category/restore/{category}','Admin\CategoryController@restore');
    Route::get('/category/deleted_permanent/{category}','Admin\CategoryController@deleted_permanent');
    Route::get('/category/restore_all','Admin\CategoryController@restore_all');
    Route::get('/category/deleted_all','Admin\CategoryController@deleted_all');
    Route::delete('/category/{category}','Admin\CategoryController@destroy');
    Route::Resource('category','Admin\CategoryController');

    //DATA PENGAJUAN CUTI
    Route::get('/cuti/trash','Admin\CutiController@trash');
    Route::get('/cuti/restore/{id}','Admin\CutiController@restore');
    Route::get('/cuti/deleted_permanent/{id}','Admin\CutiController@deleted_permanent');
    Route::get('/cuti/restore_all','Admin\CutiController@restore_all');
    Route::get('/cuti/deleted_all','Admin\CutiController@deleted_all');
    Route::get('/cuti/{cuti}/detil','Admin\CutiController@detil');
    Route::get('/cuti/{cuti}/editreq','Admin\CutiController@editreq');
    Route::Resource('cuti','Admin\CutiController');
    Route::get('rekapcuti','Admin\CutiController@indexrekap');
    Route::post('/rekapcuti','Admin\CutiController@indexrekap');

    Route::get('admin_atasan','Admin\CutiController@persetujuanatasan1');
    Route::get('/admin_atasan/{cuti}/detil','Admin\CutiController@detilatasan1');
    Route::get('/admin_atasan/{cuti}/edit','Admin\CutiController@editatasan1');
    Route::patch('/admin_atasan/{cuti}', 'Admin\CutiController@updateatasan1');

    //KELOLA Jabatan
    Route::get('/jabatan/trash','Admin\JabatanController@trash');
    Route::get('/jabatan/restore/{position}','Admin\JabatanController@restore');
    Route::get('/jabatan/deleted_permanent/{position}','Admin\JabatanController@deleted_permanent');
    Route::get('/jabatan/restore_all','Admin\JabatanController@restore_all');
    Route::get('/jabatan/deleted_all','Admin\JabatanController@deleted_all');
    Route::delete('/jabatan/{position}','Admin\JabatanController@destroy');
    Route::Resource('jabatan','Admin\JabatanController');

    //divisi
    Route::get('/divisi/trash','Admin\DivisionController@trash')->name('divisi.trash');
    Route::get('/divisi/restore/{division}','Admin\DivisionController@restore');
    Route::get('/divisi/deleted_permanent/{division}','Admin\DivisionController@deleted_permanent');
    Route::get('/divisi/restore_all','Admin\DivisionController@restore_all');
    Route::get('/divisi/deleted_all','Admin\DivisionController@deleted_all');
    Route::delete('/divisi/{division}','Admin\DivisionController@destroy');
    Route::Resource('divisi','Admin\DivisionController');

      //corporate_group
    Route::get('/corporate/trash','Admin\CorporateGroupController@trash');
    Route::get('/corporate/restore/{corporate}','Admin\CorporateGroupController@restore');
    Route::get('/corporate/deleted_permanent/{corporate}','Admin\CorporateGroupController@deleted_permanent');
    Route::get('/corporate/restore_all','Admin\CorporateGroupController@restore_all');
    Route::get('/corporate/deleted_all','Admin\CorporateGroupController@deleted_all');
    Route::delete('/corporate/{corporate}','Admin\CorporateGroupController@destroy');
    Route::Resource('corporate','Admin\CorporateGroupController');

      //grade
    Route::get('/grade/trash','Admin\GradeController@trash')->name('grade.trash');
    Route::get('/grade/restore/{grade}','Admin\GradeController@restore');
    Route::get('/grade/deleted_permanent/{grade}','Admin\GradeController@deleted_permanent');
    Route::get('/grade/restore_all','Admin\GradeController@restore_all');
    Route::get('/grade/deleted_all','Admin\GradeController@deleted_all');
    Route::delete('/grade/{grade}','Admin\GradeController@destroy');
    Route::Resource('grade','Admin\GradeController');
});



//ATASAN
Route::group(['prefix' => 'atasan','middleware' => ['auth','role:Atasan']], function(){
  Route::Resource('cuti_atasan','Atasan\CutiAtasanController');
  Route::get('/cuti_atasan/{cuti}/detil','Atasan\CutiAtasanController@detil');

  Route::get('persetujuan_atasan','Admin\CutiController@persetujuanatasan');
  Route::get('/persetujuan_atasan/{cuti}/detil','Admin\CutiController@detilatasan');
  Route::get('/persetujuan_atasan/{cuti}/edit','Admin\CutiController@editatasan');
  Route::patch('/persetujuan_atasan/{cuti}', 'Admin\CutiController@updateatasan');

  Route::Resource('gajiatasan','Atasan\GajiAtasanController');
  Route::get('/slipgajiat/{id}','Atasan\GajiAtasanController@slip');
  });

  //Direktur
  Route::group(['prefix' => 'direktur','middleware' => ['auth','role:Direktur']], function(){

    Route::get('persetujuan_direktur','Admin\CutiController@persetujuandirektur');
    Route::get('/persetujuan_direktur/{cuti}/detil','Admin\CutiController@detildirektur');
    Route::get('/persetujuan_direktur/{cuti}/edit','Admin\CutiController@editdirektur');
    Route::patch('/persetujuan_direktur/{cuti}', 'Admin\CutiController@updatedirektur');



    //CATEGORI
    Route::get('/categorydir/trash','Direktur\CategorydirController@trash');
    Route::get('/categorydir/restore/{category}','Direktur\CategorydirController@restore');
    Route::get('/categorydir/deleted_permanent/{category}','Direktur\CategorydirController@deleted_permanent');
    Route::get('/categorydir/restore_all','Direktur\CategorydirController@restore_all');
    Route::get('/categorydir/deleted_all','Direktur\CategorydirController@deleted_all');
    Route::delete('/categorydir/{category}','Direktur\CategorydirController@destroy');
    Route::Resource('categorydir','Direktur\CategorydirController');

  });


  //PEGAWAI
  Route::group(['prefix' => 'pegawai','middleware' => ['auth','role:Pegawai']], function(){
    Route::Resource('gajipegawai','Pegawai\GajiPegawaiController');
    Route::get('/slipgajipeg/{id}','Pegawai\GajiPegawaiController@slip');

    Route::Resource('cutipegawai','Pegawai\CutiPegawaiController');
    Route::get('/cutipegawai/{cuti}/detil','Pegawai\CutiPegawaiController@detil')->name('pegawai.detil');
    });

//ROUTE PERSONAL
Route::group(['prefix' => 'personal'], function(){
  Route::get('/login', 'AuthPersonal\LoginController@showLoginForm')->name('personal.login');
  Route::post('/login', 'AuthPersonal\LoginController@login')->name('personal.login.submit');
  Route::get('/', 'Personal\PersonalController@index')->name('personal.home');
  Route::get('/tes', 'Personal\PersonalControllerOld@index');
  Route::get('/logout', 'AuthPersonal\LoginController@logout')->name('personal.logout');
  Route::get('/register', 'AuthPersonal\RegisterController@create')->name('personal.register');
  Route::post('/','AuthPersonal\RegisterController@store')->name('register.personal');
  Route::get('/password/reset','AuthPersonal\ForgotPasswordController@showLinkRequestForm')->name('personal.password.request');
  Route::post('/password/email','AuthPersonal\ForgotPasswordController@sendResetLinkEmail')->name('personal.password.email');
  Route::get('/password/reset/{token}','AuthPersonal\ResetPasswordController@showResetForm')->name('personal.password.reset');
  Route::post('/password/reset','AuthPersonal\ResetPasswordController@reset');
});
  //DATA
Route::group(['prefix' => 'personal','middleware' => ['auth:personal']], function(){
  Route::get('/data','AuthPersonal\RegisterController@edit')->name('personal.data');
  Route::patch('/data','AuthPersonal\RegisterController@update')->name('personal.data.update');

  //FAMILY
  Route::get('/family', 'Personal\FamilyController@create')->name('personal.family');
  Route::post('/family', 'Personal\FamilyController@store')->name('personal.family.add');
  Route::delete('/family/{family}','Personal\FamilyController@destroy')->name('personal.family.delete');

  //EDUCATION
  Route::get('/education', 'Personal\EducationController@create')->name('personal.education');
  Route::post('/education', 'Personal\EducationController@store')->name('personal.education.add');
  Route::delete('/education/{education}','Personal\EducationController@destroy')->name('personal.education.delete');

  //OLD-JOB
  Route::get('/old_job', 'Personal\Old_jobController@create')->name('personal.old_job');
  Route::post('/old_job', 'Personal\Old_jobController@store')->name('personal.old_job.add');
  Route::delete('/old_job/{job_history}','Personal\Old_jobController@destroy')->name('personal.old_job.delete');

  //TRAINING
  Route::get('/training', 'Personal\TrainingController@create')->name('personal.training');
  Route::post('/training', 'Personal\TrainingController@store')->name('personal.training.add');
  Route::delete('/training/{training}','Personal\TrainingController@destroy')->name('personal.training.delete');

  //ORGANIZATION
  Route::get('/organization', 'Personal\OrganizationController@create')->name('personal.organization');
  Route::post('/organization', 'Personal\OrganizationController@store')->name('personal.organization.add');
  Route::delete('/organization/{organization}','Personal\OrganizationController@destroy')->name('personal.organization.delete');

  //AWARD
  Route::get('/award', 'Personal\AwardController@create')->name('personal.award');
  Route::post('/award', 'Personal\AwardController@store')->name('personal.award.add');
  Route::delete('/award/{award}','Personal\AwardController@destroy')->name('personal.award.delete');

  //LANGUAGE
  Route::get('/language', 'Personal\LanguageController@create')->name('personal.language');
  Route::post('/language', 'Personal\LanguageController@store')->name('personal.language.add');
  Route::delete('/language/{language}','Personal\LanguageController@destroy')->name('personal.language.delete');

  //REFERENCE
  Route::get('/reference', 'Personal\ReferenceController@create')->name('personal.reference');
  Route::post('/reference', 'Personal\ReferenceController@store')->name('personal.reference.add');
  Route::delete('/reference/{reference}','Personal\ReferenceController@destroy')->name('personal.reference.delete');

  //EMERGENCYCONTACT
  Route::get('/emergencycontact', 'Personal\EmergencycontactController@create')->name('personal.emergencycontact');
  Route::post('/emergencycontact/fetch', 'Personal\EmergencycontactController@fetch')->name('personal.emergencycontact.fetch');
  Route::post('/emergencycontact', 'Personal\EmergencycontactController@store')->name('personal.emergencycontact.add');
  Route::delete('/emergencycontact/{emergencycontact}','Personal\EmergencycontactController@destroy')->name('personal.emergencycontact.delete');

  //PERSONALITY TEST
  Route::get('/personality_test', 'Personal\PersonalityTestController@create')->name('personal.test');
  Route::post('/personality_test', 'Personal\PersonalityTestController@store')->name('personal.test.add');
  //Route::Resource('personality_tests','Personal\PersonalityTestController');

  //PERSONALITY SCORE
  Route::get('/personality_score', 'Personal\ScoreController@index')->name('personal.score');
});
