<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Corporate extends Model
{
    use SoftDeletes;
    protected $table = "corporate_groups";
    protected $fillable = ['kode_corporate_group', 'nama_corporate_group'];
}
