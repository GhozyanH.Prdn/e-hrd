<?php

namespace App;
use DB;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    public function salary(){
        return $this ->hasOne('App\Salary','id_pegawai','id') ;
     }

    public function cuti(){
        return $this ->hasOne('App\Cuti','id_karyawan','id') ;
      }

    public function postulant(){
        return $this ->belongsTo('App\Postulant','id_postulant');

        }
    public function grade(){
        return $this ->belongsTo('App\Grade','id_grade');

        }
    public function position(){
        return $this ->belongsTo('App\Position','id_jabatan');

        }
    public function corporate(){
        return $this ->belongsTo('App\Corporate','id_corporate');

        }
    public function division(){
        return $this ->belongsTo('App\Division','id_devisi');

        }
    public static function getId(){
        return $getId = DB::table('cuti_categorys')->orderBy('id','DESC')->take(1)->get();
      }





    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $table = "employees";
     protected $primaryKey = "id";

        protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
