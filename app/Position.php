<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;


class Position extends Model
{
    use SoftDeletes;
    protected $table = "positions";
    protected $fillable = ['kode_jabatan', 'nama_jabatan'];
}
