<?php

namespace App\Http\Controllers\Atasan;
use App\Http\Controllers\Controller;
use App\Cuti;
use Illuminate\Http\Request;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuti3 = Cuti::all()
        -> where('keputusan_hrd','=','menunggu');

        $cuti1 = Cuti::all()
        -> where('keputusan_hrd','!=','menunggu');
        return view('admin.cuti.index', compact('cuti3','cuti1'));
    }

    public function persetujuanatasan()
    {
        $cuti3 = Cuti::all()
        -> where('nama_atasan',auth()->User()->nama)
        -> where('keputusan_atasan','=','menunggu');
        $cuti1 = Cuti::all()
        ->where('nama_atasan',auth()->User()->nama)
        -> where('keputusan_atasan','!=','menunggu')->Paginate(15);
        return view('atasan.persetujuan.index', compact('cuti3','cuti1'));
    }
    public function persetujuandirektur()
    {
        $cuti3 = Cuti::all()
        -> where('keputusan_direktur','=','menunggu');

        $cuti1 = Cuti:: where('keputusan_direktur','!=','menunggu')->Paginate(15);
        return view('direktur.persetujuan.index', compact('cuti3','cuti1'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function show(Cuti $cuti)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
        return view('admin.cuti.edit', compact('cuti'));

    }

    public function editatasan(Cuti $cuti)
    {

        return view('atasan.persetujuan.edit', compact('cuti'));
    }


    public function editdirektur(Cuti $cuti)
    {

        return view('direktur.persetujuan.edit', compact('cuti'));
    }
     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */

    public function detil(Cuti $cuti)
    {

        return view('admin.cuti.detil', compact('cuti'));

    }


    public function detilatasan(Cuti $cuti)
    {

        return view('atasan.persetujuan.detil', compact('cuti'));

    }


    public function detildirektur(Cuti $cuti)
    {

        return view('direktur.persetujuan.detil', compact('cuti'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_hrd' => $request->keputusan_anda,
          'catatan_hrd' => $request->catatan_anda,
        ]);

        return redirect('/admin/cuti')-> with('edit', 'Data Aset Berhasil di Ubah');
    }

    public function updateatasan(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_atasan' => $request->keputusan_anda,
          'catatan_atasan' => $request->catatan_anda,
        ]);

        return redirect('/atasan/persetujuan_atasan')-> with('edit', 'Data Aset Berhasil di Ubah');
    }


    public function updatedirektur(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_direktur' => $request->keputusan_anda,
          'catatan_direktur' => $request->catatan_anda,
        ]);

        return redirect('/direktur/persetujuan_direktur')-> with('edit', 'Data Aset Berhasil di Ubah');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        //
    }
}
