<?php

namespace App\Http\Controllers\Atasan;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Cuti;
use App\Employee;
use App\Category;
use Illuminate\Http\Request;

class CutiAtasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuti4 = Cuti::where('id_karyawan',Auth::User()->id)->Paginate(15);

        // $sisa = Cuti::join('cuti_categorys', 'cuti_requests.id_cuti', '=', 'cuti_categorys.id')
        //   ->where('id_karyawan', Auth::user()->id)
        //   ->whereYear('tanggal_cuti', date('Y'))
        //   ->where('keputusan_hrd','=' ,'diterima')
        //   ->where('keputusan_atasan','=' ,'diterima')
        //   ->where('status','=' ,'True')
        //   ->sum('quantity');
        //     dd($sisa);
        return view('atasan.cuti.index', ['cuti4'=>$cuti4,'category'=>Category::all()]);
     
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        Cuti::create([
            'tanggal_request' =>date('Y-m-d'),
            'id_karyawan' => auth()->User()->id,
            'nama_atasan' => auth()->User()->nama_atasan,
            'id_cuti' =>  $request->jenis_cuti,
            'tanggal_cuti' => $request->tanggal_cuti,
            'quantity' => $request->quantity,
            'alasan' => $request->alasan,
            'keputusan_atasan' => 'menunggu',
            'keputusan_hrd' => 'menunggu',


          ]);
          //dd($request);
          return redirect('/atasan/cuti_atasan')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    public function detil(Cuti $cuti)
    {

        return view('atasan.cuti.detil', compact('cuti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
