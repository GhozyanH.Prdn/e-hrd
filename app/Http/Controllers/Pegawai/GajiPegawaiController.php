<?php

namespace App\Http\Controllers\Pegawai;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use PDF;
use App\Salary;
use Illuminate\Http\Request;

class GajiPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salary = Salary::where('ID_Pegawai', Auth::User()->id)->get();
        // dd($salary);
        return view('employee.gaji.index', compact('salary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detsal = Salary::where('id', $id)->get();
        $data = $detsal;
            foreach ($data as $p){
            $gp = $p->Gaji_Pensiun;
            $ctd = $p->Cuti_Tidak_Dibayar;
            $th = $p->Tunjangan_Harian;
            $jp = $p->Jumlah_Pengurangan;
            $tl = $p->Tunjangan_Lainnya_Uang_Lembur_dsb;
            $pph = $p->PPh_21_sebulan;
            $btt = $p->Bonus_Tak_Tetap;
            $upg = $p->Uang_Pembayaran_Gaji;
            $tpb = $gp + $th + $tl + $btt;
            $tp = $ctd + $jp + $pph;}
        // dd($detsal);
        return view('\employee\gaji\show', compact('detsal', 'tpb', 'tp'));
    }

    public function slip($id)
    {
        // dd($id);
        $detsal = Salary::where('id', $id)->get();
        $data = $detsal;
            foreach ($data as $p){
            $id = $p->id;
            $b = $p->Bulan;
            $t = $p->Tahun;
            $n = $p->Nama_Pegawai;
            $k = $p->Kehadiran;
            $jctd = $p->Jumlah_Cuti_Tak_Dibayar;
            $gp = $p->Gaji_Pensiun;
            $ctd = $p->Cuti_Tidak_Dibayar;
            $th = $p->Tunjangan_Harian;
            $jp = $p->Jumlah_Pengurangan;
            $tl = $p->Tunjangan_Lainnya_Uang_Lembur_dsb;
            $pph = $p->PPh_21_sebulan;
            $btt = $p->Bonus_Tak_Tetap;
            $upg = $p->Uang_Pembayaran_Gaji;
            $tpb = $gp + $th + $tl + $btt;
            $tp = $ctd + $jp + $pph;}

        $pdf = PDF::loadView('slipgaji', compact('b', 't', 'n', 'k', 'jctd', 'gp', 'ctd', 'th', 'jp', 'tl', 'pph', 'btt', 'upg', 'tpb', 'tp'));
        return $pdf->stream('slip_gaji.pdf');
        // dd($detsal);
        // return view('\admin\gaji\show', compact('detsal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function detil(Gaji $gaji)
    {

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
