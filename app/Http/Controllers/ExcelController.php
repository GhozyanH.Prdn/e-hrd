<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Salary;

use Session;
use App\Imports\KaryawanImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ExcelController extends Controller
{
	public function index()
	{
		$siswa = Salary::all();
		return view('siswa',['siswa'=>$siswa]);
	}

	

	public function import_excel(Request $request) 
	{
		$bulan="januari";
		$tahun=$request->input("tahun","");
		$file = $request->file('file');
		
		
		Excel::import(new KaryawanImport , $file);
		Salary::where('Bulan',null)->where('Tahun',null)->update(["Bulan"=>$request->bulan,"Tahun"=>$request->tahun]);

		// notifikasi dengan session
		Session::flash('sukses','Data Siswa Berhasil Diimport!');

		// alihkan halaman kembali
		return redirect('/admin/salary/create');
	}
}