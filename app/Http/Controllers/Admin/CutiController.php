<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Cuti;
use App\Employee;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuti3 = Cuti::all()
        -> where('keputusan_hrd','=','menunggu');

        $cuti1 = Cuti::all()
        -> where('keputusan_hrd','!=','menunggu');
        return view('admin.cuti.index', compact('cuti3','cuti1'));
    }
    public function indexrekap()
    {

      $month = (date('m'));
      $year = (date('Y'));

      if ($month < 9) {
        $now = '%'.$year.'-'.$month.'-%';
      }
      else {
        $now='%'.$year.'-'.$month.'-%';
      }


      $cuti = DB::table('employees')
      ->select('nama','jatah_cuti')
      ->join('cuti_requests','cuti_requests.id_karyawan','employees.id')
      ->join('cuti_categorys','cuti_categorys.id', 'cuti_requests.id_cuti')
      ->where('keputusan_hrd','=','diterima')
      ->where('keputusan_atasan','=','diterima')
      ->whereMonth('tanggal_cuti',date('m'))
      ->get();
      
      $result = DB::select('SELECT a.nama, a.jatah_cuti, count(b.quantity) as total, sum(b.quantity) as jumlah, c.status 
      FROM employees as a, cuti_requests as b
      JOIN cuti_categorys as c ON b.id_cuti = c.id 
      WHERE b.id_karyawan=a.id and b.keputusan_hrd="diterima" AND b.keputusan_atasan="diterima" AND c.status="True"
      AND b.tanggal_cuti like "'.$now.'" GROUP BY a.nama ORDER BY a.nama asc');

      $rekap = DB::select('SELECT a.nama, a.jatah_cuti, count(b.quantity) as total, sum(b.quantity) as jumlah, c.status 
      FROM employees as a, cuti_requests as b
      JOIN cuti_categorys as c ON b.id_cuti = c.id 
      WHERE b.id_karyawan=a.id and b.keputusan_hrd="diterima" AND b.keputusan_atasan="diterima" AND c.status="True"
      AND b.tanggal_cuti like "%'.$year.'%" GROUP BY a.nama ORDER BY a.nama asc');

      $kk = json_encode($now);
    //   dd($rekap);
      
      $bulan = array(
              '01' => 'Januari',
              '02' => 'Februari',
              '03' => 'Maret',
              '04' => 'April',
              '05' => 'Mei',
              '06' => 'Juni',
              '07' => 'Juli',
              '08' => 'Agustus',
              '09' => 'September',
              '10' => 'Oktober',
              '11' => 'November',
              '12' => 'Desember',
      );
      //dd($bulan);
          return view('admin.rekap_cuti.index', compact('result','bulan','rekap'));
    }

    public function persetujuanatasan()
    {
        $cuti3 = Cuti::all()
        -> where('nama_atasan',Auth::User()->nama)
        -> where('keputusan_atasan','=','menunggu');

        $cuti1 = Cuti::where('nama_atasan',Auth::User()->nama)
        -> where('keputusan_atasan','!=','menunggu')->Paginate(15);
        return view('atasan.persetujuan.index', compact('cuti3','cuti1'));
    }
    public function persetujuanatasan1()
    {
        $cuti3 = Cuti::all()
        -> where('nama_atasan',Auth::User()->nama)
        -> where('keputusan_atasan','=','menunggu');

        $cuti1 = Cuti::where('nama_atasan',Auth::User()->nama)
        -> where('keputusan_atasan','!=','menunggu')->Paginate(15);
        return view('admin.persetujuan_atasan.index', compact('cuti3','cuti1'));
    }
    public function persetujuandirektur()
    {
        $cuti3 = Cuti::all()
        -> where('nama_atasan',Auth::User()->nama)
        -> where('keputusan_atasan','=','menunggu');

        $cuti1 = Cuti:: where('nama_atasan',Auth::User()->nama)
        ->where('keputusan_atasan','!=','menunggu')->Paginate(15);
        return view('direktur.persetujuan.index', compact('cuti3','cuti1'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function show(Cuti $cuti)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuti $cuti)
    {
        // dd($cuti);
        return view('admin.cuti.edit', compact('cuti'));

    }

    public function editreq(Cuti $cuti)
    {
        // dd($cuti);
        return view('admin.cuti.editreq', compact('cuti'));

    }

    public function editatasan(Cuti $cuti)
    {

        return view('atasan.persetujuan.edit', compact('cuti'));
    }

    public function editatasan1(Cuti $cuti)
    {

        return view('admin.persetujuan_atasan.edit', compact('cuti'));
    }

    public function editdirektur(Cuti $cuti)
    {

        return view('direktur.persetujuan.edit', compact('cuti'));
    }
     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */

    public function detil(Cuti $cuti)
    {

        return view('admin.cuti.detil', compact('cuti'));

    }


    public function detilatasan(Cuti $cuti)
    {

        return view('atasan.persetujuan.detil', compact('cuti'));

    }

    public function detilatasan1(Cuti $cuti)
    {

        return view('admin.persetujuan_atasan.detil', compact('cuti'));

    }


    public function detildirektur(Cuti $cuti)
    {

        return view('direktur.persetujuan.detil', compact('cuti'));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuti $cuti)
    {
        // dd($request, $cuti);
        if($request->token == 'qazmkopcr'){
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_hrd' => $request->keputusan_anda,
          'catatan_hrd' => $request->catatan_anda,
        ]);

        return redirect('/admin/cuti')-> with('edit', 'Data Aset Berhasil di Ubah');
        }

        elseif($request->token == 'alodhcecr'){
            Cuti::where('id',$cuti->id)
        ->update([
          'tanggal_cuti' => $request->tanggal_cuti,
          'quantity' => $request->quantity,
          'alasan' => $request->alasan_cuti,
        ]);

        return redirect('/admin/cuti')-> with('edit', 'Data Aset Berhasil di Ubah');
        }

        else{
            return redirect('/admin/cuti')-> with('edit', 'Data Aset Berhasil di Ubah');
        }
    }

    public function updateatasan(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_atasan' => $request->keputusan_anda,
          'catatan_atasan' => $request->catatan_anda,
        ]);

        return redirect('/atasan/persetujuan_atasan')-> with('edit', 'Data Aset Berhasil di Ubah');
    }

    public function updateatasan1(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_atasan' => $request->keputusan_anda,
          'catatan_atasan' => $request->catatan_anda,
        ]);

        return redirect('/admin/admin_atasan')-> with('edit', 'Data Aset Berhasil di Ubah');
    }

    public function updatedirektur(Request $request, Cuti $cuti)
    {
        Cuti::where('id',$cuti->id)
        ->update([
          'keputusan_atasan' => $request->keputusan_anda,
          'catatan_atasan' => $request->catatan_anda,
        ]);

        return redirect('/direktur/persetujuan_direktur')-> with('edit', 'Data Aset Berhasil di Ubah');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cuti  $cuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuti $cuti)
    {
        //
    }
}
