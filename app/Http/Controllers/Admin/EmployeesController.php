<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Imports\KaryawanImport;
use Maatwebsite\Excel\Facades\Excel;

use App\Employee;
use App\Salaray;
use App\Postulant;
use App\Grade;
use App\Position;
use App\Division;
use App\Corporate;
use App\Test;
use App\Training;
use App\Award;
use App\Emergencycontact;
use App\Organization;
use App\Education;
use App\Reference;
use App\Language;
use App\Job_history;
use App\Family;
use App\Personality_test;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class EmployeesController extends Controller
{

  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $employees = Employee::where('status','!=','Resign')->with('position')->with('postulant')->with('corporate')->with('division')->get();

      return view('admin.karyawan.index', compact('employees'));
    }

    public function resign()
    {
      $employees = Employee::where('status','=','Resign')->get();
      //dd($employees);
      return view('admin.karyawan.resign', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $employee= Employee::where('role','=','Admin')->orwhere('role','=','Atasan')->orwhere('role','=','Direktur')->get();
        return view('admin.karyawan.create', ['postulant'=>Postulant::where ('status_lamaran','=','diterima')->get(),'grade'=>Grade::all(),'position'=>Position::all(),'division'=>Division::all(),'corporate'=>Corporate::all(),'employee'=>$employee ]);

      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     $id2 = Postulant::find($request->nama_karyawan);
     //$id_postulant = $request->nama_karyawan;
     $nama=($id2->nama_lengkap);
     //dd($nama);
     $id3 = Employee::find($request->atasan);
     //dd($id3);
     $namaatasan=($id3->nama);
     $searchString = ',';

      $id = Employee::orderBy('id','DESC')->take(1)->get();
      foreach ($id as $value);
      $idlama = $value->id;
      $idbaru = $idlama + 1;
      $kode_karyawan = 'K01A0'.$idbaru;
      $passsword = Hash::make('1234');
      $parent = $namaatasan.$searchString.$nama;
      //dd($parent);
      Employee::create([
        'id_postulant'=>$request->nama_karyawan,
        'parent_path'=> $parent,
        'nama'=>$nama,
        'kode_karyawan' => $kode_karyawan,
        'password' => $passsword,
        'role' => $request->level,
        'id_jabatan' => $request->jabatan,
        'id_grade' => $request->grade,
        'id_corporate' => $request->corporate,
        'id_devisi' => $request->devisi,
        'join_date' => $request->join_date,
        'status' => $request->status,
        'nama_atasan' => $namaatasan,
        'jatah_cuti' => $request->jatah_cuti,
        'first_contract' => $request->first,
        'start_addendum1' => $request->start,
        'end_addendum1' => $request->end,
        'start_addendum2' => $request->start,
        'end_addendum2' => $request->end,
        'end_contract' => $request->end_contract,
        'gaji_pokok' => $request->gaji,
        'tunjangan_harian' => $request->tunjangan_harian,
        'tunjangan_transport' => $request->tunjangan_transport,
        'bpjs_kes' => $request->kesehatan,
        'bpjs_ket' => $request->ketenagakerjaan,

      ]);

      Postulant::where('id',$request->nama_karyawan)
      ->update([
        'status_lamaran' => 'ditambahkan',
      ]);
      return redirect('admin/employees')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
      $family = Family::where('familys.id_postulant', $employee->postulant->id) ->get();

      $jobhistory = Job_history::where('job_historys.id_postulant', $employee->postulant->id)->get();

      $language = Language::where('languages.id_postulant',$employee->postulant->id)->get();

      $reference = Reference::where('references.id_postulant',$employee->postulant->id)->get();

      $education = Education::where('educations.id_postulant',$employee->postulant->id)->get();

      $organization = Organization::where('organizations.id_postulant', $employee->postulant->id)->get();

      $award = Award::where('awards.id_postulant', $employee->postulant->id) ->get();

      $training = Training::where('trainings.id_postulant', $employee->postulant->id)->get();

      $emercont = Emergencycontact::where('emergencycontacts.id_postulant', $employee->postulant->id)->get();

      $test = Personality_test::where('personality_tests.id_postulant',$employee->postulant->id)->get();
// dd($test);

$cek= Personality_test::where('id_postulant','=',$employee->postulant->id)
->count();
//dd($cek);

if($cek==0){
    $id = 0;
    $N = 0;
    $G = 0;
    $A = 0;
    $L = 0;
    $P = 0;

    $I = 0;
    $T = 0;
    $V = 0;
    $X = 0;
    $S = 0;

    $B = 0;
    $O = 0;
    $R = 0;
    $D = 0;
    $C = 0;

    $Z = 0;
    $E = 0;
    $K = 0;
    $F = 0;
    $W = 0;

    $dataset = array($N,$G,$A,$L,$P,$I,$T,$V,$X,$S,$B,$O,$R,$D,$C,$Z,$E,$K,$F,$W);
}

else{
    $data = $test;
    foreach ($data as $p){
    $id = $p->id;
      $N = $p->characterN;
      $G = $p->characterG;
      $A = $p->characterA;
      $L = $p->characterL;
      $P = $p->characterP;

      $I = $p->characterI;
      $T = $p->characterT;
      $V = $p->characterV;
      $X = $p->characterX;
      $S = $p->characterS;

      $B = $p->characterB;
      $O = $p->characterO;
      $R = $p->characterR;
      $D = $p->characterD;
      $C = $p->characterC;

      $Z = $p->characterZ;
      $E = $p->characterE;
      $K = $p->characterK;
      $F = $p->characterF;
      $W = $p->characterW;

      $dataset = array($N,$G,$A,$L,$P,$I,$T,$V,$X,$S,$B,$O,$R,$D,$C,$Z,$E,$K,$F,$W);
}
}
// dd($id);

      return view('admin.karyawan.show', compact('employee', 'family', 'jobhistory', 'language', 'reference', 'education', 'organization', 'award', 'training', 'emercont', 'test', 'dataset','N','G','A','L','P','I','T','V','X','S','B','O','R','D','C','Z','E','K','F','W','id'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('admin.karyawan.edit', ['grade'=>Grade::all(),'employees'=>Employee::all(),'position'=>Position::all(),'division'=>Division::all(),'corporate'=>Corporate::all(),'employee'=>$employee ]);
    }

    public function resignedit(Employee $employee)
    {

        return view('admin.karyawan.resignedit', ['grade'=>Grade::all(),'employees'=>Employee::all(),'position'=>Position::all(),'division'=>Division::all(),'corporate'=>Corporate::all(),'employee'=>$employee ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
      if($request->token == 'qazmkodd'){
        Interview::where('id',$request->idPel)
        ->update([
            'nama_lengkap' => $request->namaPel,
            'posisi_yang_dilamar' => $request->posisiPel,
            'tempat_lahir' => $request->tempatLahirPel,
            'tanggal_lahir' => $request->tanggalLahirPel,
            'no_ktp' => $request->ktpPel,
            'jenis_kelamin' => $request->jenKelPel,
            'anak_ke' => $request->anakkePel,
            'saudara' => $request->jmlhsPel,
            'status_perkawinan' => $request->statPel,
            'agama' => $request->agmPel,
            'kebangsaan' => $request->bangsaPel,
            'npwp' => $request->npwpPel,
            'alamat_domisili' => $request->alDoPel,
            'alamat_ktp' => $request->alKtpPel,
            'telepon' => $request->telPel,
            'email' => $request->emPriPel,
            'kendaraan' => $request->kendPel,
            'laptop' => $request->laptPel,
            'gaji_diharap' => $request->gajiHrpPel,
            'pencapaian_sudah_dilakukan' => $request->capSdhPel,
            'pencapaian_yang_diinginkan' => $request->capBlmPel
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'qzwxeck'){
        Family::where('id',$request->idFam)
        ->update([
            'hubungan' => $request->hubFam,
            'nama' => $request->namFam,
            'jenis_kelamin' => $request->jenkelFam,
            'usia' => $request->usiFam,
            'pendidikan' => $request->penFam,
            'pekerjaan' => $request->pekFam
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'jklasdjh'){
        Job_history::where('id',$request->idJh)
        ->update([
            'nama_perusahaan' => $request->namJh,
            'posisi' => $request->posJh,
            'tanggal_mulai' => $request->tglmJh,
            'tanggal_selesai' => $request->tglsJh,
            'alasan_berhenti' => $request->alsJh
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'oipxzcbhs'){
        Language::where('id',$request->idBhs)
        ->update([
            'language' => $request->bhsa,
            'oral' => $request->oralBhs,
            'written' => $request->writBhs,
            'reading' => $request->readBhs
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'hgfvbnref'){
        Reference::where('id',$request->idRef)
        ->update([
            'nama' => $request->namaRef,
            'perusahaan' => $request->perusRef,
            'jabatan' => $request->jbtnRef
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'mnbpoiedu'){
        Education::where('id',$request->idEdu)
        ->update([
            'jenis_pendidikan' => $request->jenpenEdu,
            'nama_sekolah' => $request->namsekEdu,
            'jurusan' => $request->jrsnEdu,
            'kota' => $request->kotaEdu,
            'tahun_mulai' => $request->thnmEdu,
            'tahun_selesai' => $request->thnsEdu
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'zwxecrorg'){
        Organization::where('id',$request->idOrg)
        ->update([
            'nama_organisasi' => $request->namorOrg,
            'jabatan' => $request->jbtnOrg,
            'tahun_mulai' => $request->thnmOrg,
            'tahun_selesai' => $request->thnsOrg
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'adgjlxaw'){
        Award::where('id',$request->idAw)
        ->update([
            'nama_prestasi' => $request->nampresAw,
            'periode' => $request->periodAw
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'urhfbvtrn'){
        Training::where('id',$request->idTrn)
        ->update([
            'jenis_pelatihan' => $request->jenpelTrn,
            'nama_penyelenggara' => $request->nampelTrn,
            'tempat' => $request->tempelTrn,
            'tanggal_mulai' => $request->tglmTrn,
            'tanggal_selesai' => $request->tglsTrn
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    elseif($request->token == 'qrupdhec'){
        Emergencycontact::where('id',$request->idEc)
        ->update([
            'nama' => $request->namaEc,
            'hubungan' => $request->hubunganEc,
            'alamat' => $request->almtEc,
            'telepon' => $request->tlpEc
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }


    elseif($request->token == 'awsxdrtest'){
        Test::where('id',$request->idTest)
        ->update([
            'characterN' => $request->jchN,
            'characterG' => $request->jchG,
            'characterA' => $request->jchA,
            'characterL' => $request->jchL,
            'characterP' => $request->jchP,
            'characterI' => $request->jchI,
            'characterT' => $request->jchT,
            'characterV' => $request->jchV,
            'characterX' => $request->jchX,
            'characterS' => $request->jchS,
            'characterB' => $request->jchB,
            'characterO' => $request->jchO,
            'characterR' => $request->jchR,
            'characterD' => $request->jchD,
            'characterC' => $request->jchC,
            'characterZ' => $request->jchZ,
            'characterE' => $request->jchE,
            'characterK' => $request->jchK,
            'characterF' => $request->jchF,
            'characterW' => $request->jchW
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }


    else{
        Interview::where('id',$request->idIntv)
        ->update([
            'catatan' => $request->catatanIntv,
            'status_lamaran' => $request->keputusanIntv,
            'alasan' => $request->alasanIntv
        ]);
        return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
    }
    }
    public function resignupdate(Request $request, Employee $employee)
    {

        Employee::where('id',$employee->id)
        ->update([

            'role'=> $request->level,
            'id_jabatan'=> $request->jabatan,
            'id_grade'=> $request->devisi,
            'id_corporate'=> $request->grade,
            'id_devisi'=> $request->corporate,
            'join_date'=> $request->join_date,
            'status'=> $request->status,
            'nama_atasan'=> $request->atasan,
            'jatah_cuti'=> $request->jatah_cuti,
            'first_contract'=> $request->keputusan_anda,
            'start_addendum1'=> $request->start,
            'end_addendum1'=> $request->end,
            'start_addendum2'=> $request->start2,
            'end_addendum2'=> $request->end2,
            'end_contract'=> $request->end_contract,
            'gaji_pokok'=> $request->gaji,
            'tunjangan_harian'=> $request->tunjangan_harian,
            'tunjangan_transport'=> $request->tunjangan_transport,
            'bpjs_kes'=> $request->kesehatan,
            'bpjs_ket'=> $request->ketenagakerjaan,
        ]);

        return redirect('/admin/resign')-> with('edit', 'Data karyawan Berhasil di Ubah');
    }

    public function karyawanupdate(Request $request, Employee $employee)
    {

        Employee::where('id',$employee->id)
        ->update([

            'role'=> $request->level,
            'id_jabatan'=> $request->jabatan,
            'id_grade'=> $request->devisi,
            'id_corporate'=> $request->grade,
            'id_devisi'=> $request->corporate,
            'join_date'=> $request->join_date,
            'status'=> $request->status,
            'nama_atasan'=> $request->atasan,
            'jatah_cuti'=> $request->jatah_cuti,
            'first_contract'=> $request->keputusan_anda,
            'start_addendum1'=> $request->start,
            'end_addendum1'=> $request->end,
            'start_addendum2'=> $request->start2,
            'end_addendum2'=> $request->end2,
            'end_contract'=> $request->end_contract,
            'gaji_pokok'=> $request->gaji,
            'tunjangan_harian'=> $request->tunjangan_harian,
            'tunjangan_transport'=> $request->tunjangan_transport,
            'bpjs_kes'=> $request->kesehatan,
            'bpjs_ket'=> $request->ketenagakerjaan,
        ]);

        Postulant::where('id',$employee->id_postulant)
        ->update([

            'posisi_yang_dilamar'=> $request->ketenagakerjaan,
            'tempat_lahir'=> $request->tempat_lahir,
            'tanggal_lahir'=> $request->tanggal_lahir,
            'no_ktp'=> $request->no_ktp,
            'jenis_kelamin'=> $request->jenis_kelamin,
            'status_perkawinan'=> $request->status_perkawinan,
            'agama'=> $request->agama,
            'kebangsaan'=> $request->kebangsaan,
            'tanggal_join'=> $request->tanggal_join,
            'npwp'=> $request->npwp,
            'alamat_domisili'=> $request->alamat_domisili,
            'kota_domisili'=> $request->kota_domisili,
            'alamat_ktp'=> $request->alamat_ktp,
            'telepon'=> $request->telepon,
            'email'=> $request->email,
            'kendaraan'=> $request->kendaraan,
            'laptop'=> $request->laptop,
            'gaji_diharap'=> $request->gaji_diharap,
            'pencapaian_sudah_dilakukan'=> $request->pencapaian_sudah_dilakukan,
            'pencapaian_yang_diinginkan'=> $request->pencapaian_yang_diinginkan,

          'status_lamaran' => 'ditambahkan',
        ]);

        return redirect('/admin/employees')-> with('edit', 'Data karyawan Berhasil di Ubah');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
      $employee = Employee::destroy($employee->id);
      //dd($employee);
      return redirect('/admin/resign')-> with('delete', 'Data Aset Berhasil di Hapus');
    }
}
