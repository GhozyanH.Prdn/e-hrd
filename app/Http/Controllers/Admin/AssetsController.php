<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Asset;
use App\AssetTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AssetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $assets = Asset::with(['transactions' => function($query) {
        $query->where('tanggal_kembali', null);
      }])->get();


      return view('admin.assets.index', compact('assets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      $id = Asset::getId();
      foreach ($id as $value);
      $idlama = $value->id;
      $idbaru = $idlama + 1;
      $bulan = date('m-Y');
      $kode_aset = 'QWERTY/'.$idbaru.'/'.$bulan;

      $imageName = time().'.'.$request->image->extension();

      $request->image->move(public_path('images/aset'), $imageName);

      Asset::create([
        'kode_aset' => $kode_aset,
        'nama_aset' => $request->nama_aset,
        'tanggal_beli' => $request->tanggal_beli,
        'kategori' => $request->kategori,
        'gambar' => $imageName,

      ]);
      //dd($request);
      return redirect('/admin/assets')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
      $tes = DB::table('asset_transactions')
    ->select('asset_transactions.id_aset','asset_transactions.kode_peminjaman','assets.nama_aset','asset_transactions.tanggal_pinjam','asset_transactions.tanggal_kembali', 'employees.id', 'employees.nama')
    ->join('assets','asset_transactions.id_aset','assets.id')
    ->join('employees','asset_transactions.id_pegawai','employees.id')
    ->where('assets.id', $asset->id)
    ->get();
//dd($tes);
    $users = DB::table('asset_transactions')
              ->select('assets.id','assets.kode_aset','assets.nama_aset','assets.tanggal_beli','asset_transactions.kode_peminjaman','asset_transactions.tanggal_pinjam','asset_transactions.tanggal_kembali','employees.nama')
              ->join('assets','asset_transactions.id_aset','assets.id')
              ->join('employees','asset_transactions.id_pegawai','employees.id')
              ->where('asset_transactions.tanggal_kembali', '=', null)
              ->where('assets.id', $asset->id)
              ->orderBy('asset_transactions.id', 'desc')
              ->limit(1)
              ->get();

    //dd($users);

      return view('admin.assets.show', compact('asset','tes', 'users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit(Asset $asset)
    {   //dd($asset);
        $kategori = $asset->kategori;
        //dd($kategori);
        return view('admin.assets.edit', compact('asset','kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, Asset $asset)
     {

         $asset-> kode_aset = $request->input('kode_aset');
         $asset-> nama_aset = $request->input('nama_aset');
         $asset-> tanggal_beli = $request->input('tanggal_beli');
         $asset-> kategori = $request->input('kategori');

         if ($request->has('image')) {
             // Get image file

             $imageName = time().'.'.$request->image->extension();
             $request->image->move(public_path('images/aset'), $imageName);
             $asset->gambar = $imageName;
         }
         $asset->save();
         //dd($request  );
         Alert::success('Berhasil di Update', 'Success');
         return redirect('/admin/assets')-> with('Alert');
     }


     public function pengembalian(Request $request,Asset $asset)
    {
      AssetTransaction::where('tanggal_kembali', null)->where('id_aset',$asset->id)
      ->update([
        'tanggal_kembali' =>  date('d-m-Y'),
      ]);
      Alert::success('Berhasil di Update', 'Success');
      return redirect('/admin/assets')-> with('Alert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
      Asset::destroy($asset->id);
      return redirect('/admin/assets')-> with('delete', 'Data Aset Berhasil di Hapus');
    }
    public function trash(){
      $assets = Asset::onlyTrashed()->get();
      return view ('admin.assets.trash', ['asset' => $assets]);
    }

    public function restore($id){
      $asset = asset::onlyTrashed()->where('id', $id);
      $asset->restore();
      return redirect('/admin/assets/trash')-> with('restore', 'Data Aset Berhasil di Restore');
    }

    public function deleted_permanent($id){
      $assets = Asset::onlyTrashed()->where('id', $id);
      $assets->forceDelete();

      return redirect('/admin/assets/trash')-> with('delete', 'Data Aset Berhasil di Delete Permanent');
    }

    public function restore_all(){
      $assets = Asset::onlyTrashed();
      $assets->restore();

      return redirect('/admin/assets/trash')-> with('restore_all', 'Data Aset Berhasil di Restore all');
    }

    public function deleted_all(){
      $assets = Asset::onlyTrashed();
      $assets->forceDelete();

      return redirect('/admin/assets/trash')-> with('deleted_all', 'Data Aset Berhasil di Delete Semua');
    }


    public function transaksi(Asset $asset){

      $transaksi = DB::table('asset_transactions')
      ->select('asset_transactions.id','assets.kode_aset','assets.nama_aset','assets.gambar','assets.kategori','assets.tanggal_beli','asset_transactions.id_pegawai','asset_transactions.tanggal_pinjam','employees.nama')
      ->join('assets','asset_transactions.id_aset','assets.id')
      ->join('employees','asset_transactions.id_pegawai','employees.id')
      ->get();

       //dd($transaksi);
      return view('admin.assets.penyerahan', compact('asset','transaksi'));

    }

    public function input_transaksi(Request $request)
    {
        //return $request;
        $id = AssetTransaction::getId();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $bulan = date('m-Y');
        $kode_peminjaman = 'QWERTY/'.$idbaru.'/'.$bulan;
        // return($request);
        $AssetTransaction = new AssetTransaction;
        $AssetTransaction -> kode_peminjaman = $kode_peminjaman;
        $AssetTransaction -> id_aset = $request -> id_aset;
        $AssetTransaction -> id_pegawai = $request -> id_pegawai;
        $AssetTransaction -> tanggal_pinjam = $request -> tanggal_pinjam;
        $AssetTransaction -> tanggal_kembali = null;
        $AssetTransaction -> save();


        //dd($request);

        return redirect('/admin/assets')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }
    public function penyerahan(Asset $asset)
    {
        $ambil = Asset::all();
        return view('admin.assets.penyerahan', compact('asset','ambil'));
    }
}
