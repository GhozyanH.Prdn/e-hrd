<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Division;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisions = Division::all();
        return view ('admin.divisi.index',compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaDiv' => 'required'
        ]);

        $id = DB::table('divisions')->orderBy('id','DESC')->take(1)->get();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_divisi = 'Div-'.$idbaru;

        Division::create([
            'kode_devisi' => $kode_divisi,
            'nama_devisi' => $request->namaDiv,
        ]);
        //dd($request);
        return redirect('/admin/divisi')-> with('status', 'Data Divisi Baru Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Division $division)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function edit(Division $division)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'namaDiv' => 'required'
        ]);

        Division::where('id',$request->idDiv)
        ->update([
            'nama_devisi' => $request->namaDiv
        ]);

        return redirect('/admin/divisi')-> with('edit', 'Data Divisi Berhasil Diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function destroy(Division $division)
    {
        //dd($division);
        Division::destroy($division->id);

        return redirect('/admin/divisi')-> with('delete', 'Data Divisi Berhasil di Hapus !!');
    }

    public function trash(){
        $divisi = Division::onlyTrashed()->get();
        return view ('admin.divisi.trash', ['divisions' => $divisi]);
    }

    public function restore($id){
        $divisi = Division::onlyTrashed()->where('id', $id);
        $divisi->restore();
        return redirect('/admin/divisi/trash')-> with('restore', 'Data Divisi Berhasil di Restore');
    }

    public function deleted_permanent($id){
        $divisi = Division::onlyTrashed()->where('id', $id);
        $divisi->forceDelete();
        return redirect('/admin/divisi/trash')-> with('delete', 'Data Divisi Berhasil di Delete Permanent');
    }

    public function restore_all(){
        $divisi = Division::onlyTrashed();
        $divisi->restore();
        return redirect('/admin/divisi/trash')-> with('restore_all', 'Data Divisi Berhasil di Restore all');
    }

    public function deleted_all(){
        $divisi = Division::onlyTrashed();
        $divisi->forceDelete();
        return redirect('/admin/divisi/trash')-> with('deleted_all', 'Data Divisi Berhasil di Delete Semua');
    }
}
