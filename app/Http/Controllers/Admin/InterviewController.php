<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Test;
use App\Training;
use App\Award;
use App\Emergencycontact;
use App\Organization;
use App\Education;
use App\Reference;
use App\Language;
use App\Job_history;
use App\Family;
use App\Interview;
use App\Personality_test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interview1 = DB::table('Postulants')
        -> select('postulants.id', 'postulants.nama_lengkap', 'postulants.telepon', 'postulants.form_lamaran_lengkap', 'postulants.catatan', 'postulants.alasan', 'postulants.status_lamaran', 'positions.nama_jabatan')
        -> join('positions','postulants.posisi_yang_dilamar','positions.id')
        -> where('status_lamaran','=',null)
        -> orWhere('status_lamaran','=','')
        -> get();

        $interview2 = DB::table('Postulants')
        -> select('postulants.id', 'postulants.nama_lengkap', 'postulants.telepon', 'postulants.form_lamaran_lengkap', 'postulants.catatan', 'postulants.alasan', 'postulants.status_lamaran', 'positions.nama_jabatan')
        -> join('positions','postulants.posisi_yang_dilamar','positions.id')
        -> where('status_lamaran','=','Diterima')
        -> orWhere('status_lamaran','=','Ditolak : Good')
        -> orWhere('status_lamaran','=','Ditolak : Bad')
        -> get();
        // dd($interview1);

        return view('admin.interview.index', compact('interview1','interview2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview  $interview)
    {
        // dd($interview);

        $family = DB::table('familys')
        ->select('postulants.id','familys.id', 'familys.id_postulant','familys.hubungan','familys.nama','familys.jenis_kelamin','familys.usia','familys.pendidikan','familys.pekerjaan')
        ->join('postulants','familys.id_postulant','postulants.id')
        ->where('familys.id_postulant', $interview->id)
        ->get();

        $jobhistory = DB::table('job_historys')
        ->select('postulants.id','job_historys.id', 'job_historys.id_postulant', 'job_historys.nama_perusahaan', 'job_historys.posisi', 'job_historys.tanggal_mulai', 'job_historys.tanggal_selesai', 'job_historys.alasan_berhenti')
        ->join('postulants','job_historys.id_postulant','postulants.id')
        ->where('job_historys.id_postulant', $interview->id)
        ->get();

        $language = DB::table('languages')
        ->select('postulants.id','languages.id', 'languages.id_postulant', 'languages.language', 'languages.oral', 'languages.written', 'languages.reading')
        ->join('postulants','languages.id_postulant','postulants.id')
        ->where('languages.id_postulant', $interview->id)
        ->get();

        $reference = DB::table('references')
        ->select('postulants.id','references.id', 'references.id_postulant', 'references.nama', 'references.perusahaan', 'references.jabatan')
        ->join('postulants','references.id_postulant','postulants.id')
        ->where('references.id_postulant', $interview->id)
        ->get();

        $education = DB::table('educations')
        ->select('postulants.id','educations.id', 'educations.id_postulant', 'educations.jenis_pendidikan', 'educations.nama_sekolah', 'educations.jurusan', 'educations.tahun_mulai', 'educations.tahun_selesai', 'educations.kota')
        ->join('postulants','educations.id_postulant','postulants.id')
        ->where('educations.id_postulant', $interview->id)
        ->get();

        $organization = DB::table('organizations')
        ->select('postulants.id','organizations.id', 'organizations.id_postulant', 'organizations.nama_organisasi', 'organizations.jabatan', 'organizations.tahun_mulai', 'organizations.tahun_selesai')
        ->join('postulants','organizations.id_postulant','postulants.id')
        ->where('organizations.id_postulant', $interview->id)
        ->get();

        $award = DB::table('awards')
        ->select('postulants.id','awards.id', 'awards.id_postulant', 'awards.nama_prestasi', 'awards.periode')
        ->join('postulants','awards.id_postulant','postulants.id')
        ->where('awards.id_postulant', $interview->id)
        ->get();

        $training = DB::table('trainings')
        ->select('postulants.id','trainings.id', 'trainings.id_postulant', 'trainings.jenis_pelatihan', 'trainings.nama_penyelenggara', 'trainings.tempat', 'trainings.tanggal_mulai', 'trainings.tanggal_selesai')
        ->join('postulants','trainings.id_postulant','postulants.id')
        ->where('trainings.id_postulant', $interview->id)
        ->get();

        $emercont = DB::table('emergencycontacts')
        ->select('postulants.id','emergencycontacts.id', 'emergencycontacts.id_postulant', 'emergencycontacts.nama', 'emergencycontacts.hubungan', 'emergencycontacts.alamat', 'emergencycontacts.telepon')
        ->join('postulants','emergencycontacts.id_postulant','postulants.id')
        ->where('emergencycontacts.id_postulant', $interview->id)
        ->get();

        $test = DB::table('personality_tests')
        ->select('postulants.id','personality_tests.id', 'personality_tests.id_postulant', 'personality_tests.characterN', 'personality_tests.characterG', 'personality_tests.characterA', 'personality_tests.characterL', 'personality_tests.characterP', 'personality_tests.characterI', 'personality_tests.characterT', 'personality_tests.characterV', 'personality_tests.characterX', 'personality_tests.characterS', 'personality_tests.characterB', 'personality_tests.characterO', 'personality_tests.characterR', 'personality_tests.characterD', 'personality_tests.characterC', 'personality_tests.characterZ', 'personality_tests.characterE', 'personality_tests.characterK', 'personality_tests.characterF', 'personality_tests.characterW')
        ->join('postulants','personality_tests.id_postulant','postulants.id')
        ->where('personality_tests.id_postulant', $interview->id)
        ->get();
        // dd($test);

        $cek= Personality_test::where('id_postulant','=',$interview->id)
        ->count();
        //dd($cek);

        if($cek==0){
            $id = 0;
            $N = 0;
            $G = 0;
            $A = 0;
            $L = 0;
            $P = 0;

            $I = 0;
            $T = 0;
            $V = 0;
            $X = 0;
            $S = 0;

            $B = 0;
            $O = 0;
            $R = 0;
            $D = 0;
            $C = 0;

            $Z = 0;
            $E = 0;
            $K = 0;
            $F = 0;
            $W = 0;

            $dataset = array($N,$G,$A,$L,$P,$I,$T,$V,$X,$S,$B,$O,$R,$D,$C,$Z,$E,$K,$F,$W);
        }

        else{
            $data = $test;
            foreach ($data as $p){
            $id = $p->id;
              $N = $p->characterN;
              $G = $p->characterG;
              $A = $p->characterA;
              $L = $p->characterL;
              $P = $p->characterP;

              $I = $p->characterI;
              $T = $p->characterT;
              $V = $p->characterV;
              $X = $p->characterX;
              $S = $p->characterS;

              $B = $p->characterB;
              $O = $p->characterO;
              $R = $p->characterR;
              $D = $p->characterD;
              $C = $p->characterC;

              $Z = $p->characterZ;
              $E = $p->characterE;
              $K = $p->characterK;
              $F = $p->characterF;
              $W = $p->characterW;

              $dataset = array($N,$G,$A,$L,$P,$I,$T,$V,$X,$S,$B,$O,$R,$D,$C,$Z,$E,$K,$F,$W);
        }
        }
        // dd($id);

        return view('admin.interview.show', compact('interview', 'family', 'jobhistory', 'language', 'reference', 'education', 'organization', 'award', 'training', 'emercont', 'test', 'dataset','N','G','A','L','P','I','T','V','X','S','B','O','R','D','C','Z','E','K','F','W','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview  $interview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->token == 'qazmkodd'){
            Interview::where('id',$request->idPel)
            ->update([
                'nama_lengkap' => $request->namaPel,
                'posisi_yang_dilamar' => $request->posisiPel,
                'tempat_lahir' => $request->tempatLahirPel,
                'tanggal_lahir' => $request->tanggalLahirPel,
                'no_ktp' => $request->ktpPel,
                'jenis_kelamin' => $request->jenKelPel,
                'anak_ke' => $request->anakkePel,
                'saudara' => $request->jmlhsPel,
                'status_perkawinan' => $request->statPel,
                'agama' => $request->agmPel,
                'kebangsaan' => $request->bangsaPel,
                'npwp' => $request->npwpPel,
                'alamat_domisili' => $request->alDoPel,
                'alamat_ktp' => $request->alKtpPel,
                'telepon' => $request->telPel,
                'email' => $request->emPriPel,
                'kendaraan' => $request->kendPel,
                'laptop' => $request->laptPel,
                'gaji_diharap' => $request->gajiHrpPel,
                'pencapaian_sudah_dilakukan' => $request->capSdhPel,
                'pencapaian_yang_diinginkan' => $request->capBlmPel
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'qzwxeck'){
            Family::where('id',$request->idFam)
            ->update([
                'hubungan' => $request->hubFam,
                'nama' => $request->namFam,
                'jenis_kelamin' => $request->jenkelFam,
                'usia' => $request->usiFam,
                'pendidikan' => $request->penFam,
                'pekerjaan' => $request->pekFam
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'jklasdjh'){
            Job_history::where('id',$request->idJh)
            ->update([
                'nama_perusahaan' => $request->namJh,
                'posisi' => $request->posJh,
                'tanggal_mulai' => $request->tglmJh,
                'tanggal_selesai' => $request->tglsJh,
                'alasan_berhenti' => $request->alsJh
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'oipxzcbhs'){
            Language::where('id',$request->idBhs)
            ->update([
                'language' => $request->bhsa,
                'oral' => $request->oralBhs,
                'written' => $request->writBhs,
                'reading' => $request->readBhs
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'hgfvbnref'){
            Reference::where('id',$request->idRef)
            ->update([
                'nama' => $request->namaRef,
                'perusahaan' => $request->perusRef,
                'jabatan' => $request->jbtnRef
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'mnbpoiedu'){
            Education::where('id',$request->idEdu)
            ->update([
                'jenis_pendidikan' => $request->jenpenEdu,
                'nama_sekolah' => $request->namsekEdu,
                'jurusan' => $request->jrsnEdu,
                'kota' => $request->kotaEdu,
                'tahun_mulai' => $request->thnmEdu,
                'tahun_selesai' => $request->thnsEdu
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'zwxecrorg'){
            Organization::where('id',$request->idOrg)
            ->update([
                'nama_organisasi' => $request->namorOrg,
                'jabatan' => $request->jbtnOrg,
                'tahun_mulai' => $request->thnmOrg,
                'tahun_selesai' => $request->thnsOrg
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'adgjlxaw'){
            Award::where('id',$request->idAw)
            ->update([
                'nama_prestasi' => $request->nampresAw,
                'periode' => $request->periodAw
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'urhfbvtrn'){
            Training::where('id',$request->idTrn)
            ->update([
                'jenis_pelatihan' => $request->jenpelTrn,
                'nama_penyelenggara' => $request->nampelTrn,
                'tempat' => $request->tempelTrn,
                'tanggal_mulai' => $request->tglmTrn,
                'tanggal_selesai' => $request->tglsTrn
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'qrupdhec'){
            Emergencycontact::where('id',$request->idEc)
            ->update([
                'nama' => $request->namaEc,
                'hubungan' => $request->hubunganEc,
                'alamat' => $request->almtEc,
                'telepon' => $request->tlpEc
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        elseif($request->token == 'awsxdrtest'){
            Test::where('id',$request->idTest)
            ->update([
                'characterN' => $request->jchN,
                'characterG' => $request->jchG,
                'characterA' => $request->jchA,
                'characterL' => $request->jchL,
                'characterP' => $request->jchP,
                'characterI' => $request->jchI,
                'characterT' => $request->jchT,
                'characterV' => $request->jchV,
                'characterX' => $request->jchX,
                'characterS' => $request->jchS,
                'characterB' => $request->jchB,
                'characterO' => $request->jchO,
                'characterR' => $request->jchR,
                'characterD' => $request->jchD,
                'characterC' => $request->jchC,
                'characterZ' => $request->jchZ,
                'characterE' => $request->jchE,
                'characterK' => $request->jchK,
                'characterF' => $request->jchF,
                'characterW' => $request->jchW
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }

        else{
            Interview::where('id',$request->idIntv)
            ->update([
                'catatan' => $request->catatanIntv,
                'status_lamaran' => $request->keputusanIntv,
                'alasan' => $request->alasanIntv
            ]);
            return back()-> with('edit', 'Data Grade Berhasil Diubah !!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview  $interview)
    {
        $interview = Interview::destroy($interview->id);
        return redirect('/admin/interview')-> with('delete', 'Data Grade Berhasil Dihapus');
    }

    public function trash(){
        $interview = Interview::onlyTrashed()->get();
        return view ('/admin/interview/trash', ['interviews' => $interview]);
      }

    public function restore($id){
        $interview = Interview::onlyTrashed()->where('id', $id);
        $interview->restore();
        return redirect('/admin/interview/trash')-> with('restore', 'Data Grade Berhasil di Restore');
      }

    public function deleted_permanent($id){
        $interview = Interview::onlyTrashed()->where('id', $id);
        $interview->forceDelete();

        return redirect('/admin/interview/trash')-> with('delete', 'Data Grade Berhasil di Delete Permanent');
      }

    public function restore_all(){
        $interview = Interview::onlyTrashed();
        $interview->restore();

        return redirect('/admin/interview/trash')-> with('restore_all', 'Data Grade Berhasil di Restore all');
      }

    public function deleted_all(){
        $interview = Interview::onlyTrashed();
        $interview->forceDelete();

        return redirect('/admin/interview/trash')-> with('deleted_all', 'Data Grade Berhasil di Delete Semua');
      }
}
