<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Asset;
use App\AssetTransaction;
use App\Cuti;
use App\Interview;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hitkary = Employee::where('status','=','Kontrak')
        ->orWhere('status','=','Tetap')
        ->count();

        $hitkarytet = Employee::where('status','=','Tetap')
        ->count();

        $hitkarykon = Employee::where('status','=','Kontrak')
        ->count();

        $hitkaryres = Employee::where('status','=','Resign')
        ->count();

        $jenkell = Interview::where('jenis_kelamin','=','L')
        ->count();

        $jenkelp = Interview::where('jenis_kelamin','=','P')
        ->count();

        $hitast = Asset::count();

        $astel = Asset::where('kategori','=','Elektronik')
        ->count();
        $astper = Asset::where('kategori','=','Peralatan Kantor')
        ->count();
        $astla = Asset::where('kategori','=','Lain-Lain')
        ->count();

        $hitastel = $astel / ($astel + $astper + $astla) * 100;
        $hitastper = $astper / ($astel + $astper + $astla) * 100;
        $hitastla = $astla / ($astel + $astper + $astla) * 100;

        $hitastpin = AssetTransaction::where('tanggal_kembali','=', null)
        ->orWhere('tanggal_kembali','=', '')
        ->count();

        $hitcutadm = Cuti::Where('keputusan_hrd','=', 'menunggu')
        ->count();

        $hitcutats = Cuti::Where('keputusan_atasan','=', 'menunggu')
        ->count();

        $hitcutatsmy = Cuti::where('nama_atasan', Auth::user()->nama)
        ->where('keputusan_atasan','=', 'menunggu')
        ->count();
        // dd(Auth::user()->nama, $hitcutatsmy);

        $hitpel = Interview::where('status_lamaran','=', null)
        ->orWhere('status_lamaran','=', '')
        ->count();


        $karultah = DB::select("SELECT nama_lengkap, tanggal_lahir FROM postulants
        WHERE DATE_FORMAT(tanggal_lahir,'%m %d')
        BETWEEN DATE_FORMAT((INTERVAL 1 DAY + CURDATE()),'%m %d')
        AND DATE_FORMAT((INTERVAL 30 DAY + CURDATE()),'%m %d')
        ORDER BY tanggal_lahir asc;");
        // dd($karultah);

        $da = date('d');
        $ma = date('m');
        $ya = date('Y');
        $mp = 0;

        $dp = $da + 30;
        if($dp > 31){
            $da = $dp - 31;
            $ma = $ma + 1;
            $mp = $ma - 12;
            if($mp > 0){
                $ya = $ya + $mp;
            }
        }

        if($ma <10){
            $ma = '0'.$ma;
        }

        $now = $ya.'-'.$ma.'-'.$da;
        //dd($ma);

        $takonaw = date('Y-m-d');
        $takonak = $now;
        // dd($takonaw, $takonak);

        $konkar = DB::table('employees')
        ->whereBetween('end_contract', [$takonaw, $takonak])
        ->where('status', 'Tetap')
        ->orWhere('status', 'Kontrak')
        ->orderBy('end_contract', 'asc')
        ->get();
        $konkar2 = DB::table('employees')
        ->whereBetween('end_addendum1', [$takonaw, $takonak])
        ->where('status', 'Tetap')
        ->orWhere('status', 'Kontrak')
        ->orderBy('end_addendum1', 'asc')
        ->get();
        $konkar3 = DB::table('employees')
        ->whereBetween('end_addendum2', [$takonaw, $takonak])
        ->where('status', 'Tetap')
        ->orWhere('status', 'Kontrak')
        ->orderBy('end_addendum2', 'asc')
        ->get();
        // dd($konkar);
        $result = DB::select('select a.id,a.nama,a.parent,a.parent_path,b.nama_jabatan,a.nama from employees a, positions b where a.id_jabatan = b.id order by id');
        $k = count($result);
        for($i=0;$i<$k;$i++){
        $tmp[] = $result[$i]->parent_path;

        $myJSON[] = '{"id":'.$result[$i]->nama.',"title":'.$result[$i]->nama_jabatan.',"name":'.$result[$i]->nama.'}';

     }

        $tole = implode(",",$tmp);
        $kk = json_encode($tmp);

        return view('admin.tes')
        ->with('hitkary',$hitkary)
        ->with('hitkarytet',$hitkarytet)
        ->with('hitkarykon',$hitkarykon)
        ->with('hitkaryres',$hitkaryres)
        ->with('jenkell',$jenkell)
        ->with('jenkelp',$jenkelp)
        ->with('hitast',$hitast)
        ->with('hitastpin',$hitastpin)
        ->with('hitastel',$hitastel)
        ->with('hitastper',$hitastper)
        ->with('hitastla',$hitastla)
        ->with('hitcutadm',$hitcutadm)
        ->with('hitcutats',$hitcutats)
        ->with('hitcutatsmy',$hitcutatsmy)
        ->with('hitpel',$hitpel)
        ->with('karultah',$karultah)
        ->with('konkar',$konkar)
        ->with('konkar2',$konkar2)
        ->with('konkar3',$konkar3)
        ->with('tole',$kk)
        ->with('tes',$myJSON);

    }
    public function depan()
    {
        return view('welcome');
    }
}
