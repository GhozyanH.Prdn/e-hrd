<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Reference;
use Illuminate\Http\Request;
use Auth;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = Reference::where('id_postulant',Auth::User()->id)->get();
      //dd($tampil);
      return view('postulant.personal.reference', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    if($request->cekref == "no"){
        Reference::create([
            'id_postulant' => Auth::User()->id,
            'nama' => "Tidak Memiliki Referensi"
            ]);
    }

    else{
        $request->validate([
            'nama' => 'required',
            'perusahaan' => 'required',
            'jabatan' => 'required',
            'telepon' => 'required'
        ]);

    Reference::create([
      'id_postulant' => Auth::User()->id,
      'nama' => $request->nama,
      'perusahaan' => $request->perusahaan,
      'jabatan' => $request->jabatan,
      'telepon' => $request->telepon,
      ]);
    }
      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/reference')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\References  $references
     * @return \Illuminate\Http\Response
     */
    public function show(Reference $reference)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\References  $references
     * @return \Illuminate\Http\Response
     */
    public function edit(References $reference)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\References  $references
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reference $reference)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\References  $references
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reference $reference)
    {
        Reference::destroy($reference->id);
        Alert::warning('Data Berhasil Dihapus', 'Warning');
        return redirect('/personal/reference')-> with('Alert');
    }
}
