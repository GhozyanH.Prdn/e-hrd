<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Emergencycontact;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class EmergencycontactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = DB::table('emergencycontacts')
        // ->select('emergencycontacts.id','emergencycontacts.id_postulant','emergencycontacts.nama','familys.id','familys.nama')
        // ->join('familys','emergencycontacts.nama','familys.id')
        ->where('id_postulant',Auth::User()->id)->get();
      $hubungan = DB::table('familys')
        -> groupBy('nama')
        -> where('id_postulant',Auth::User()->id)
        -> get();
      //dd($tampil);
      return view('postulant.personal.emergencycontact', compact('tampil','hubungan'));
    }

    function fetch(Request $request)
    {
     $select = $request->get('select');
     $value = $request->get('value');
     $dependent = $request->get('dependent');
     $data = DB::table('familys')
       ->where('nama', $value)
       ->groupBy($dependent)
       ->get();
     $output = '.ucfirst($dependent).';
     foreach($data as $row)
     {
      $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
     }
     echo $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request);
      Emergencycontact::create([
      'id_postulant' => Auth::User()->id,
      'nama' => $request->nama,
      'hubungan' => $request->hubungan,
      'alamat' => $request->alamat,
      'telepon' => $request->telepon,
      ]);
      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/emergencycontact')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emergencycontact  $emergencycontact
     * @return \Illuminate\Http\Response
     */
    public function show(Emergencycontact $emergencycontact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emergencycontact  $emergencycontact
     * @return \Illuminate\Http\Response
     */
    public function edit(Emergencycontact $emergencycontact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emergencycontact  $emergencycontact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emergencycontact $emergencycontact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emergencycontact  $emergencycontact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emergencycontact $emergencycontact)
    {
      Emergencycontact::destroy($emergencycontact->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/emergencycontact')-> with('Alert');
    }
}
