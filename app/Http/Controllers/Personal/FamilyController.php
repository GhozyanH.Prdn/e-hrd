<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Family;
use Illuminate\Http\Request;
use Auth;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tampil = Family::where('id_postulant',Auth::User()->id)->get();
        //dd($tampil);
        return view('postulant.personal.family', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request);
      Family::create([
      'id_postulant' => $request->id_postulant,
      'hubungan' => $request->hubungan,
      'nama' => $request->nama,
      'jenis_kelamin' => $request->jenis_kelamin,
      'usia' => $request->usia,
      'pendidikan' => $request->pendidikan,
      'pekerjaan' => $request->pekerjaan,
      ]);

      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/family')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function show(Family $family)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function edit(Family $family)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Family $family)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Family  $family
     * @return \Illuminate\Http\Response
     */
    public function destroy(Family $family)
    { //dd($family);
      Family::destroy($family->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/family')-> with('Alert');
    }
}
