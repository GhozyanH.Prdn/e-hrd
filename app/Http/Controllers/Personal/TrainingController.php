<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Training;
use Illuminate\Http\Request;
use Auth;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = Training::where('id_postulant',Auth::User()->id)->get();
      //dd($tampil);
      return view('postulant.personal.training', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { //dd($request);
    
    if($request->cektrain == "no"){
      Training::create([
            'id_postulant' => Auth::User()->id,
            'jenis_pelatihan' => "Tidak Memiliki Riwayat Pekerjaan"
            ]);
    }

    else{
      $request->validate([
        'jenis_pelatihan' => 'required',
        'nama_penyelenggara' => 'required',
        'tanggal_mulai' => 'required',
        'tanggal_selesai' => 'required',
        'tempat' => 'required',
      ]);

      Training::create([
      'id_postulant' => Auth::User()->id,
      'jenis_pelatihan' => $request->jenis_pelatihan,
      'nama_penyelenggara' => $request->nama_penyelenggara,
      'tanggal_mulai' => $request->tanggal_mulai,
      'tanggal_selesai' => $request->tanggal_selesai,
      'tempat' => $request->tempat,
      ]);
    }
      
      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/training')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function show(Training $training)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function edit(Training $training)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Training $training)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Training  $training
     * @return \Illuminate\Http\Response
     */
    public function destroy(Training $training)
    {
      Training::destroy($training->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/training')-> with('Alert');
    }
}
