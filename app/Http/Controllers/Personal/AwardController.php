<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Award;
use Illuminate\Http\Request;
use Auth;


class AwardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = Award::where('id_postulant',Auth::User()->id)->get();
      //dd($tampil);
      return view('postulant.personal.award', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->cekawd == "no"){
        Award::create([
            'id_postulant' => Auth::User()->id,
            'nama_prestasi' => "Tidak Memiliki Riwayat Prestasi"
            ]);
      }

      else{
        $request->validate([
          'nama_prestasi' => 'required',
          'periode' => 'required',
        ]);

      Award::create([
        'id_postulant' => Auth::User()->id,
        'nama_prestasi' => $request->nama_prestasi,
        'periode' => $request->periode,
      ]);
      }

      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/award')-> with('Alert');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Award  $award
     * @return \Illuminate\Http\Response
     */
    public function show(Award $award)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Award  $award
     * @return \Illuminate\Http\Response
     */
    public function edit(Award $award)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Award  $award
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Award $award)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Award  $award
     * @return \Illuminate\Http\Response
     */
    public function destroy(Award $award)
    {
      Award::destroy($award->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/award')-> with('Alert');
    }
}
