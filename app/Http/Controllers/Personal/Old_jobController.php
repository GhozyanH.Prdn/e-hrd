<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Job_history;
use Illuminate\Http\Request;
use Auth;

class Old_jobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tampil = Job_history::where('id_postulant',Auth::User()->id)->get();
        return view('postulant.personal.old_job', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request);
    if($request->cekjh == "no"){
        Job_history::create([
            'id_postulant' => Auth::User()->id,
            'nama_perusahaan' => "Tidak Memiliki Riwayat Pekerjaan"
            ]);
    }

    else{
        $request->validate([
            'nama_perusahaan' => 'required',
            'posisi' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_selesai' => 'required',
            'alasan_berhenti' => 'required',
        ]);

        Job_history::create([
            'id_postulant' => Auth::User()->id,
            'nama_perusahaan' => $request->nama_perusahaan,
            'posisi' => $request->posisi,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_selesai' => $request->tanggal_selesai,
            'alasan_berhenti' => $request->alasan_berhenti,
            ]);
    }
            
            Alert::success('Berhasil di Tambahkan', 'Success');
            return redirect('/personal/old_job')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job_history  $job_history
     * @return \Illuminate\Http\Response
     */
    public function show(Job_history $job_history)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job_history  $job_history
     * @return \Illuminate\Http\Response
     */
    public function edit(Job_history $job_history)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job_history  $job_history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job_history $job_history)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job_history  $job_history
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job_history $job_history)
    { //dd($job_history);
      Job_history::destroy($job_history->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/old_job')-> with('Alert');
    }
}
