<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Personality_test;
use App\Postulant;
use App\Employee;
use App\Family;
use App\Education;
use App\Job_history;
use App\Training;
use App\Organization;
use App\Award;
use App\Language;
use App\Reference;
use App\Emergencycontact;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class PersonalityTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cek = Personality_test::where('id_postulant', Auth::user()->id)
        ->count();

        $cekpost = Postulant::where('posisi_yang_dilamar', '!=', null)
        ->where('id', Auth::user()->id)
        ->count();

        if($cekpost>=1){
            $cekpost=1;
        }
        else{
            $cekpost=0;
        }

        $cekfam = Family::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekfam>=1){
            $cekfam=1;
        }
        else{
            $cekfam=0;
        }
        $cekedu = Education::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekedu>=1){
            $cekedu=1;
        }
        else{
            $cekedu=0;
        }

        $cekjhis = Job_history::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekjhis>=1){
            $cekjhis=1;
        }
        else{
            $cekjhis=0;
        }
        
        $cektra = Training::where('id_postulant', Auth::user()->id)
        ->count();

        if($cektra>=1){
            $cektra=1;
        }
        else{
            $cektra=0;
        }

        $cekorg = Organization::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekorg>=1){
            $cekorg=1;
        }
        else{
            $cekorg=0;
        }

        $cekawd = Award::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekawd>=1){
            $cekawd=1;
        }
        else{
            $cekawd=0;
        }

        $ceklang = Language::where('id_postulant', Auth::user()->id)
        ->count();

        if($ceklang>=1){
            $ceklang=1;
        }
        else{
            $ceklang=0;
        }

        $cekref = Reference::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekref>=1){
            $cekref=1;
        }
        else{
            $cekref=0;
        }

        $cekemc = Emergencycontact::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekemc>=1){
            $cekemc=1;
        }
        else{
            $cekemc=0;
        }

        $cekall = $cekpost + $cekfam + $cekedu + $cekjhis + $cektra + $cekorg + $cekawd + $ceklang + $cekref + $cekemc;
        // dd($cekpost, $cekfam, $cekedu, $cekjhis, $cektra, $cekorg, $cekawd, $ceklang, $cekref, $cekemc, $cekall);

        return view('postulant.personal.test', compact('cek', 'cekall'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      Personality_test::create([
      'id_postulant' => Auth::User()->id,
      'characterG' => ($request->SQ1)+($request->SQ11)+($request->SQ21)+($request->SQ31)+($request->SQ41)+($request->SQ51)+($request->SQ61)+($request->SQ71)+($request->SQ81),
      'characterL' => ($request->SQ12)+($request->SQ12)+($request->SQ22)+($request->SQ32)+($request->SQ42)+($request->SQ52)+($request->SQ62)+($request->SQ72)+($request->SQ82),
      'characterI' => ($request->SQ23)+($request->SQ33)+($request->SQ43)+($request->SQ53)+($request->SQ63)+($request->SQ73)+($request->SQ83)+($request->SQ71)+($request->SQ82),
      'characterT' => ($request->SQ34)+($request->SQ44)+($request->SQ54)+($request->SQ64)+($request->SQ74)+($request->SQ84)+($request->SQ83)+($request->SQ72)+($request->SQ61),
      'characterV' => ($request->SQ45)+($request->SQ55)+($request->SQ65)+($request->SQ75)+($request->SQ85)+($request->SQ84)+($request->SQ73)+($request->SQ62)+($request->SQ51),

      'characterS' => ($request->SQ56)+($request->SQ66)+($request->SQ76)+($request->SQ86)+($request->SQ85)+($request->SQ74)+($request->SQ63)+($request->SQ52)+($request->SQ41),
      'characterR' => ($request->SQ67)+($request->SQ77)+($request->SQ87)+($request->SQ86)+($request->SQ75)+($request->SQ64)+($request->SQ53)+($request->SQ42)+($request->SQ31),
      'characterD' => ($request->SQ78)+($request->SQ88)+($request->SQ87)+($request->SQ76)+($request->SQ65)+($request->SQ54)+($request->SQ43)+($request->SQ32)+($request->SQ21),
      'characterC' => ($request->SQ89)+($request->SQ88)+($request->SQ77)+($request->SQ66)+($request->SQ55)+($request->SQ44)+($request->SQ33)+($request->SQ22)+($request->SQ11),
      'characterE' => ($request->SQ89)+($request->SQ78)+($request->SQ67)+($request->SQ56)+($request->SQ45)+($request->SQ34)+($request->SQ23)+($request->SQ12)+($request->SQ1),

      'characterW' => ($request->SQ90)+($request->SQ80)+($request->SQ70)+($request->SQ60)+($request->SQ50)+($request->SQ40)+($request->SQ30)+($request->SQ20)+($request->SQ10),
      'characterF' => ($request->SQ79)+($request->SQ69)+($request->SQ59)+($request->SQ49)+($request->SQ39)+($request->SQ29)+($request->SQ19)+($request->SQ9)+($request->SQ10),
      'characterK' => ($request->SQ68)+($request->SQ58)+($request->SQ48)+($request->SQ38)+($request->SQ28)+($request->SQ18)+($request->SQ8)+($request->SQ9)+($request->SQ20),
      'characterZ' => ($request->SQ57)+($request->SQ47)+($request->SQ37)+($request->SQ27)+($request->SQ17)+($request->SQ7)+($request->SQ8)+($request->SQ19)+($request->SQ20),
      'characterO' => ($request->SQ46)+($request->SQ36)+($request->SQ26)+($request->SQ16)+($request->SQ6)+($request->SQ7)+($request->SQ18)+($request->SQ29)+($request->SQ40),

      'characterB' => ($request->SQ35)+($request->SQ25)+($request->SQ15)+($request->SQ5)+($request->SQ6)+($request->SQ17)+($request->SQ28)+($request->SQ39)+($request->SQ20),
      'characterX' => ($request->SQ24)+($request->SQ14)+($request->SQ4)+($request->SQ5)+($request->SQ16)+($request->SQ27)+($request->SQ38)+($request->SQ49)+($request->SQ60),
      'characterP' => ($request->SQ13)+($request->SQ3)+($request->SQ4)+($request->SQ15)+($request->SQ26)+($request->SQ37)+($request->SQ48)+($request->SQ59)+($request->SQ70),
      'characterA' => ($request->SQ2)+($request->SQ3)+($request->SQ14)+($request->SQ25)+($request->SQ36)+($request->SQ47)+($request->SQ58)+($request->SQ69)+($request->SQ80),
      'characterN' => ($request->SQ2)+($request->SQ13)+($request->SQ24)+($request->SQ35)+($request->SQ46)+($request->SQ57)+($request->SQ68)+($request->SQ79)+($request->SQ90),
      ]);

      DB::table('postulants')->where('id', Auth::User()->id)->update(['form_lamaran_lengkap'=>'yes']);

      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/personality_test')-> with('Alert');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personality_test  $personality_test
     * @return \Illuminate\Http\Response
     */
    public function show(Personality_test $personality_test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personality_test  $personality_test
     * @return \Illuminate\Http\Response
     */
    public function edit(Personality_test $personality_test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personality_test  $personality_test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Personality_test $personality_test)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personality_test  $personality_test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personality_test $personality_test)
    {
        //
    }
}
