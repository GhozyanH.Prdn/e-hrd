<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
  protected $table = "languages";
  protected $fillable = ['id_postulant','language', 'oral', 'written', 'reading'];
}
