<?php

namespace App\Providers;
use Auth;
use App\Family;
use Illuminate\Support\ServiceProvider;

class DynamicFamily extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
        $view->with('family_array', Family::all());

      });
    }
}
