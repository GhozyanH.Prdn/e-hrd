<?php

namespace App\Providers;
use App\Position;
use Illuminate\Support\ServiceProvider;

class DynamicPosition extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
        $view->with('position_array', Position::all());
      });
    }
}
