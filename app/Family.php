<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $table = "familys";
    protected $fillable = ['id_postulant','hubungan', 'nama', 'jenis_kelamin', 'usia', 'pendidikan','pekerjaan'];

    public function kontak(){
      return $this -> belongsTo('App\Family','id_postulant');
    }
}
