<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = "salaries";
    protected $fillable = ['Tahun', 'Bulan', 'ID_Pegawai','Nama_Pegawai', 'JUMLAH_CUTI_TIDAK_DIBAYAR', 'Gaji_Pensiun'
    , 'Tunjangan_Harian', 'Tunjangan_Lainnya_Uang_Lembur_dsb', 'Cuti_Tidak_Dibayar', 'Bonus_Tak_Tetap'
    , 'Jumlah_Penghasilan_Bruto', 'Jumlah_Pengurangan', 'PPh_21_sebulan', 'Uang_Pembayaran_Gaji'];

    public function employee(){
		return $this ->belongsTo('App\Employee','id_postulant');
    }
}
