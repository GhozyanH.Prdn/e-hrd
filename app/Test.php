<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = "personality_tests";
    protected $fillable = ['characterN', 'characterG', 'characterA', 'characterL', 'characterP', 'characterI', 'characterT', 'characterV', 'characterX', 'characterS', 'characterB', 'characterO', 'characterR', 'characterD', 'characterC', 'characterZ', 'characterE', 'characterK', 'characterF', 'characterW'];
}
