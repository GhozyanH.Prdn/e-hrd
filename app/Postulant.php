<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notification\PersonalResetPasswordNotification;

class Postulant extends Authenticatable
{
  use Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $guard = 'personal';
   protected $fillable = [
       'nama_lengkap','posisi_yang_dilamar','tempat_lahir','tanggal_lahir','no_ktp','jenis_kelamin',
       'status_perkawinan','agama','kebangsaan','tanggal_join','npwp','alamat_domisili','alamat_ktp',
       'telepon','email','password','kendaraan','laptop','gaji_diharap','anak_ke','saudara','pencapaian_sudah_dilakukan',
       'pencapaian_yang_diinginkan','foto','status_lamaran','alasan','form_lamaran_lengkap'
   ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
      'email_verified_at' => 'datetime',
  ];
  public function SendsPasswordResetNotification($token){
    $this->nofity(new PersonalResetPasswordNotification($token));
  }
}
