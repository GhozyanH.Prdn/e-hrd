<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_history extends Model
{
  protected $table = "Job_historys";
  protected $fillable = ['id_postulant','nama_perusahaan', 'posisi', 'tanggal_mulai', 'tanggal_selesai', 'alasan_berhenti'];
}
