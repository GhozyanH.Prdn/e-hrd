<?php

namespace App\Imports;

use App\Salary;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;

use Maatwebsite\Excel\Concerns\WithStartRow;



    class ThirdSheetImport implements ToModel,WithStartRow
    {

        public function model(array $row)
        {
            return new Salary([
                'Bulan' => 'Februari',
                'Nama_Pegawai' => $row[2],
                'Kehadiran' => $row[8], 
                'Jumlah_Cuti_Tak_Dibayar' => $row[10], 
                'Gaji_Pensiun' => $row[12],
                'Tunjangan_Harian' => $row[13], 
                'Tunjangan_Lainnya_Uang_Lembur_dsb' => $row[14], 
                'Cuti_Tidak_Dibayar' => $row[16],
                'Bonus_Tak_Tetap' => $row[19], 
                'Jumlah_Penghasilan_Bruto' => $row[19], 
                'Jumlah_Pengurangan' => $row[21], 
                'PPh_21_sebulan' => $row[28],
                'Uang_Pembayaran_Gaji' => $row[29], 
               
            ]);
        }
        public function startRow(): int
        {
            return 15;
        }
      
        
    }
  
  
   
    
