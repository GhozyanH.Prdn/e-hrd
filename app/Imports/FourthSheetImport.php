<?php

namespace App\Imports;

use App\Salary;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;


class FourthSheetImport implements ToModel,WithStartRow
    {
        
        public function model(array $row)
    {
        $row['Bulan'] = 'Maret';
        $row['Nama_Pegawai'] = $row[2];
        $row['Kehadiran']  = $row[8];
        $row['Jumlah_Cuti_Tak_Dibayar'] = $row[10]; 
        $row['Gaji_Pensiun']  = $row[12];
        $row['Tunjangan_Harian'] = $row[13]; 
        $row['Tunjangan_Lainnya_Uang_Lembur_dsb'] = $row[14];
        $row['Cuti_Tidak_Dibayar'] = $row[16];
        $row['Bonus_Tak_Tetap'] = $row[19]; 
        $row['Jumlah_Penghasilan_Bruto']  = $row[19]; 
        $row['Jumlah_Pengurangan']  = $row[21]; 
        $row['PPh_21_sebulan'] = $row[28];
        $row['Uang_Pembayaran_Gaji'] = $row[29]; 
      
        return new Salary($row);
    }
        public function startRow(): int
        {
            return 15;
        }
       
    }

