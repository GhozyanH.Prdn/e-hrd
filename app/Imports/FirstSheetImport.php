<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;


    class FirstSheetImport implements  SkipsUnknownSheets
    {
        public function onUnknownSheet($sheet2)
        {
            // E.g. you can log that a sheet was not found.
            info("Sheet {$sheet2} was skipped");
        }
    }
    