<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personality_test extends Model
{
    protected $table = "personality_tests";
    protected $fillable = ['id_postulant','characterN','characterG','characterA','characterL','characterP','characterI','characterT','characterV','characterX','characterS','characterB','characterO','characterR','characterD','characterC','characterZ','characterE','characterK','characterF','characterW'];

    public function personal(){
		return $this ->belongsTo('App\Postulant','id_postulant');
    }
}
