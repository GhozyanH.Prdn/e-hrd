<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
  protected $table = "trainings";
  protected $fillable = ['id_postulant','jenis_pelatihan', 'nama_penyelenggara', 'tanggal_mulai', 'tanggal_selesai','tempat'];
}
